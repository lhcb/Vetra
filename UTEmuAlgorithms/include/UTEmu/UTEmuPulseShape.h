/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/*
 *  UTEmuPulseShape.h
 *
 *  Created on: August, 2023
 *      Author: Wojciech Krupa (wokrupa@cern.ch)
 */

#pragma once

#include "Event/ODIN.h"
#include "Event/UTDigit.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiKernel/IEventProcessor.h"
#include "Kernel/IEventCounter.h"
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/UTDAQID.h"
#include "LHCbAlgs/Consumer.h"
#include "UTDet/DeUTDetector.h"
#include "UTEmu/UTEmuVetraHelper.h"
#include "UTEmu/UTMaps.h"

using namespace LHCb;

namespace UTEmu {

  class PulseShape
      : public LHCb::Algorithm::Consumer<void( UTDigits const&, UTDigits const&, const LHCb::ODIN&,
                                               DeUTDetector const& ),
                                         LHCb::DetDesc::usesBaseAndConditions<GaudiHistoAlg, DeUTDetector>> {
  public:
    /// constructer
    PulseShape( const std::string& name, ISvcLocator* svcloc )
        : Consumer{name,
                   svcloc,
                   {{"DigitLocation", UTEmu::UTDigitLocation::UTDigits},
                    {"ErrDigitLocation", UTEmu::UTDigitLocation::UTErrDigits},
                    {"ODINLocation", LHCb::ODINLocation::Default},
                    {"UTLocation", DeUTDetLocation::location()}}} {}

    StatusCode initialize() override;
    void       operator()( const UTDigits&, const UTDigits&, const LHCb::ODIN&, DeUTDetector const& ) const override;

    mutable UTMaps UTMap;

    mutable std::map<std::string, Gaudi::Accumulators::Histogram<2>> m_2d_ch;

    Gaudi::Property<unsigned int> m_layer{this, "Layer", 0};

  private:
    void fillHistograms( const LHCb::UTDigit*, DeUTDetector const&, const LHCb::ODIN& ) const;
  };
} // namespace UTEmu