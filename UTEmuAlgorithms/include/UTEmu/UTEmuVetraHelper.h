/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/*
 * UTEmuVetraHelper.h
 *
 *  Created on: August, 2023
 *      Author: Wojciech Krupa (wokrupa@cern.ch)
 */

#pragma once
#pragma GCC diagnostic ignored "-Wunused-variable"
#include "Gaudi/Accumulators/Histogram.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiAlg/GaudiHistoAlg.h"
#include <iomanip>
#include <iostream>
#include <string>
#include <vector>

namespace UTEmu {

  namespace Utility {
    template <typename T, typename OWNER>
    void map_emplace( T& t, typename T::key_type key, OWNER* owner, std::string const& title,
                      Gaudi::Accumulators::Axis<typename T::mapped_type::AxisArithmeticType> axis1 ) {
      t.emplace( std::piecewise_construct, std::forward_as_tuple( key ),
                 std::forward_as_tuple( owner, key, title, axis1 ) );
    }
    template <typename T, typename OWNER>
    void map_emplace( T& t, typename T::key_type key, OWNER* owner, std::string const& title,
                      Gaudi::Accumulators::Axis<typename T::mapped_type::AxisArithmeticType> axis1,
                      Gaudi::Accumulators::Axis<typename T::mapped_type::AxisArithmeticType> axis2 ) {
      t.emplace( std::piecewise_construct, std::forward_as_tuple( key ),
                 std::forward_as_tuple( owner, key, title, axis1, axis2 ) );
    }

  } // namespace Utility

  namespace DataLocations {
    static const std::string& UTEmu_path = UTEMU_PATH;
  } // namespace DataLocations

  namespace UTDigitLocation {
    inline const std::string Default          = "DAQ/RawEvent"; ///< Original FULL Raw Event
    inline const std::string UTDigits         = "/Event/UT/Digits";
    inline const std::string UTErrDigits      = "/Event/UT/ErrDigits";
    inline const std::string UTPedestalDigits = "/Event/UT/PedestalDigits";
    inline const std::string UTCMSDigits      = "/Event/UT/CMSDigits";
    inline const std::string UTTightDigits    = "/Event/UT/TightDigits";
  } // namespace UTDigitLocation

  // Define a vector of layer's perfixes
  const std::array<std::string, 4> UT_layers = {"UTaX", "UTaU", "UTbV", "UTbX"};

  namespace UTNumbers {
    inline const unsigned int nPlanes           = 4;
    inline const unsigned int nASICs            = 4;
    inline const unsigned int nStrips           = 512;
    inline const unsigned int nStripsPerASIC    = 128;
    inline const unsigned int nSectorsPerHybrid = 2;
    inline const int          ADC_range_low     = -32.5;
    inline const int          ADC_range_high    = 31.5;

  } // namespace UTNumbers

} // namespace UTEmu
