
/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/*
 * UTEmuCommonModeSubtractor.h
 *
 *  Created on: August, 2023
 *      Author: Wojciech Krupa (wokrupa@cern.ch)
 */

#pragma once
#include "Event/UTDigit.h"
#include "GaudiKernel/IEventProcessor.h"
#include "Kernel/IEventCounter.h"
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/UTDAQBoard.h"
#include "Kernel/UTDAQDefinitions.h"
#include "Kernel/UTDAQID.h"
#include "LHCbAlgs/Consumer.h"
#include "LHCbAlgs/Transformer.h"
#include "UTDAQ/UTADCWord.h"
#include "UTDAQ/UTHeaderWord.h"
#include "UTEmu/UTEmuVetraHelper.h"
#include "UTEmu/UTMaps.h"
#include <ctime>
#include <fstream>
#include <math.h> /* fabs */
#include <mutex>

using namespace LHCb;

namespace UTEmu {

  class CommonModeSubtractor
      : public LHCb::Algorithm::Transformer<LHCb::UTDigits( UTDigits const& ),
                                            LHCb::DetDesc::usesBaseAndConditions<GaudiHistoAlg, DeUTDetector>> {
  public:
    /// Standard constructor
    CommonModeSubtractor( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer{name,
                      pSvcLocator,
                      {{"PedestalDigitLocation", UTEmu::UTDigitLocation::UTPedestalDigits}},
                      {{"OutputCMSDigitData", UTEmu::UTDigitLocation::UTCMSDigits}}} {}

    StatusCode     initialize() override;
    LHCb::UTDigits operator()( const UTDigits& ) const override;

    std::string getASICName( const Detector::UT::ChannelID ) const;
    void        saveCMSNoiseToCSV();
    void        saveCommonMoseToCSV();
    void        makeCommonModePlots() const;
    void        makeCMSNoisePlots() const;

    // Parametrised funtion to plot average
    void plotAverage( const std::map<std::string, float>&, const std::string& ) const;

    StatusCode finalize() override;

    // Properties
    mutable std::map<std::string, Gaudi::Accumulators::Histogram<2>> m_2d_CMSADCCounter;

    Gaudi::Property<std::string>  m_runNumber{this, "RunNumber", "00000000"};
    Gaudi::Property<unsigned int> m_commonModeCut{this, "CommonModeCut", 10};
    Gaudi::Property<float>        m_eventMax{this, "Events", 100};

    mutable std::map<unsigned int, Gaudi::Accumulators::SigmaCounter<>> CMSNoise;
    mutable std::map<std::string, Gaudi::Accumulators::SigmaCounter<>>  CommonModeAv;
    mutable std::map<std::string, Gaudi::Accumulators::SigmaCounter<>>  CMSNoiseAv;

    mutable std::map<std::string, float> CommonModeMean;
    mutable std::map<std::string, float> CommonModeSigma;
    mutable std::map<std::string, float> CMSNoiseMean;
    mutable std::map<std::string, float> CMSNoiseSigma;

    mutable Gaudi::Accumulators::Counter<> m_eventNumber{this, "Event number"};
    mutable UTMaps                         UTMap;

  private:
    void fillHistograms( const LHCb::UTDigit* ) const;
  };
} // namespace UTEmu