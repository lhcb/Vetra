/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/*
 * UTEmuPedestalCalculatorDataMonitorAlgorithm.cpp
 *
 *  Created on: August, 2023
 *      Author: Wojciech Krupa (wokrupa@cern.ch)
 */

#pragma once

#include "Event/UTDigit.h"
#include "GaudiKernel/IEventProcessor.h"
#include "Kernel/IEventCounter.h"
#include "Kernel/IUTReadoutTool.h"
#include "LHCbAlgs/Consumer.h"
#include "TFile.h"
#include "UTDet/DeUTDetector.h"
#include "UTEmu/UTEmuVetraHelper.h"
#include "UTEmu/UTMaps.h"
#include <math.h>

using namespace LHCb;

namespace UTEmu {

  class PedestalCalculatorDataMonitorAlgorithm
      : public LHCb::Algorithm::Consumer<void(), LHCb::DetDesc::usesBaseAndConditions<GaudiHistoAlg, DeUTDetector>> {

  public:
    /// constructer
    PedestalCalculatorDataMonitorAlgorithm( const std::string& name, ISvcLocator* svcloc )
        : Consumer{name, svcloc, {}} {}

    Gaudi::Property<std::string> m_runNumber{this, "RunNumber", "00000000"};
    Gaudi::Property<float>       m_zs_factor{this, "ZS_factor", 4.5};

    mutable std::map<std::string, Gaudi::Accumulators::AveragingCounter<>> Pedestal_Av;
    mutable std::map<std::string, Gaudi::Accumulators::AveragingCounter<>> SigmaNoise_Av;

    mutable std::map<unsigned int, float> Pedestals;
    mutable std::map<unsigned int, float> SigmaNoise;
    mutable std::map<std::string, float>  ZS_th;

    StatusCode getSigmaNoise();
    StatusCode getPedestals();
    StatusCode getTresholds();
    StatusCode makePedestalsPlots();
    StatusCode makeSigmaNoisePlots();
    StatusCode makeTresholdsPlots();
    StatusCode initialize() override;
    StatusCode finalize() override;

    void operator()() const override;

    UTMaps UTMap;

  private:
  };
} // namespace UTEmu
