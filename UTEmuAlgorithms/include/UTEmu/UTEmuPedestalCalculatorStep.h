/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/*
 * UTEmuPedestalCalculatorStep.h
 *
 *  Created on: August, 2023
 *      Author: Wojciech Krupa (wokrupa@cern.ch)
 */

#pragma once
#include "Event/ODIN.h"
#include "Event/UTDigit.h"
#include "Kernel/IEventCounter.h"
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/UTDAQBoard.h"
#include "Kernel/UTDAQDefinitions.h"
#include "Kernel/UTDAQID.h"
#include "LHCbAlgs/Consumer.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TKey.h"
#include "UTDAQ/UTADCWord.h"
#include "UTDAQ/UTHeaderWord.h"
#include "UTEmu/UTEmuVetraHelper.h"
#include "UTEmu/UTMaps.h"
#include <fstream>
#include <map>
#include <math.h>

using namespace LHCb;

namespace UTEmu {

  class PedestalCalculatorStep
      : public LHCb::Algorithm::Consumer<void(), LHCb::DetDesc::usesBaseAndConditions<GaudiHistoAlg>> {
  public:
    PedestalCalculatorStep( const std::string& name,   // algorithm instance name
                            ISvcLocator*       pSvcLocator ) // service locator
        : Consumer{name, pSvcLocator, {}} {}

    StatusCode initialize() override;
    StatusCode finalize() override;
    void       operator()() const override;

    void savePedestalsToCsv();
    void saveBadChannelsToCsv();
    void saveSigmaNoiseToCsv();
    void calculatePedestals();
    void findBadChannels();
    void saveThresholdsToCsv();

    // properties
    Gaudi::Property<std::string>                                       m_runNumber{this, "RunNumber", "00000000"};
    mutable std::map<std::string, Gaudi::Accumulators::Histogram<2>>   m_2d_ADCCounter;
    mutable std::map<unsigned int, float>                              Pedestal;
    mutable std::map<unsigned int, float>                              SigmaNoise;
    mutable std::map<std::string, int>                                 Pedestal_Max;
    mutable std::map<std::string, Gaudi::Accumulators::SigmaCounter<>> SigmaNoiseAv;
    mutable std::map<std::string, Gaudi::Accumulators::SigmaCounter<>> PedestalAv;

    mutable std::map<unsigned int, bool> BadChannels;
    UTMaps                               UTMap;

  private:
  };
} // namespace UTEmu