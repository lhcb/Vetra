/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/*
 *  UTEmuRPedestalSubtractorDataMonitorAlgorithm.h
 *
 *  Created on: August, 2023
 *      Author: Wojciech Krupa (wokrupa@cern.ch)
 */

#pragma once

#include "Event/UTDigit.h"
#include "GaudiKernel/IEventProcessor.h"
#include "Kernel/IEventCounter.h"
#include "Kernel/IUTReadoutTool.h"
#include "LHCbAlgs/Consumer.h"
#include "UTDet/DeUTDetector.h"
#include "UTEmu/UTEmuVetraHelper.h"
#include "UTEmu/UTMaps.h"

using namespace LHCb;

namespace UTEmu {

  class PedestalSubtractorDataMonitorAlgorithm
      : public LHCb::Algorithm::Consumer<void( UTDigits const& ),
                                         LHCb::DetDesc::usesBaseAndConditions<GaudiHistoAlg, DeUTDetector>> {

  public:
    /// constructer
    PedestalSubtractorDataMonitorAlgorithm( const std::string& name, ISvcLocator* svcloc )
        : Consumer{name, svcloc, {{"PedestalDigitLocation", UTEmu::UTDigitLocation::UTPedestalDigits}}} {}

    StatusCode initialize() override;
    StatusCode finalize() override;
    void       operator()( const UTDigits& ) const override;

    mutable std::map<std::string, Gaudi::Accumulators::Histogram<2>> m_2d_PedSubADCCounter;

    UTMaps UTMap;

  private:
    void fillHistograms( const LHCb::UTDigit* ) const;
  };
} // namespace UTEmu
