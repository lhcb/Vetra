/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3),copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence,CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/** @class UTMaps
 *
 *  Helper class with several UT maps usefull in monitoring algorithms
 *
 *  @author Wojciech Krupa (wokrupa@cern.ch)
 *  @author Christos Hadjivasiliou (christos.hadjivasiliou@cern.ch)

 *  @date   2023-08-10
 */

#pragma once
#pragma GCC diagnostic ignored "-Wunused-variable"

#include <algorithm>
#include <iostream>
#include <map>
#include <string>
#include <tuple>
#include <vector>

class UTMaps {
public:
  // Define a tuple
  typedef std::tuple<float, float, float, float, std::string, std::string> CoordinatesTuple;

  // Constructor initializes the 5D vector
  UTMaps();

  // Function to access and modify elements in the 5D vector
  CoordinatesTuple         getTuple( int, int, int, int, int ) const;
  unsigned int             getChannel( std::string );
  std::string              getTELL40( unsigned int );
  unsigned int             getTELL40Bin( unsigned int );
  std::vector<std::string> getTELL40NamesASide();
  std::vector<std::string> getTELL40NamesCSide();
  std::vector<std::string> getModulesNames();
  std::vector<std::string> getASICNames();
  unsigned int             getSourceIDfromBoardNumber( unsigned int );
  std::vector<std::string> getNamesfromTELL40( std::string );
  unsigned int             get2_4_Flow0_Bin( unsigned int );
  unsigned int             get2_4_Flow1_Bin( unsigned int );
  unsigned int             get2_5_Flow0_Bin( unsigned int );
  unsigned int             get2_5_Flow1_Bin( unsigned int );
  std::string              getFlavour( std::string );
  std::string              getDCB( std::string );
  std::vector<std::string> getDCBsAside();
  std::vector<std::string> getDCBsCside();
  std::string              getDCBQuadrant( std::string );
  std::vector<std::string> getDCBQuadrants();
  unsigned int             getASICNumber( std::string );
  std::string              getASICName( unsigned int );
  unsigned int             getRadialID( unsigned int );

private:
  std::vector<std::vector<std::vector<std::vector<std::vector<CoordinatesTuple>>>>> UT_CoordinatesMap;
  std::map<std::string, unsigned int>                                               UT_ModulesMap;
  std::map<unsigned int, std::string>                                               UT_TELL40Map;
  std::map<unsigned int, unsigned int>                                              UT_BoardIDtoSourceID;
  std::multimap<std::string, std::string>                                           UT_TELL40toModuleName;
  std::map<unsigned int, unsigned int>                                              UT_TELL40BinsASide_Flow0;
  std::map<unsigned int, unsigned int>                                              UT_TELL40BinsCSide_Flow0;
  std::map<unsigned int, unsigned int>                                              UT_TELL40BinsASide_Flow1;
  std::map<unsigned int, unsigned int>                                              UT_TELL40BinsCSide_Flow1;
  std::map<std::string, std::string>                                                UT_FlavourMap;
  std::map<unsigned int, unsigned int>                                              UT_2_5_BinsMap_Flow0;
  std::map<unsigned int, unsigned int>                                              UT_2_5_BinsMap_Flow1;
  std::map<unsigned int, unsigned int>                                              UT_2_4_BinsMap_Flow0;
  std::map<unsigned int, unsigned int>                                              UT_2_4_BinsMap_Flow1;
  std::map<std::string, std::string>                                                UT_DCBMap;
  std::vector<std::string>                                                          UT_DCBsASide;
  std::vector<std::string>                                                          UT_DCBsCSide;
  std::vector<std::string>                                                          UT_DCBQuadrants;
  std::map<std::string, unsigned int>                                               UTASICMap;
  std::map<unsigned int, unsigned int>                                              UT_GlobalAsicIDtoRadialID;
};
