/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/*
 *  UTEmuRawDataMonitorAlgorithm.h
 *
 *  Created on: August, 2023
 *      Author: Wojciech Krupa (wokrupa@cern.ch)
 */

#pragma once

#include "Event/ODIN.h"
#include "Event/UTDigit.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiKernel/IEventProcessor.h"
#include "Kernel/IEventCounter.h"
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/UTDAQID.h"
#include "Kernel/UTDigitFun.h"
#include "LHCbAlgs/Consumer.h"
#include "UTDet/DeUTDetector.h"
#include "UTEmu/UTEmuVetraHelper.h"
#include "UTEmu/UTMaps.h"

using namespace LHCb;

enum class masks {
  channel = 0x000001ff,
  lane    = 0x00000e00,
  board   = 0x000ff000
}; /// Enumeration for use in  bit packing

template <masks m>
[[nodiscard]] static constexpr unsigned int extract( unsigned int i ) {
  constexpr auto b =
      __builtin_ctz( static_cast<unsigned int>( m ) ); // FIXME: C++20 replace __builtin_ctz with std::countr_zero
  return ( i & static_cast<unsigned int>( m ) ) >> b;
}

namespace UTEmu {

  class RawDataMonitorAlgorithm
      : public LHCb::Algorithm::Consumer<void( UTDigits const&, UTDigits const&, const LHCb::ODIN&,
                                               DeUTDetector const& ),
                                         LHCb::DetDesc::usesBaseAndConditions<GaudiTupleAlg, DeUTDetector>> {
  public:
    /// constructer
    RawDataMonitorAlgorithm( const std::string& name, ISvcLocator* svcloc )
        : Consumer{name,
                   svcloc,
                   {{"DigitLocation", UTEmu::UTDigitLocation::UTDigits},
                    {"ErrDigitLocation", UTEmu::UTDigitLocation::UTErrDigits},
                    {"ODINLocation", LHCb::ODINLocation::Default},
                    {"UTLocation", DeUTDetLocation::location()}}} {}

    ToolHandle<IUTReadoutTool> readoutTool{this, "ReadoutTool", "UTReadoutTool"};
    Gaudi::Property<float>     m_events{this, "Events", 100};

    StatusCode initialize() override;
    void       operator()( const UTDigits&, const UTDigits&, const LHCb::ODIN&, DeUTDetector const& ) const override;

    mutable UTMaps UTMap;

    mutable std::map<std::string, Gaudi::Accumulators::Histogram<2>> m_2d_ADCCounter;
    mutable std::map<std::string, Gaudi::Accumulators::Histogram<2>> m_2d_occupancy;
    mutable std::map<std::string, Gaudi::Accumulators::Histogram<2>> m_2d_mcm_val;
    mutable std::map<std::string, Gaudi::Accumulators::Histogram<2>> m_2d_mcm_strip;

    mutable Gaudi::Accumulators::Histogram<2> m_2d_TELL40_vs_ASIC_A_0{
        this, "TELL40_vs_ASIC_ASide_Flow0", "TELL40_vs_ASIC_ASide_Flow0", {{54, 0, 54}, {24, 0, 24}}};
    mutable Gaudi::Accumulators::Histogram<2> m_2d_TELL40_vs_ASIC_C_0{
        this, "TELL40_vs_ASIC_CSide_Flow0", "TELL40_vs_ASIC_CSide_Flow0", {{54, 0, 54}, {24, 0, 24}}};
    mutable Gaudi::Accumulators::Histogram<2> m_2d_TELL40_vs_ASIC_A_1{
        this, "TELL40_vs_ASIC_ASide_Flow1", "TELL40_vs_ASIC_ASide_Flow1", {{54, 0, 54}, {24, 0, 24}}};
    mutable Gaudi::Accumulators::Histogram<2> m_2d_TELL40_vs_ASIC_C_1{
        this, "TELL40_vs_ASIC_CSide_Flow1", "TELL40_vs_ASIC_CSide_Flow1", {{54, 0, 54}, {24, 0, 24}}};

  private:
    void fillHistograms( const LHCb::UTDigit*, DeUTDetector const&, const LHCb::ODIN& ) const;
    void fillTrimTuples( const LHCb::UTDigit*, Tuple& ) const;
  };
} // namespace UTEmu