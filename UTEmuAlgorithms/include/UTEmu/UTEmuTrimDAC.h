/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/*
 *  UTEmuTrimDAC.h
 *
 *  Created on: January, 2024
 *      Author: Mingjie Feng (mingjie.feng@cern.ch)
 */

#pragma once

#include "Event/ODIN.h"
#include "Event/UTDigit.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiKernel/IEventProcessor.h"
#include "Kernel/IEventCounter.h"
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/UTDAQID.h"
#include "LHCbAlgs/Consumer.h"
#include "UTEmu/UTEmuVetraHelper.h"
#include "UTEmu/UTMaps.h"
#include <iomanip>
#include <string>
#include <vector>

using namespace LHCb;

namespace UTEmu {

  class TrimDACAlgorithm : public LHCb::Algorithm::Consumer<void( UTDigits const&, UTDigits const&, const LHCb::ODIN& ),
                                                            LHCb::DetDesc::usesBaseAndConditions<GaudiTupleAlg>> {
  public:
    /// constructer
    TrimDACAlgorithm( const std::string& name, ISvcLocator* svcloc )
        : Consumer{name,
                   svcloc,
                   {{"DigitLocation", UTEmu::UTDigitLocation::UTDigits},
                    {"ErrDigitLocation", UTEmu::UTDigitLocation::UTErrDigits},
                    {"ODINLocation", LHCb::ODINLocation::Default}}} {}

    ToolHandle<IUTReadoutTool> readoutTool{this, "ReadoutTool", "UTReadoutTool"};

    Gaudi::Property<std::string> m_layer{this, "Layer", "UTaX"};

    StatusCode initialize() override;
    void       operator()( const UTDigits&, const UTDigits&, const LHCb::ODIN& ) const override;

    mutable UTMaps UTMap;

    mutable std::map<std::string, Gaudi::Accumulators::Histogram<1>> m_1d_tell40;

    mutable std::vector<std::string>   m_layerName   = {"UTaX", "UTaU", "UTbV", "UTbX"};
    mutable std::map<std::string, int> m_layerNumber = {{"UTaX", 0}, {"UTaU", 1}, {"UTbV", 2}, {"UTbX", 3}};

  private:
    // fill ADC value
    void fillTrimTuples( const LHCb::UTDigit*, Tuple& ) const;
  };
} // namespace UTEmu
