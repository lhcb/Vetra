/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/*
 * UTEmuPedestalSubtractor.h
 *
 *  Created on: August, 2023
 *      Author: Wojciech Krupa (wokrupa@cern.ch)
 */

#pragma once
#include "Event/UTDigit.h"
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/UTDAQBoard.h"
#include "Kernel/UTDAQDefinitions.h"
#include "Kernel/UTDAQID.h"
#include "LHCbAlgs/Transformer.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TKey.h"
#include "UTDAQ/UTADCWord.h"
#include "UTDAQ/UTHeaderWord.h"
#include "UTEmu/UTEmuVetraHelper.h"
#include <fstream>
#include <iostream>
#include <map>
#include <math.h>

using namespace LHCb;

namespace UTEmu {

  class PedestalSubtractor
      : public LHCb::Algorithm::Transformer<LHCb::UTDigits( const LHCb::UTDigits&, const LHCb::UTDigits& )> {
  public:
    /// Standard constructor
    PedestalSubtractor( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer{name,
                      pSvcLocator,
                      {{"DigitLocation", UTEmu::UTDigitLocation::UTDigits},
                       {"ErrDigitLocation", UTEmu::UTDigitLocation::UTErrDigits}},
                      {{"OutputDigitData", UTEmu::UTDigitLocation::UTPedestalDigits}}} {}

    Gaudi::Property<std::string> m_runNumber{this, "RunNumber", "00000000"};

    StatusCode getPedestals();
    StatusCode getSigmaNoise();
    StatusCode initialize() override;
    StatusCode finalize() override;

    LHCb::UTDigits operator()( const LHCb::UTDigits&, const LHCb::UTDigits& ) const override; ///< Algorithm execution

    // properties
    mutable std::map<unsigned int, float> Pedestals;
    mutable std::map<unsigned int, float> sigmaNoiseMean;
    mutable std::map<unsigned int, float> sigmaNoiseRMS;

  private:
  };
} // namespace UTEmu