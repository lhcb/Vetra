/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/*
 *  UTEmuStepMonitorAlgorithm.cpp
 *
 *  Created on: October, 2024
 *      Author: Wojciech Krupa (wokrupa@cern.ch)
 */

#include "UTEmu/UTEmuStepMonitorAlgorithm.h"

using namespace LHCb;
using namespace UTEmu;

StatusCode StepMonitorAlgorithm::initialize() {

  return Consumer::initialize().andThen( [&] {
    for ( const auto& module : UTMap.getModulesNames() ) {
      for ( unsigned int i = 0; i < m_steps; i++ ) {
        auto title = module + ".Step" + std::to_string( i );
        Utility::map_emplace( m_2d_ADCCounter, title, this, title, {512, 0, 512}, {64, -32.5, 31.5} );
      }
    }
    return StatusCode::SUCCESS;
  } );
}

void StepMonitorAlgorithm::operator()( const UTDigits& digitsCont, const UTDigits& errdigitsCont,
                                       const LHCb::ODIN& odin, DeUTDetector const& det ) const {

  // fill histos for each digit
  for ( const auto& d : digitsCont ) fillHistograms( d, det, odin );
  for ( const auto& d : errdigitsCont ) fillHistograms( d, det, odin );
}

void StepMonitorAlgorithm::fillHistograms( const LHCb::UTDigit* aDigit, DeUTDetector const&,
                                           const LHCb::ODIN&    odin ) const {

  // aDigit (channelID) .stave() / module() = software side

  // Let's see where we are
  auto tuple = UTMap.getTuple( aDigit->module(), aDigit->face(), aDigit->stave(), aDigit->side(), aDigit->sector() );
  auto x     = std::get<0>( tuple );
  auto y     = std::get<1>( tuple );
  std::string module_name = std::get<4>( tuple );
  std::string type        = std::get<5>( tuple );
  if ( type == "D" ) type = "C";

  unsigned int asic = aDigit->asic() % UTEmu::UTNumbers::nASICs;

  Detector::UT::ChannelID channelID = aDigit->channelID();
  UTDAQID                 daqID     = readoutTool->channelIDToDAQID( channelID );
  auto                    board_ID  = daqID.board();

  // Digits occupancy plots
  if ( aDigit->strip() == 0 ) {
    debug() << " Stave: " << aDigit->stave() << " Side: " << aDigit->side() << " Module: " << aDigit->module()
            << " Sector: " << aDigit->sector() << " Face: " << aDigit->face() << " Module name: " << module_name << " "
            << "Type: " << type << " Stave_x: " << x << " Module_y: " << y << endmsg;
  }

  auto title_layer = "Occupancy_" + UTEmu::UT_layers[aDigit->layer()];

  // Let's include mirroring! Probably it could be done better
  if ( ( aDigit->face() == 1 && y < 0 && x < 0 ) || ( aDigit->face() == 0 && y > 0 && x < 0 ) ||
       ( aDigit->face() == 0 && y < 0 && x > 0 ) || ( aDigit->face() == 1 && y > 0 && x > 0 ) )
    asic = ( 3 - asic ) % 4;

  // Raw ADC vs channel
  auto title_charge =
      UTEmu::UT_layers[aDigit->layer()] + "_" + module_name + ".Step" + std::to_string( odin.calibrationStep() );

  ++m_2d_ADCCounter.at( title_charge )[{aDigit->strip(), aDigit->depositedCharge()}];
}
