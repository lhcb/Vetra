/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/*
 *  UTEmuRawDataMonitorAlgorithm.cpp
 *
 *  Created on: August, 2023
 *      Author: Wojciech Krupa (wokrupa@cern.ch)
 */

#include "UTEmu/UTEmuRawDataMonitorAlgorithm.h"

using namespace LHCb;
using namespace UTEmu;

StatusCode RawDataMonitorAlgorithm::initialize() {

  return Consumer::initialize().andThen( [&] {
    // Book 2D per hybrid raw ADC plot
    for ( const auto& module : UTMap.getModulesNames() ) {
      Utility::map_emplace( m_2d_ADCCounter, module, this, module, {512, 0, 512}, {64, -32.5, 31.5} );
    }

    // Book 2D occupancy, mcm_val, mcm_strip plot
    for ( const auto& layer : UTEmu::UT_layers ) {
      auto title = "Occupancy_" + layer;
      Utility::map_emplace( m_2d_occupancy, title, this, title, {144, -9, 9}, {28, -7, 7} );

      title = "MCM_val_" + layer;
      Utility::map_emplace( m_2d_mcm_val, title, this, title, {144, -9, 9}, {28, -7, 7} );

      title = "MCM_strip_" + layer;
      Utility::map_emplace( m_2d_mcm_strip, title, this, title, {144, -9, 9}, {28, -7, 7} );
    }
  } );
}

void RawDataMonitorAlgorithm::operator()( const UTDigits& digitsCont, const UTDigits& errdigitsCont,
                                          const LHCb::ODIN& odin, DeUTDetector const& det ) const {

  // fill histos for each digit
  Tuple tuple = nTuple( "RawADC", "RawADC" );
  for ( const auto& d : digitsCont ) {

    fillHistograms( d, det, odin );
    fillTrimTuples( d, tuple );
  }

  for ( const auto& d : errdigitsCont ) {

    fillHistograms( d, det, odin );
    fillTrimTuples( d, tuple );
  }
  tuple->write().ignore();
}

void RawDataMonitorAlgorithm::fillTrimTuples( const LHCb::UTDigit* aDigit, Tuple& tuple ) const {
  std::string module_name = std::get<4>(
      UTMap.getTuple( aDigit->module(), aDigit->face(), aDigit->stave(), aDigit->side(), aDigit->sector() ) );
  auto name = UTEmu::UT_layers[aDigit->layer()] + "_" + module_name + ".Chip" + std::to_string( aDigit->asic() % 4 );
  tuple->column( name, (short)aDigit->depositedCharge() ).ignore();
}

void RawDataMonitorAlgorithm::fillHistograms( const LHCb::UTDigit* aDigit, DeUTDetector const&,
                                              const LHCb::ODIN&    odin ) const {

  // aDigit (channelID) .stave() / module() = software side

  // Let's see where we are
  auto tuple = UTMap.getTuple( aDigit->module(), aDigit->face(), aDigit->stave(), aDigit->side(), aDigit->sector() );
  auto x     = std::get<0>( tuple );
  auto y     = std::get<1>( tuple );
  std::string module_name = std::get<4>( tuple );
  std::string type        = std::get<5>( tuple );
  if ( type == "D" ) type = "C";

  unsigned int asic = aDigit->asic() % UTEmu::UTNumbers::nASICs;

  Detector::UT::ChannelID channelID = aDigit->channelID();
  UTDAQID                 daqID     = readoutTool->channelIDToDAQID( channelID );
  auto                    board_ID  = daqID.board();

  // Digits occupancy plots
  if ( aDigit->strip() == 0 ) {
    debug() << " Stave: " << aDigit->stave() << " Side: " << aDigit->side() << " Module: " << aDigit->module()
            << " Sector: " << aDigit->sector() << " Face: " << aDigit->face() << " Module name: " << module_name << " "
            << "Type: " << type << " Stave_x: " << x << " Module_y: " << y << endmsg;
  }

  auto title_layer = "Occupancy_" + UTEmu::UT_layers[aDigit->layer()];

  // Let's include mirroring! Probably it could be done better
  if ( ( aDigit->face() == 1 && y < 0 && x < 0 ) || ( aDigit->face() == 0 && y > 0 && x < 0 ) ||
       ( aDigit->face() == 0 && y < 0 && x > 0 ) || ( aDigit->face() == 1 && y > 0 && x > 0 ) )
    asic = ( 3 - asic ) % 4;

  if ( type == "A" ) {
    ++m_2d_occupancy.at( title_layer )[{x + asic * 0.25 - 0.5, y - 0.5}];
    ++m_2d_occupancy.at( title_layer )[{x + asic * 0.25 - 0.5 + 0.125, y - 0.5}];
    ++m_2d_occupancy.at( title_layer )[{x + asic * 0.25 - 0.5, y}];
    ++m_2d_occupancy.at( title_layer )[{x + asic * 0.25 - 0.5 + 0.125, y}];
  }
  if ( type == "B" ) {
    ++m_2d_occupancy.at( title_layer )[{x + asic * 0.125 - 0.25, y - 0.5}];
    ++m_2d_occupancy.at( title_layer )[{x + asic * 0.125 - 0.25, y}];
  }
  if ( type == "C" ) { ++m_2d_occupancy.at( title_layer )[{x + asic * 0.125 - 0.25, y}]; }

  // Raw ADC vs channel
  auto title_charge  = UTEmu::UT_layers[aDigit->layer()] + "_" + module_name;
  auto title_charge_ = UTEmu::UT_layers[aDigit->layer()] + "_" + module_name + ";Channel;ADC";

  ++m_2d_ADCCounter.at( title_charge )[{aDigit->strip(), aDigit->depositedCharge()}];

  // TELL40 vs ASIC plots
  auto sourceID  = UTMap.getSourceIDfromBoardNumber( board_ID );
  int  SourceID_ = sourceID / 1000;

  unsigned int asic_times_lane =
      aDigit->asic() % UTEmu::UTNumbers::nASICs + UTEmu::UTNumbers::nASICs * extract<masks::lane>( aDigit->getdaqID() );

  // This code fill TELL49 vs ASIC plot. We need to splitt it into 4 plots since each flow and each side has different
  // binning. For 4x3, 2x3 and Jx3 we can use the same binning for all plots. We need to check if this is correct.
  if ( aDigit->strip() % 128 == 0 ) {
    if ( UTMap.getTELL40( sourceID ).find( "UA" ) != std::string::npos ) {
      if ( SourceID_ == 10 ) {
        if ( UTMap.getFlavour( UTMap.getTELL40( sourceID ) ) == "2x4" ) {
          ++m_2d_TELL40_vs_ASIC_A_0[{UTMap.getTELL40Bin( sourceID ), UTMap.get2_4_Flow0_Bin( asic_times_lane )}];
        } else if ( UTMap.getFlavour( UTMap.getTELL40( sourceID ) ) == "2x5" ) {
          ++m_2d_TELL40_vs_ASIC_A_0[{UTMap.getTELL40Bin( sourceID ), UTMap.get2_5_Flow0_Bin( asic_times_lane )}];
        } else if ( UTMap.getFlavour( UTMap.getTELL40( sourceID ) ) == "4x3" ||
                    UTMap.getFlavour( UTMap.getTELL40( sourceID ) ) == "2x3" ||
                    UTMap.getFlavour( UTMap.getTELL40( sourceID ) ) == "Jx3" ) {
          ++m_2d_TELL40_vs_ASIC_A_0[{UTMap.getTELL40Bin( sourceID ), asic_times_lane}];
        }
      } else if ( SourceID_ == 11 ) {
        if ( UTMap.getFlavour( UTMap.getTELL40( sourceID ) ) == "2x4" ) {
          if ( asic_times_lane == 16 || asic_times_lane == 17 )
            ++m_2d_TELL40_vs_ASIC_A_0[{UTMap.getTELL40Bin( sourceID ), UTMap.get2_4_Flow0_Bin( asic_times_lane )}];
          else
            ++m_2d_TELL40_vs_ASIC_A_1[{UTMap.getTELL40Bin( sourceID ), UTMap.get2_4_Flow1_Bin( asic_times_lane )}];
        } else if ( UTMap.getFlavour( UTMap.getTELL40( sourceID ) ) == "2x5" )
          ++m_2d_TELL40_vs_ASIC_A_1[{UTMap.getTELL40Bin( sourceID ), UTMap.get2_5_Flow1_Bin( asic_times_lane )}];
        else if ( UTMap.getFlavour( UTMap.getTELL40( sourceID ) ) == "4x3" ||
                  UTMap.getFlavour( UTMap.getTELL40( sourceID ) ) == "2x3" ||
                  UTMap.getFlavour( UTMap.getTELL40( sourceID ) ) == "Jx3" )
          ++m_2d_TELL40_vs_ASIC_A_1[{UTMap.getTELL40Bin( sourceID ), asic_times_lane}];
      }

    } else if ( UTMap.getTELL40( sourceID ).find( "UC" ) != std::string::npos ) {
      if ( SourceID_ == 12 ) {
        if ( UTMap.getFlavour( UTMap.getTELL40( sourceID ) ) == "2x4" ) {
          ++m_2d_TELL40_vs_ASIC_C_0[{UTMap.getTELL40Bin( sourceID ), UTMap.get2_4_Flow0_Bin( asic_times_lane )}];
        } else if ( UTMap.getFlavour( UTMap.getTELL40( sourceID ) ) == "2x5" ) {
          ++m_2d_TELL40_vs_ASIC_C_0[{UTMap.getTELL40Bin( sourceID ), UTMap.get2_5_Flow0_Bin( asic_times_lane )}];
        } else if ( UTMap.getFlavour( UTMap.getTELL40( sourceID ) ) == "4x3" ||
                    UTMap.getFlavour( UTMap.getTELL40( sourceID ) ) == "2x3" ||
                    UTMap.getFlavour( UTMap.getTELL40( sourceID ) ) == "Jx3" ) {
          ++m_2d_TELL40_vs_ASIC_C_0[{UTMap.getTELL40Bin( sourceID ), asic_times_lane}];
        }
      }

      else if ( SourceID_ == 13 ) {
        if ( UTMap.getFlavour( UTMap.getTELL40( sourceID ) ) == "2x4" ) {
          if ( asic_times_lane == 16 || asic_times_lane == 17 )
            ++m_2d_TELL40_vs_ASIC_C_0[{UTMap.getTELL40Bin( sourceID ), UTMap.get2_4_Flow0_Bin( asic_times_lane )}];
          else
            ++m_2d_TELL40_vs_ASIC_C_1[{UTMap.getTELL40Bin( sourceID ), UTMap.get2_4_Flow1_Bin( asic_times_lane )}];
        } else if ( UTMap.getFlavour( UTMap.getTELL40( sourceID ) ) == "2x5" )
          ++m_2d_TELL40_vs_ASIC_C_1[{UTMap.getTELL40Bin( sourceID ), UTMap.get2_5_Flow1_Bin( asic_times_lane )}];
        else if ( UTMap.getFlavour( UTMap.getTELL40( sourceID ) ) == "4x3" ||
                  UTMap.getFlavour( UTMap.getTELL40( sourceID ) ) == "2x3" ||
                  UTMap.getFlavour( UTMap.getTELL40( sourceID ) ) == "Jx3" )
          ++m_2d_TELL40_vs_ASIC_C_1[{UTMap.getTELL40Bin( sourceID ), asic_times_lane}];
      }
    }
  }

  if ( odin.eventNumber() == 1 && aDigit->strip() % 128 == 0 ) {

    title_layer = "MCM_val_" + UTEmu::UT_layers[aDigit->layer()];

    if ( type == "A" ) {
      m_2d_mcm_val.at( title_layer )[{x + asic * 0.25 - 0.5, y - 0.5}] += abs( aDigit->mcmVal() );
      m_2d_mcm_val.at( title_layer )[{x + asic * 0.25 - 0.5 + 0.125, y - 0.5}] += abs( aDigit->mcmVal() );
      m_2d_mcm_val.at( title_layer )[{x + asic * 0.25 - 0.5, y}] += abs( aDigit->mcmVal() );
      m_2d_mcm_val.at( title_layer )[{x + asic * 0.25 - 0.5 + 0.125, y}] += abs( aDigit->mcmVal() );
    }
    if ( type == "B" ) {
      m_2d_mcm_val.at( title_layer )[{x + asic * 0.125 - 0.25, y - 0.5}] += abs( aDigit->mcmVal() );
      m_2d_mcm_val.at( title_layer )[{x + asic * 0.125 - 0.25, y}] += abs( aDigit->mcmVal() );
    }
    if ( type == "C" ) { m_2d_mcm_val.at( title_layer )[{x + asic * 0.125 - 0.25, y}] += abs( aDigit->mcmVal() ); }

    title_layer = "MCM_strip_" + UTEmu::UT_layers[aDigit->layer()];

    if ( type == "A" ) {
      m_2d_mcm_strip.at( title_layer )[{x + asic * 0.25 - 0.5, y - 0.5}] += aDigit->mcmStrip();
      m_2d_mcm_strip.at( title_layer )[{x + asic * 0.25 - 0.5 + 0.125, y - 0.5}] += aDigit->mcmStrip();
      m_2d_mcm_strip.at( title_layer )[{x + asic * 0.25 - 0.5, y}] += aDigit->mcmStrip();
      m_2d_mcm_strip.at( title_layer )[{x + asic * 0.25 - 0.5 + 0.125, y}] += aDigit->mcmStrip();
    }
    if ( type == "B" ) {
      m_2d_mcm_strip.at( title_layer )[{x + asic * 0.125 - 0.25, y - 0.5}] += aDigit->mcmStrip();
      m_2d_mcm_strip.at( title_layer )[{x + asic * 0.125 - 0.25, y}] += aDigit->mcmStrip();
    }
    if ( type == "C" ) { m_2d_mcm_strip.at( title_layer )[{x + asic * 0.125 - 0.25, y}] += aDigit->mcmStrip(); }
  }
}
