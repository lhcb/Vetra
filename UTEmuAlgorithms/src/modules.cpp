/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include <UTEmu/UTEmuCommonModeSubtractor.h>
#include <UTEmu/UTEmuPedestalCalculator.h>
#include <UTEmu/UTEmuPedestalCalculatorDataMonitorAlgorithm.h>
#include <UTEmu/UTEmuPedestalCalculatorStep.h>
#include <UTEmu/UTEmuPedestalSubtractor.h>
#include <UTEmu/UTEmuPedestalSubtractorDataMonitorAlgorithm.h>
#include <UTEmu/UTEmuPulseShape.h>
#include <UTEmu/UTEmuRawDataMonitorAlgorithm.h>
#include <UTEmu/UTEmuStepMonitorAlgorithm.h>
#include <UTEmu/UTEmuTrimDAC.h>

DECLARE_COMPONENT( UTEmu::RawDataMonitorAlgorithm )
DECLARE_COMPONENT( UTEmu::StepMonitorAlgorithm )
DECLARE_COMPONENT( UTEmu::TrimDACAlgorithm )
DECLARE_COMPONENT( UTEmu::PedestalCalculator )
DECLARE_COMPONENT( UTEmu::PedestalCalculatorStep )
DECLARE_COMPONENT( UTEmu::PedestalSubtractor )
DECLARE_COMPONENT( UTEmu::PedestalCalculatorDataMonitorAlgorithm )
DECLARE_COMPONENT( UTEmu::PedestalSubtractorDataMonitorAlgorithm )
DECLARE_COMPONENT( UTEmu::CommonModeSubtractor )
DECLARE_COMPONENT( UTEmu::PulseShape )
