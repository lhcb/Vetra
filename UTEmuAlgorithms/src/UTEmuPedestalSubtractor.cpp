/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/*
 * UTEmuPedestalSubtractor.cpp
 *
 *  Created on: August, 2023
 *      Author: Wojciech Krupa (wokrupa@cern.ch)
 */

#include "UTEmu/UTEmuPedestalSubtractor.h"

using namespace LHCb;
using namespace UTEmu;

StatusCode PedestalSubtractor::initialize() {

  auto sc = getPedestals();
  if ( sc != StatusCode::SUCCESS ) return StatusCode::FAILURE;

  sc = getSigmaNoise();
  if ( sc != StatusCode::SUCCESS ) return StatusCode::FAILURE;

  return StatusCode::SUCCESS;
}

// Get pedestals from pedestal file
StatusCode PedestalSubtractor::getPedestals() {

  // File pointer
  std::fstream fin;

  // Open an existing file
  std::string input_path = std::string( UTEMU_PATH ) + m_runNumber + "/pedestals_" + m_runNumber + ".csv";
  fin.open( input_path.c_str(), std::ios::in );

  if ( fin ) { info() << "Found pedestal file!" << endmsg; }
  // Opening may fail, always check.
  if ( !fin ) {
    info() << "FATAL: Pedestal file is missing" << endmsg;
    return StatusCode::FAILURE;
  }

  unsigned int channelID;
  float        pedestal;

  std::string line;
  std::string delimiter = ",";

  while ( getline( fin, line ) ) {

    unsigned int channelID = std::stof( ( line.substr( 0, line.find( delimiter ) ).c_str() ) );
    line.erase( 0, line.find( delimiter ) + 1 );
    auto pedestal = std::atof( line.c_str() );
    debug() << "Getting pedestal for: " << std::fixed << channelID << " " << pedestal << endmsg;
    Pedestals[channelID] = pedestal;
  }

  // Close the file:
  fin.close();

  return StatusCode::SUCCESS;
}

// Get sigma of noise from sigmaNoise file
StatusCode PedestalSubtractor::getSigmaNoise() {

  std::fstream fin;

  std::string input_path = std::string( UTEMU_PATH ) + m_runNumber + "/sigmaNoise_" + m_runNumber + ".csv";
  fin.open( input_path.c_str(), std::ios::in );

  if ( fin ) { info() << "Found sigmaNoise file!" << endmsg; }
  if ( !fin ) {
    info() << "FATAL: sigmaNoise file is missing" << endmsg;
    return StatusCode::FAILURE;
  }

  unsigned int channelID;
  float        sigmaNoise;

  std::string line;
  std::string delimiter = ",";

  while ( getline( fin, line ) ) {
    unsigned int channelID = atof( line.substr( 0, line.find( delimiter ) ).c_str() );
    line.erase( 0, line.find( delimiter ) + 1 );
    auto noise_sigma         = atof( line.c_str() );
    sigmaNoiseRMS[channelID] = noise_sigma;
  }

  fin.close();

  return StatusCode::SUCCESS;
}

LHCb::UTDigits PedestalSubtractor::operator()( const LHCb::UTDigits& digits, const LHCb::UTDigits& errdigits ) const {

  LHCb::UTDigits PedestalSubtractedDigits;
  for ( const auto& digit : digits ) {

    auto channelID = digit->channelID();

    if ( fabs( digit->depositedCharge() - Pedestals[channelID.channelID()] ) >
         std::max( 2.0, static_cast<double>( 3 * sigmaNoiseRMS[channelID.channelID()] ) ) )
      continue; // eliminate outliers +-5/10 5sigma cut

    PedestalSubtractedDigits.insert( new UTDigit( channelID,
                                                  digit->depositedCharge() - Pedestals[channelID.channelID()],
                                                  digit->mcmVal(), digit->mcmStrip() ),
                                     channelID );
  }
  for ( const auto& digit : errdigits ) {

    auto channelID = digit->channelID();

    if ( fabs( digit->depositedCharge() - Pedestals[channelID.channelID()] ) >
         std::max( 2.0, static_cast<double>( 3 * sigmaNoiseRMS[channelID.channelID()] ) ) )
      continue; // eliminate outliers +-5/10 5sigma cut

    PedestalSubtractedDigits.insert( new UTDigit( channelID,
                                                  digit->depositedCharge() - Pedestals[channelID.channelID()],
                                                  digit->mcmVal(), digit->mcmStrip() ),
                                     channelID );
  }

  return PedestalSubtractedDigits;
}

StatusCode PedestalSubtractor::finalize() { return StatusCode::SUCCESS; }
