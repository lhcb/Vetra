/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/*
 * UTEmuPedestalCalculator.cpp
 *
 *  Created on: August, 2023
 *      Author: Wojciech Krupa (wokrupa@cern.ch)
 */

#include "UTEmu/UTEmuPedestalCalculator.h"

using namespace LHCb;
using namespace UTEmu;

StatusCode PedestalCalculator::initialize() { return StatusCode::SUCCESS; }

void PedestalCalculator::operator()() const {}

void PedestalCalculator::calculatePedestals() {

  TFile* f1 = TFile::Open(
      ( std::string( UTEMU_PATH ) + m_runNumber + "/ut_data_nzs_" + std::string( m_runNumber ) + ".root" ).c_str() );

  if ( !f1 ) {
    std::cerr << "Error: ut_data_nzs.root file does not exist." << std::endl;
    exit( -1 );
  }
  static TString classname_th2( "TH2D" );

  TDirectory* f2 = (TDirectory*)f1->Get( "UTDigitMonitor" );

  if ( f2 == nullptr ) {
    std::cerr << "Error: UTDigitMonitor directory not found in the file." << std::endl;
    exit( -1 );
  }

  // Description of method: read 2D histogram and after removing outliers from the projection for each bin (channel) get
  // the mu.

  for ( const auto& module : UTMap.getModulesNames() ) {

    TH2D* hist2D = dynamic_cast<TH2D*>( f2->Get( module.c_str() ) );
    if ( hist2D ) {

      for ( unsigned int asic = 0; asic < UTEmu::UTNumbers::nASICs; ++asic ) {
        int start_bin = asic * UTEmu::UTNumbers::nStripsPerASIC + 1; // ROOT bins start at 1
        int end_bin   = ( asic + 1 ) * UTEmu::UTNumbers::nStripsPerASIC;

        TH1D* projection = hist2D->ProjectionY( "Projection", start_bin, end_bin, "" );
        if ( projection->GetEntries() == 0 ) { disabledASICs.push_back( module + ".Chip" + std::to_string( asic ) ); }
      }
      for ( int bin = 1; bin <= hist2D->GetNbinsX(); bin++ ) {

        TH1D* projection = hist2D->ProjectionY( "Projection", bin, bin, "" );
        float mean       = projection->GetMean();
        float rms        = projection->GetRMS();

        // Filter out outliers
        for ( Int_t bin = 1; bin <= projection->GetNbinsX(); ++bin ) {
          if ( TMath::Abs( projection->GetBinCenter( bin ) - mean ) > 3 * rms ) { projection->SetBinContent( bin, 0 ); }
        }

        Pedestal[UTMap.getChannel( module ) + ( bin - 1 )]   = projection->GetMean();
        SigmaNoise[UTMap.getChannel( module ) + ( bin - 1 )] = projection->GetRMS();

        Detector::UT::ChannelID channelID( UTMap.getChannel( module ) + ( bin - 1 ) );

        auto tuple = UTMap.getTuple( channelID.module(), channelID.face(), channelID.stave(), channelID.side(),
                                     channelID.sector() );

        std::string module_name = std::get<4>( tuple );

        std::string current_asic_name = UTEmu::UT_layers[channelID.layer()] + "_" + module_name + "_" +
                                        std::to_string( int( channelID.strip() / UTEmu::UTNumbers::nStripsPerASIC ) );

        SigmaNoiseAv[current_asic_name] += projection->GetRMS();
        PedestalAv[current_asic_name] += projection->GetMean();

        delete projection;
      }
    } else {
      info() << "Very serious Problem" << endmsg;
    }
  }
}

void PedestalCalculator::findBadChannels() {

  // bad channels
  for ( const auto& ped : Pedestal ) {

    Detector::UT::ChannelID channelID( ped.first );

    auto tuple =
        UTMap.getTuple( channelID.module(), channelID.face(), channelID.stave(), channelID.side(), channelID.sector() );

    std::string module_name = std::get<4>( tuple );

    std::string current_asic_name = UTEmu::UT_layers[channelID.layer()] + "_" + module_name + "_" +
                                    std::to_string( int( channelID.strip() / UTEmu::UTNumbers::nStripsPerASIC ) );

    float zs_th = std::ceil( 4.5 * SigmaNoiseAv[current_asic_name].mean() );
    if ( std::round( std::abs( ped.second ) ) > zs_th )
      BadChannels[ped.first] = 1;
    else
      BadChannels[ped.first] = 0;
  }
}

void PedestalCalculator::saveBadChannelsToCsv() {

  // file pointer
  std::fstream fout;

  // opens an existing csv file or creates a new file.
  std::string input_path = std::string( UTEMU_PATH ) + m_runNumber + "/above_thresholds_" + m_runNumber + ".csv";
  fout.open( input_path.c_str(), std::ios::out | std::ios::trunc );

  for ( const auto& channel : BadChannels ) { fout << channel.first << ", " << channel.second << "\n"; };
  info() << "Saving channels above threshold file" << endmsg;
}

void PedestalCalculator::saveThresholdsToCsv() {

  std::fstream fout;

  std::string input_path = std::string( UTEMU_PATH ) + m_runNumber + "/zs_th_mcms_" + m_runNumber + ".csv";
  fout.open( input_path.c_str(), std::ios::out | std::ios::trunc );

  for ( const auto& sigmaNoise : SigmaNoiseAv ) {
    float zs_th = std::ceil( 5 * sigmaNoise.second.mean() ) - 1; // Tomasz optimistic version
    fout << sigmaNoise.first << ", " << zs_th << "\n";
  }
  info() << "Saving zs_threshold file" << endmsg;
};

void PedestalCalculator::saveDisabledToCsv() {

  std::fstream fout;

  std::string input_path = std::string( UTEMU_PATH ) + m_runNumber + "/disabled_" + m_runNumber + ".csv";
  fout.open( input_path.c_str(), std::ios::out | std::ios::trunc );

  for ( const auto& asic : disabledASICs ) { fout << asic << "\n"; }
  info() << "Saving disabled asics file" << endmsg;
};

void PedestalCalculator::savePedestalsToCsv() {

  // file pointer
  std::fstream fout;

  // opens an existing csv file or creates a new file.
  std::string input_path = std::string( UTEMU_PATH ) + m_runNumber + "/pedestals_" + m_runNumber + ".csv";
  fout.open( input_path.c_str(), std::ios::out | std::ios::trunc );

  for ( const auto& ped : Pedestal ) { fout << ped.first << ", " << std::round( ped.second ) << "\n"; };
  info() << "Saving pedestal file" << endmsg;
}

void PedestalCalculator::saveSigmaNoiseToCsv() {

  std::fstream fout;

  std::string input_path = std::string( UTEMU_PATH ) + m_runNumber + "/sigmaNoise_" + m_runNumber + ".csv";
  fout.open( input_path.c_str(), std::ios::out | std::ios::trunc );

  for ( const auto& sigmaNoise : SigmaNoise ) {
    fout << sigmaNoise.first << ", " << Pedestal[sigmaNoise.first] << ", " << sigmaNoise.second << "\n";
  }

  info() << "Saving sigma of sigmaNoise file" << endmsg;
};

StatusCode PedestalCalculator::finalize() {

  calculatePedestals();
  savePedestalsToCsv();
  saveSigmaNoiseToCsv();
  findBadChannels();
  saveBadChannelsToCsv();
  saveThresholdsToCsv();
  saveDisabledToCsv();

  return StatusCode::SUCCESS;
}
