/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/*
 *  UTEmuPedestalCalculatorDataMonitorAlgorithm.cpp
 *
 *  Created on: August, 2023
 *      Author: Wojciech Krupa (wokrupa@cern.ch)
 */

#include "UTEmu/UTEmuPedestalCalculatorDataMonitorAlgorithm.h"

using namespace LHCb;
using namespace UTEmu;

template <typename T, typename OWNER>
void vector_emplace( T& t, typename T::key_type key, OWNER* owner, std::string const& title ) {
  t.emplace( std::piecewise_construct, std::forward_as_tuple( key ), std::forward_as_tuple( owner, title ) );
}

StatusCode PedestalCalculatorDataMonitorAlgorithm::makePedestalsPlots() {

  auto sc = getPedestals();
  if ( sc != StatusCode::SUCCESS ) return StatusCode::FAILURE;

  for ( const auto& pedestal : Pedestals ) {

    Detector::UT::ChannelID channelID( pedestal.first );

    auto tuple =
        UTMap.getTuple( channelID.module(), channelID.face(), channelID.stave(), channelID.side(), channelID.sector() );

    auto        x           = std::get<0>( tuple );
    auto        y           = std::get<1>( tuple );
    std::string module_name = std::get<4>( tuple );
    std::string type        = std::get<5>( tuple );
    if ( type == "D" ) type = "C";

    // Pedestal 1D plots
    auto title_pedestal  = "MeanADC_" + UTEmu::UT_layers[channelID.layer()] + "_" + module_name;
    auto title_pedestal_ = "MeanADC_" + UTEmu::UT_layers[channelID.layer()] + "_" + module_name + ";Channel;ADC";

    plot1D( channelID.strip(), title_pedestal, title_pedestal_, 0, UTEmu::UTNumbers::nStrips, UTEmu::UTNumbers::nStrips,
            pedestal.second );

    std::string current_asic_name = UTEmu::UT_layers[channelID.layer()] + "_" + module_name + "_" +
                                    std::to_string( int( channelID.strip() / UTEmu::UTNumbers::nStripsPerASIC ) );

    Pedestal_Av.at( current_asic_name ) += pedestal.second;

    plot1D( pedestal.second, "Projection_MeanADC", "Projection_MeanADC", -31, 31, 62 );
    if ( type == "A" ) plot1D( pedestal.second, "Projection_MeanADC_A", "Projection_MeanADC_A", -31, 31, 62 );
    if ( type == "B" ) plot1D( pedestal.second, "Projection_MeanADC_B", "Projection_MeanADC_B", -31, 31, 62 );
    if ( type == "C" ) plot1D( pedestal.second, "Projection_MeanADC_C", "Projection_MeanADC_C", -31, 31, 62 );
  };

  for ( const auto& av : Pedestal_Av ) {

    // Let's transform ASIC name to x,y position!
    auto        asic_name = av.first;
    size_t      pos       = asic_name.rfind( '_' );
    std::string module    = asic_name.substr( 0, pos );
    std::string asic_str  = asic_name.substr( pos + 1 );
    int         asic      = std::stoi( asic_str );

    Detector::UT::ChannelID channelID( UTMap.getChannel( module ) );
    auto                    tuple =
        UTMap.getTuple( channelID.module(), channelID.face(), channelID.stave(), channelID.side(), channelID.sector() );

    auto        x    = std::get<0>( tuple );
    auto        y    = std::get<1>( tuple );
    std::string type = std::get<5>( tuple );

    if ( type == "D" ) type = "C";

    auto title_layer = "MeanADCAverage_" + UTEmu::UT_layers[channelID.layer()];

    // Let's include mirroring! Probably it could be done better
    if ( ( channelID.face() == 1 && y < 0 && x < 0 ) || ( channelID.face() == 0 && y > 0 && x < 0 ) ||
         ( channelID.face() == 0 && y < 0 && x > 0 ) || ( channelID.face() == 1 && y > 0 && x > 0 ) )
      asic = ( 3 - asic ) % 4;

    // Abs value for plotting purpose!
    // += 0.001 is protection agains white bin in case of average = 0
    auto val = TMath::Abs( av.second.mean() );
    if ( val == 0 && SigmaNoise_Av[asic_name].mean() != 0 ) val += 0.01;

    if ( type == "A" ) {
      plot2D( x + asic * 0.25 - 0.5, y - 0.5, title_layer, title_layer, -9, 9, -7, 7, 144, 28, val );
      plot2D( x + asic * 0.25 - 0.5 + 0.125, y - 0.5, title_layer, title_layer, -9, 9, -7, 7, 144, 28, val );
      plot2D( x + asic * 0.25 - 0.5, y, title_layer, title_layer, -9, 9, -7, 7, 144, 28, val );
      plot2D( x + asic * 0.25 - 0.5 + 0.125, y, title_layer, title_layer, -9, 9, -7, 7, 144, 28, val );
    }
    if ( type == "B" ) {
      plot2D( x + asic * 0.125 - 0.25, y - 0.5, title_layer, title_layer, -9, 9, -7, 7, 144, 28, val );
      plot2D( x + asic * 0.125 - 0.25, y, title_layer, title_layer, -9, 9, -7, 7, 144, 28, val );
    }
    if ( type == "C" ) { plot2D( x + asic * 0.125 - 0.25, y, title_layer, title_layer, -9, 9, -7, 7, 144, 28, val ); }
  }

  return StatusCode::SUCCESS;
}

StatusCode PedestalCalculatorDataMonitorAlgorithm::makeTresholdsPlots() {

  auto sc = getTresholds();
  if ( sc != StatusCode::SUCCESS ) return StatusCode::FAILURE;

  for ( const auto& av : ZS_th ) {

    // Let's transform ASIC name to x,y position!
    auto        asic_name = av.first;
    size_t      pos       = asic_name.rfind( '_' );
    std::string module    = asic_name.substr( 0, pos );
    std::string asic_str  = asic_name.substr( pos + 1 );
    int         asic      = std::stoi( asic_str );

    Detector::UT::ChannelID channelID( UTMap.getChannel( module ) );
    auto                    tuple =
        UTMap.getTuple( channelID.module(), channelID.face(), channelID.stave(), channelID.side(), channelID.sector() );

    auto        x    = std::get<0>( tuple );
    auto        y    = std::get<1>( tuple );
    std::string type = std::get<5>( tuple );

    if ( type == "D" ) type = "C";

    auto title_layer = "ZS_th_" + UTEmu::UT_layers[channelID.layer()];

    // Let's include mirroring! Probably it could be done better
    if ( ( channelID.face() == 1 && y < 0 && x < 0 ) || ( channelID.face() == 0 && y > 0 && x < 0 ) ||
         ( channelID.face() == 0 && y < 0 && x > 0 ) || ( channelID.face() == 1 && y > 0 && x > 0 ) )
      asic = ( 3 - asic ) % 4;

    auto val = av.second;

    if ( type == "A" ) {
      plot2D( x + asic * 0.25 - 0.5, y - 0.5, title_layer, title_layer, -9, 9, -7, 7, 144, 28, val );
      plot2D( x + asic * 0.25 - 0.5 + 0.125, y - 0.5, title_layer, title_layer, -9, 9, -7, 7, 144, 28, val );
      plot2D( x + asic * 0.25 - 0.5, y, title_layer, title_layer, -9, 9, -7, 7, 144, 28, val );
      plot2D( x + asic * 0.25 - 0.5 + 0.125, y, title_layer, title_layer, -9, 9, -7, 7, 144, 28, val );
    }
    if ( type == "B" ) {
      plot2D( x + asic * 0.125 - 0.25, y - 0.5, title_layer, title_layer, -9, 9, -7, 7, 144, 28, val );
      plot2D( x + asic * 0.125 - 0.25, y, title_layer, title_layer, -9, 9, -7, 7, 144, 28, val );
    }
    if ( type == "C" ) { plot2D( x + asic * 0.125 - 0.25, y, title_layer, title_layer, -9, 9, -7, 7, 144, 28, val ); }
  }

  return StatusCode::SUCCESS;
}

StatusCode PedestalCalculatorDataMonitorAlgorithm::makeSigmaNoisePlots() {

  auto sc = getSigmaNoise();
  if ( sc != StatusCode::SUCCESS ) return StatusCode::FAILURE;

  for ( const auto& noise : SigmaNoise ) {

    Detector::UT::ChannelID channelID( noise.first );

    auto tuple =
        UTMap.getTuple( channelID.module(), channelID.face(), channelID.stave(), channelID.side(), channelID.sector() );

    auto        x           = std::get<0>( tuple );
    auto        y           = std::get<1>( tuple );
    std::string module_name = std::get<4>( tuple );
    std::string type        = std::get<5>( tuple );
    if ( type == "D" ) type = "C";

    // SigmaNoise 1D plots
    auto title_SigmaNoise  = "SigmaNoise_" + UTEmu::UT_layers[channelID.layer()] + "_" + module_name;
    auto title_SigmaNoise_ = "SigmaNoise_" + UTEmu::UT_layers[channelID.layer()] + "_" + module_name + ";Channel;ADC";

    plot1D( channelID.strip(), title_SigmaNoise, title_SigmaNoise_, 0, UTEmu::UTNumbers::nStrips,
            UTEmu::UTNumbers::nStrips, noise.second );

    std::string current_asic_name = UTEmu::UT_layers[channelID.layer()] + "_" + module_name + "_" +
                                    std::to_string( int( channelID.strip() / UTEmu::UTNumbers::nStripsPerASIC ) );

    SigmaNoise_Av[current_asic_name] += noise.second;

    plot1D( noise.second, "Projection_SigmaNoise", "Projection_SigmaNoise", 0, 2, 100 );
    if ( type == "A" ) plot1D( noise.second, "Projection_SigmaNoise_A", "Projection_SigmaNoise_A", 0, 2, 100 );
    if ( type == "B" ) plot1D( noise.second, "Projection_SigmaNoise_B", "Projection_SigmaNoise_B", 0, 2, 100 );
    if ( type == "C" ) plot1D( noise.second, "Projection_SigmaNoise_C", "Projection_SigmaNoise_C", 0, 2, 100 );
  };

  for ( const auto& av : SigmaNoise_Av ) {

    // Let's transform ASIC name to x,y position!
    auto        asic_name = av.first;
    size_t      pos       = asic_name.rfind( '_' );
    std::string module    = asic_name.substr( 0, pos );
    std::string asic_str  = asic_name.substr( pos + 1 );
    int         asic      = std::stoi( asic_str );

    Detector::UT::ChannelID channelID( UTMap.getChannel( module ) );
    auto                    tuple =
        UTMap.getTuple( channelID.module(), channelID.face(), channelID.stave(), channelID.side(), channelID.sector() );

    auto        x    = std::get<0>( tuple );
    auto        y    = std::get<1>( tuple );
    std::string type = std::get<5>( tuple );

    if ( type == "D" ) type = "C";

    auto title_layer = "SigmaNoiseAverage_" + UTEmu::UT_layers[channelID.layer()];

    // Let's include mirroring! Probably it could be done better
    if ( ( channelID.face() == 1 && y < 0 && x < 0 ) || ( channelID.face() == 0 && y > 0 && x < 0 ) ||
         ( channelID.face() == 0 && y < 0 && x > 0 ) || ( channelID.face() == 1 && y > 0 && x > 0 ) )
      asic = ( 3 - asic ) % 4;

    auto val = av.second.mean();

    if ( type == "A" ) {
      plot2D( x + asic * 0.25 - 0.5, y - 0.5, title_layer, title_layer, -9, 9, -7, 7, 144, 28, val );
      plot2D( x + asic * 0.25 - 0.5 + 0.125, y - 0.5, title_layer, title_layer, -9, 9, -7, 7, 144, 28, val );
      plot2D( x + asic * 0.25 - 0.5, y, title_layer, title_layer, -9, 9, -7, 7, 144, 28, val );
      plot2D( x + asic * 0.25 - 0.5 + 0.125, y, title_layer, title_layer, -9, 9, -7, 7, 144, 28, val );
    }
    if ( type == "B" ) {
      plot2D( x + asic * 0.125 - 0.25, y - 0.5, title_layer, title_layer, -9, 9, -7, 7, 144, 28, val );
      plot2D( x + asic * 0.125 - 0.25, y, title_layer, title_layer, -9, 9, -7, 7, 144, 28, val );
    }
    if ( type == "C" ) { plot2D( x + asic * 0.125 - 0.25, y, title_layer, title_layer, -9, 9, -7, 7, 144, 28, val ); }
  }

  return StatusCode::SUCCESS;
}

StatusCode PedestalCalculatorDataMonitorAlgorithm::initialize() {

  for ( const auto& module : UTMap.getModulesNames() ) {

    for ( unsigned int i = 0; i < UTEmu::UTNumbers::nASICs; i++ ) {
      auto chip_name = module + "_" + std::to_string( i );
      auto title     = "MeanADCAv_" + chip_name;
      vector_emplace( Pedestal_Av, chip_name, this, title );
      title = "SigmaNoiseAv_" + chip_name;
      vector_emplace( SigmaNoise_Av, chip_name, this, title );
    }
  };

  return Consumer::initialize().andThen( [&] {
    StatusCode sc;
    // Do not change order
    sc = makeSigmaNoisePlots();
    if ( sc != StatusCode::SUCCESS ) return StatusCode::FAILURE;
    sc = makePedestalsPlots();
    if ( sc != StatusCode::SUCCESS ) return StatusCode::FAILURE;
    sc = makeTresholdsPlots();
    if ( sc != StatusCode::SUCCESS ) return StatusCode::FAILURE;

    return StatusCode::SUCCESS;
  } );
}

void PedestalCalculatorDataMonitorAlgorithm::operator()() const {}

StatusCode PedestalCalculatorDataMonitorAlgorithm::getSigmaNoise() {

  std::fstream fin;

  std::string input_path = std::string( UTEMU_PATH ) + m_runNumber + "/sigmaNoise_" + m_runNumber + ".csv";
  fin.open( input_path.c_str(), std::ios::in );

  if ( fin ) { info() << "Found noise file!" << endmsg; }
  if ( !fin ) {
    info() << "FATAL: Noise file is missing" << endmsg;
    return StatusCode::FAILURE;
  }

  unsigned int channelID;
  float        noise;

  std::string line;
  std::string delimiter = ",";

  while ( getline( fin, line ) ) {

    unsigned int channelID = atof( line.substr( 0, line.find( delimiter ) ).c_str() );
    line.erase( 0, line.find( delimiter ) + 1 );
    auto pedestal = atof( line.c_str() );
    line.erase( 0, line.find( delimiter ) + 1 );
    auto noise_sigma      = atof( line.c_str() );
    SigmaNoise[channelID] = noise_sigma;
  }

  fin.close();

  return StatusCode::SUCCESS;
}

StatusCode PedestalCalculatorDataMonitorAlgorithm::getTresholds() {

  std::fstream fin;

  std::string input_path = std::string( UTEMU_PATH ) + m_runNumber + "/zs_th_mcms_" + m_runNumber + ".csv";
  fin.open( input_path.c_str(), std::ios::in );

  if ( fin ) { info() << "Found treshold file!" << endmsg; }
  if ( !fin ) {
    info() << "FATAL: Treshold file is missing" << endmsg;
    return StatusCode::FAILURE;
  }

  unsigned int channelID;
  float        treshold;

  std::string line;
  std::string delimiter = ",";

  while ( getline( fin, line ) ) {

    auto asic = line.substr( 0, line.find( delimiter ) ).c_str();
    line.erase( 0, line.find( delimiter ) + 1 );
    auto treshold = atof( line.c_str() );
    ZS_th[asic]   = treshold;
  }

  fin.close();

  return StatusCode::SUCCESS;
}

StatusCode PedestalCalculatorDataMonitorAlgorithm::getPedestals() {

  std::fstream fin;

  std::string input_path = std::string( UTEMU_PATH ) + m_runNumber + "/pedestals_" + m_runNumber + ".csv";
  fin.open( input_path.c_str(), std::ios::in );

  if ( fin ) { info() << "Found pedestal file!" << endmsg; }
  if ( !fin ) {
    info() << "FATAL: Pedestal file is missing" << endmsg;
    return StatusCode::FAILURE;
  }

  unsigned int channelID;
  float        pedestal;

  std::string line;
  std::string delimiter = ",";

  while ( getline( fin, line ) ) {

    unsigned int channelID = atof( line.substr( 0, line.find( delimiter ) ).c_str() );
    line.erase( 0, line.find( delimiter ) + 1 );
    auto pedestal        = atof( line.c_str() );
    Pedestals[channelID] = pedestal;
  }

  fin.close();

  return StatusCode::SUCCESS;
}

StatusCode PedestalCalculatorDataMonitorAlgorithm::finalize() { return GaudiHistoAlg::finalize(); }
