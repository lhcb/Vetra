/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/*
 *  UTEmuPulseShape.cpp
 *
 *  Created on: August, 2023
 *      Author: Wojciech Krupa (wokrupa@cern.ch)
 */

#include "UTEmu/UTEmuPulseShape.h"

using namespace LHCb;
using namespace UTEmu;

StatusCode PulseShape::initialize() {
  return Consumer::initialize().andThen( [&] {
    for ( const auto& module : UTMap.getModulesNames() ) {
      for ( unsigned int i = 0; i < 512; i++ ) {
        auto title = module + ".Chip" + std::to_string( i / 128 ) + ".Ch" + std::to_string( i % 128 );
        if ( title.find( UTEmu::UT_layers[m_layer] ) != std::string::npos ) {
          Utility::map_emplace( m_2d_ch, title, this, title, {128, 0, 128}, {64, -32.5, 31.5} );
        }
      }
    }
    return StatusCode::SUCCESS;
  } );
}

void PulseShape::operator()( const UTDigits& digitsCont, const UTDigits& errdigitsCont, const LHCb::ODIN& odin,
                             DeUTDetector const& det ) const {

  // fill histos for each digit
  for ( const auto& d : digitsCont ) fillHistograms( d, det, odin );
  for ( const auto& d : errdigitsCont ) fillHistograms( d, det, odin );
}

void PulseShape::fillHistograms( const LHCb::UTDigit* aDigit, DeUTDetector const&, const LHCb::ODIN& odin ) const {

  auto tuple = UTMap.getTuple( aDigit->module(), aDigit->face(), aDigit->stave(), aDigit->side(), aDigit->sector() );

  auto        x           = std::get<0>( tuple );
  auto        y           = std::get<1>( tuple );
  std::string module_name = std::get<4>( tuple );
  std::string type        = std::get<5>( tuple );
  if ( type == "D" ) type = "C";

  auto title_charge = UTEmu::UT_layers[aDigit->layer()] + "_" + module_name + ".Chip" +
                      std::to_string( aDigit->strip() / 128 ) + ".Ch" + std::to_string( aDigit->strip() % 128 );
  if ( title_charge.find( UTEmu::UT_layers[m_layer] ) != std::string::npos ) {
    ++m_2d_ch.at( title_charge )[{odin.calibrationStep(), aDigit->depositedCharge()}];
  }
}
