/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/*
 * UTEmuCommonModeSubtractor.cpp
 *
 *  Created on: August, 2023
 *      Author: Wojciech Krupa (wokrupa@cern.ch)
 */

#include "UTEmu/UTEmuCommonModeSubtractor.h"

using namespace LHCb;
using namespace UTEmu;

StatusCode CommonModeSubtractor::initialize() {

  // Initialize plots
  for ( const auto& module : UTMap.getModulesNames() ) {
    auto title = "CMS_" + module;
    Utility::map_emplace( m_2d_CMSADCCounter, title, this, title,
                          {UTEmu::UTNumbers::nStrips, 0, UTEmu::UTNumbers::nStrips},
                          {64, UTEmu::UTNumbers::ADC_range_low, UTEmu::UTNumbers::ADC_range_high} );
  }

  return StatusCode::SUCCESS;
}

void CommonModeSubtractor::makeCommonModePlots() const {

  // Convert Common Mode to average
  for ( const auto& CM : CommonModeAv ) {
    CommonModeMean[CM.first]  = CM.second.mean();
    CommonModeSigma[CM.first] = CM.second.standard_deviation();
  }

  // Plott averages
  plotAverage( CommonModeMean, "CommonModeMeanAverage_" );
  plotAverage( CommonModeSigma, "CommonModeSigmaAverage_" );
}

void CommonModeSubtractor::makeCMSNoisePlots() const {

  for ( const auto& CMSnoise : CMSNoise ) {

    Detector::UT::ChannelID channelID( CMSnoise.first );

    auto tuple =
        UTMap.getTuple( channelID.module(), channelID.face(), channelID.stave(), channelID.side(), channelID.sector() );

    auto        stave_x     = std::get<0>( tuple );
    auto        module_y    = std::get<1>( tuple );
    std::string module_name = std::get<4>( tuple );
    std::string type        = std::get<5>( tuple );
    if ( type == "D" ) type = "C"; // C & D goes together

    // CMSNoise 1D plots mean
    auto title_CMSNoise  = "CMSNoiseMean_" + UTEmu::UT_layers[channelID.layer()] + "_" + module_name;
    auto title_CMSNoise_ = "CMSNoiseMean_" + UTEmu::UT_layers[channelID.layer()] + "_" + module_name + ";Channel;ADC";

    plot1D( channelID.strip(), title_CMSNoise, title_CMSNoise_, 0, UTEmu::UTNumbers::nStrips, UTEmu::UTNumbers::nStrips,
            CMSnoise.second.mean() );

    // CMSNoise 1D plots std
    title_CMSNoise  = "CMSNoiseSigma_" + UTEmu::UT_layers[channelID.layer()] + "_" + module_name;
    title_CMSNoise_ = "CMSNoiseSigma_" + UTEmu::UT_layers[channelID.layer()] + "_" + module_name + ";Channel;ADC";

    plot1D( channelID.strip(), title_CMSNoise, title_CMSNoise_, 0, UTEmu::UTNumbers::nStrips, UTEmu::UTNumbers::nStrips,
            CMSnoise.second.standard_deviation() );

    plot1D( CMSnoise.second.standard_deviation(), "Projection_CMSNoiseSigma", "Projection_CMSNoiseSigma", 0, 2, 100 );

    if ( type == "A" )
      plot1D( CMSnoise.second.standard_deviation(), "Projection_CMSNoiseSigma_A", "Projection_CMSNoiseSigma_A", 0, 2,
              100 );
    if ( type == "B" )
      plot1D( CMSnoise.second.standard_deviation(), "Projection_CMSNoiseSigma_B", "Projection_CMSNoiseSigma_B", 0, 2,
              100 );
    if ( type == "C" )
      plot1D( CMSnoise.second.standard_deviation(), "Projection_CMSNoiseSigma_C", "Projection_CMSNoiseSigma_C", 0, 2,
              100 );
  }

  // Convert Common Mode to average
  for ( const auto& CMS : CMSNoiseAv ) {
    debug() << "CMS.first: " << CMS.first << " " << CMS.second.mean() << " " << CMS.second.standard_deviation()
            << endmsg;
    CMSNoiseMean[CMS.first]  = CMS.second.mean();
    CMSNoiseSigma[CMS.first] = CMS.second.standard_deviation();
  }

  // Plott averages
  plotAverage( CMSNoiseMean, "CMSNoiseMeanAverage_" );
  plotAverage( CMSNoiseSigma, "CMSNoiseSigmaAverage_" );
}
// Plot 2D histogram
void CommonModeSubtractor::plotAverage( const std::map<std::string, float>& data_av,
                                        const std::string&                  title_base ) const {
  for ( const auto& av : data_av ) {
    auto        asic_name = av.first;
    size_t      pos       = asic_name.rfind( '_' );
    std::string module    = asic_name.substr( 0, pos );
    std::string asic_str  = asic_name.substr( pos + 1 );
    int         asic      = std::stoi( asic_str );

    Detector::UT::ChannelID channelID( UTMap.getChannel( module ) );
    auto                    tuple =
        UTMap.getTuple( channelID.module(), channelID.face(), channelID.stave(), channelID.side(), channelID.sector() );

    auto        x    = std::get<0>( tuple );
    auto        y    = std::get<1>( tuple );
    std::string type = std::get<5>( tuple );

    if ( type == "D" ) type = "C";
    std::string title_layer = title_base + UTEmu::UT_layers[channelID.layer()];

    // Let's include mirroring! Probably it could be done better
    if ( ( channelID.face() == 1 && y < 0 && x < 0 ) || ( channelID.face() == 0 && y > 0 && x < 0 ) ||
         ( channelID.face() == 0 && y < 0 && x > 0 ) || ( channelID.face() == 1 && y > 0 && x > 0 ) )
      asic = ( 3 - asic ) % 4;

    auto val = av.second;

    if ( type == "A" ) {
      plot2D( x + asic * 0.25 - 0.5, y - 0.5, title_layer, title_layer, -9, 9, -7, 7, 144, 28, val );
      plot2D( x + asic * 0.25 - 0.5 + 0.125, y - 0.5, title_layer, title_layer, -9, 9, -7, 7, 144, 28, val );
      plot2D( x + asic * 0.25 - 0.5, y, title_layer, title_layer, -9, 9, -7, 7, 144, 28, val );
      plot2D( x + asic * 0.25 - 0.5 + 0.125, y, title_layer, title_layer, -9, 9, -7, 7, 144, 28, val );
    }
    if ( type == "B" ) {
      plot2D( x + asic * 0.125 - 0.25, y - 0.5, title_layer, title_layer, -9, 9, -7, 7, 144, 28, val );
      plot2D( x + asic * 0.125 - 0.25, y, title_layer, title_layer, -9, 9, -7, 7, 144, 28, val );
    }
    if ( type == "C" ) { plot2D( x + asic * 0.125 - 0.25, y, title_layer, title_layer, -9, 9, -7, 7, 144, 28, val ); }
  }
}
// Get ASIC name form channelID
std::string CommonModeSubtractor::getASICName( const Detector::UT::ChannelID channelID ) const {

  auto tuple =
      UTMap.getTuple( channelID.module(), channelID.face(), channelID.stave(), channelID.side(), channelID.sector() );

  std::string module_name = std::get<4>( tuple );

  return UTEmu::UT_layers[channelID.layer()] + "_" + module_name + "_" +
         std::to_string( int( channelID.strip() / UTEmu::UTNumbers::nStripsPerASIC ) );
}

// Fill 1D histograms
void CommonModeSubtractor::fillHistograms( const LHCb::UTDigit* aDigit ) const {

  auto tuple = UTMap.getTuple( aDigit->module(), aDigit->face(), aDigit->stave(), aDigit->side(), aDigit->sector() );

  auto        x           = std::get<0>( tuple );
  auto        y           = std::get<1>( tuple );
  std::string module_name = std::get<4>( tuple );
  std::string type        = std::get<5>( tuple );

  auto title_charge = "CMS_" + UTEmu::UT_layers[aDigit->layer()] + "_" + module_name;

  ++m_2d_CMSADCCounter.at( title_charge )[{aDigit->strip(), aDigit->depositedCharge()}];
}

// Calculate and subtract common mode
LHCb::UTDigits CommonModeSubtractor::operator()( const LHCb::UTDigits& digits ) const {

  std::map<std::string, Gaudi::Accumulators::SigmaCounter<>> CommonMode;

  for ( const auto& digit : digits ) {
    if ( fabs( digit->depositedCharge() ) < m_commonModeCut ) {
      CommonMode[getASICName( digit->channelID() )] += digit->depositedCharge();
      CommonModeAv[getASICName( digit->channelID() )] += digit->depositedCharge();
    }
  }

  // Subtract common mode
  LHCb::UTDigits CMSubtractedDigits;
  for ( const auto& digit : digits ) {

    auto channelID     = digit->channelID();
    auto correction    = CommonMode[getASICName( digit->channelID() )].mean();
    auto corrected_ADC = digit->depositedCharge() - correction;
    CMSNoise[channelID.channelID()] += corrected_ADC;
    CMSNoiseAv[getASICName( digit->channelID() )] += corrected_ADC;

    auto CMSDigit = new UTDigit( channelID, corrected_ADC, digit->mcmVal(), digit->mcmStrip() );

    CMSubtractedDigits.insert( CMSDigit, channelID );

    fillHistograms( CMSDigit );
  }

  for ( auto& asic : CommonMode ) {
    plot1D( asic.second.standard_deviation(), "Projection_CommonModeSigma", "Projection_CommonModeSigma", 0, 2, 50 );
    plot1D( asic.second.mean(), "Projection_CommonModeMean", "Projection_CommonModeMean", -5, 5, 100 );
  }

  ++m_eventNumber;

  // If the last event make average plots
  if ( m_eventNumber.sum() == m_eventMax ) {
    makeCommonModePlots();
    makeCMSNoisePlots();
  }

  return CMSubtractedDigits;
}

// save CMS noise to CSV file
void CommonModeSubtractor::saveCMSNoiseToCSV() {

  // file pointer
  std::fstream fout;
  std::fstream fout_zs;

  // opens an existing CSV file or creates a new file.
  std::string input_path = std::string( UTEMU_PATH ) + m_runNumber + "/CMS_noise_" + m_runNumber + ".csv";
  fout.open( input_path.c_str(), std::ios::out | std::ios::trunc );

  for ( const auto& CMS : CMSNoise ) {
    fout << CMS.first << ", " << CMS.second.mean() << ", " << CMS.second.standard_deviation() << ", "
         << CMS.second.nEntries() << "\n";
  }

  info() << "Saving CMS noise file" << endmsg;
};

// save Common Mode to CSV file
void CommonModeSubtractor::saveCommonMoseToCSV() {

  // file pointer
  std::fstream fout;

  std::string input_path = std::string( UTEMU_PATH ) + m_runNumber + "/commonMode_" + m_runNumber + ".csv";
  fout.open( input_path.c_str(), std::ios::out | std::ios::trunc );

  for ( const auto& CM : CommonModeAv ) {
    fout << CM.first << ", " << CM.second.mean() << ", " << CM.second.standard_deviation() << ", "
         << CM.second.nEntries() << "\n";
  }

  info() << "Saving Common Mode file" << endmsg;
};

// Finalize
StatusCode CommonModeSubtractor::finalize() {

  saveCMSNoiseToCSV();
  saveCommonMoseToCSV();

  return StatusCode::SUCCESS;
}