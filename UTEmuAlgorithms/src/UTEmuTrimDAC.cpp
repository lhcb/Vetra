/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/*
 *  UTEmuTrimDAC.cpp
 *
 *  Created on: January, 2024
 *      Author: Mingjie Feng (mingjie.feng@cern.ch)
 *      Author: Wojciech Krupa (wokrupa@cern.ch)
 *
 */

#include "UTEmu/UTEmuTrimDAC.h"

using namespace LHCb;
using namespace UTEmu;

StatusCode TrimDACAlgorithm::initialize() {
  return Consumer::initialize().andThen( [&] {
    for ( const auto& tell40 : UTMap.getTELL40NamesASide() ) {
      Utility::map_emplace( m_1d_tell40, tell40 + "_calibrationStep", this, tell40 + "_calibrationStep",
                            {256, -0.5, 255.5} );
    }

    for ( const auto& tell40 : UTMap.getTELL40NamesCSide() ) {
      Utility::map_emplace( m_1d_tell40, tell40 + "_calibrationStep", this, tell40 + "_calibrationStep",
                            {256, -0.5, 255.5} );
    }
    return StatusCode::SUCCESS;
  } );
}

void TrimDACAlgorithm::operator()( const UTDigits& digitsCont, const UTDigits& errdigitsCont,
                                   const LHCb::ODIN& odin ) const {

  Tuple tuple = nTuple( ( m_layer.value() ).c_str(), "" );
  // store calibration step
  tuple->column( "calibrationStep", (short)odin.calibrationStep() ).ignore();
  for ( const auto& d : digitsCont ) fillTrimTuples( d, tuple );
  for ( const auto& d : errdigitsCont ) fillTrimTuples( d, tuple );

  tuple->write().ignore();
}

void TrimDACAlgorithm::fillTrimTuples( const LHCb::UTDigit* aDigit, Tuple& tuple ) const {
  std::string module_name = std::get<4>(
      UTMap.getTuple( aDigit->module(), aDigit->face(), aDigit->stave(), aDigit->side(), aDigit->sector() ) );
  auto name = UTEmu::UT_layers[aDigit->layer()] + "_" + module_name + "_" + std::to_string( aDigit->asic() % 4 ) + "_" +
              std::to_string( ( aDigit->strip() ) % 128 );
  if ( UTEmu::UT_layers[aDigit->layer()] == m_layer.value() )
    tuple->column( name, (short)aDigit->depositedCharge() ).ignore();
}
