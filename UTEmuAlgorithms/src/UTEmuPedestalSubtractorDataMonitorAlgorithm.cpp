
/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/*
 *  UTEmuPedestalSubtractorDataMonitorAlgorithm.cpp
 *
 *  Created on: August, 2023
 *      Author: Wojciech Krupa (wokrupa@cern.ch)
 */

#include "UTEmu/UTEmuPedestalSubtractorDataMonitorAlgorithm.h"

using namespace LHCb;
using namespace UTEmu;

StatusCode PedestalSubtractorDataMonitorAlgorithm::initialize() {

  return Consumer::initialize().andThen( [&] {
    for ( const auto& module : UTMap.getModulesNames() ) {
      auto title = "PedSubDigits_" + module;
      Utility::map_emplace( m_2d_PedSubADCCounter, title, this, title,
                            {UTEmu::UTNumbers::nStrips, 0, UTEmu::UTNumbers::nStrips}, {64, -32.5, 31.5} );
    }
    return StatusCode::SUCCESS;
  } );
}

void PedestalSubtractorDataMonitorAlgorithm::operator()( const UTDigits& digitsCont ) const {
  // histos per digit
  for ( const auto& d : digitsCont ) fillHistograms( d );
}

void PedestalSubtractorDataMonitorAlgorithm::fillHistograms( const LHCb::UTDigit* aDigit ) const {

  auto tuple = UTMap.getTuple( aDigit->module(), aDigit->face(), aDigit->stave(), aDigit->side(), aDigit->sector() );

  auto        x           = std::get<0>( tuple );
  auto        y           = std::get<1>( tuple );
  std::string module_name = std::get<4>( tuple );
  std::string type        = std::get<5>( tuple );
  if ( type == "D" ) type = "C";

  auto title_charge = "PedSubDigits_" + UTEmu::UT_layers[aDigit->layer()] + "_" + module_name;

  ++m_2d_PedSubADCCounter.at( title_charge )[{aDigit->strip(), aDigit->depositedCharge()}];
}

StatusCode PedestalSubtractorDataMonitorAlgorithm::finalize() { return GaudiHistoAlg::finalize(); }