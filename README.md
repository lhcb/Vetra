# VETRA

## Description

**VETRA project** - emulation, offline monitoring, and calibration of UT detectors at LHCb. 

The project consists of 3 parts:

1. **Emulation:** emulation of SALT DPC (Silicon ASIC for LHCb Tracking - UT detector readout ASIC) data processing chain for Non-Zero Suppress (NZS) data.
2. **Monitoring:** providing performance plots of all readout channels of UT silicon sensors
3. **Calibration:** providing all necessary information required by the recipe builder to prepare the SALT configuration file. 

## Getting Started

These instructions will get you a copy of the project up and running on lxplus or plus machine for development and testing purposes. 

### Installation of VETRA

1. Set up th LHCb stack using the following instruction: https://gitlab.cern.ch/rmatev/lb-stack-setup
2. In the LHCb stack:

``` bash
make Vetra
``` 

## Running the project

Run:
``` bash
Decoding + monitoring:
utils/run-env Vetra gaudirun.py ./Vetra/Ut/UTEmuOptions/options/UT_RawADC_Run.py

Pedestals calculation:
utils/run-env Vetra gaudirun.py ./Vetra/Ut/UTEmuOptions/options/UT_PedestalCalculation.py

Pedestal subtraction + pedestal subtracted data monitoring (run after pedestal calculation):
utils/run-env Vetra gaudirun.py ./Vetra/Ut/UTEmuOptions/options/UT_PedestalSubtraction.py

Pedestals monitoring (run after pedestal calculation) as well as MCMS data monitoring:
utils/run-env Vetra gaudirun.py ./Vetra/Ut/UTEmuOptions/options/UT_PedestalMonitoring.py

CMS noise calculation and monitoring (emulation, run after pedestal calculation):
utils/run-env Vetra gaudirun.py ./Vetra/Ut/UTEmuOptions/options/UT_Emulation_NoiseComponents.py
```
after modifying the above scripts with proper I/O paths. 

## Getting monitoring plots

Run: 
``` bash
Decoding:
utils/run-env Vetra root -l -b ./Vetra/Vetra_Plotter_Decoding.cpp

Pedestals monitoring as well as monitoring of MCMS data:
utils/run-env Vetra root -l -b ./Vetra/Vetra_Plotter_Pedestals.cpp

Pedestal subtracted data monitoring:
utils/run-env Vetra root -l -b ./Vetra/Vetra_Plotter_PedSub.cpp

Noise components monitoring:
utils/run-env Vetra root -l -b ./Vetra/Vetra_Plotter_EmuNoiseComponents.cpp
```
after modifying the above scripts with proper I/O paths. 

Create directory ```Plots_XXXXX``` (XXXXX is the run number) and put all output histograms there. 

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.


## Authors
* **Wojtek Krupa** - *Maintenance and development*
* **Mingjie Feng** - *Development*
* **Karol Sowa** - *Development*
 
## Contact
* **Wojtek Krupa** - *wokrupa@cern.ch*

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Adam Dendek (for initial work on TbUT)
* Tomasz Szumlak (decoder and Vetra Run I & II)
* Paweł Kopciewicz (Vetra II - Velo)

## VELO
Project for Vertex Locator was moved to https://gitlab.cern.ch/ivelo