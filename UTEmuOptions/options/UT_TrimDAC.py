###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organizatiodn #
# or submit itself to any jurisdiction.                                       #
###############################################################################

##Avoid modifying this script unless you know what are you doing
# Run it with UT_Run_TrimDAC.py

import sys, os, glob
from Gaudi.Configuration import MessageSvc, VERBOSE, DEBUG, INFO, ERROR, WARNING
from Gaudi.Configuration import ApplicationMgr
from Configurables import LHCbApp, GaudiSequencer
from Configurables import LoKiSvc
from Configurables import AlgResourcePool, CPUCrunchSvc, HiveSlimEventLoopMgr, HiveWhiteBoard, AvalancheSchedulerSvc
from Configurables import UTRawBankToUTNZSDigitsAlg, UTEmu__RawDataMonitorAlgorithm, UTEmu__TrimDACAlgorithm
from Configurables import Gaudi__Histograming__Sink__Root as RootHistoSink
from Configurables import Gaudi__Monitoring__MessageSvcSink as MessageSvcSink
from Configurables import LHCb__Det__LbDD4hep__IOVProducer as IOVProducer
from Configurables import createODIN, LHCb__UnpackRawEvent, EventSelector
from Configurables import TimingAuditor, AuditorSvc, SequencerTimerTool
from GaudiConf import IOHelper
from DDDB.CheckDD4Hep import UseDD4Hep

JOB_ID = 0
layer = "UTaX"
run_number = "0000284476"
evtMax = -1
output_path = ""
input_path = ""
num_parts = 5
half = 1

# -------------------------------------------------------------------------------
app = LHCbApp()
app.DataType = "Upgrade"
app.Simulation = False
app.EvtMax = evtMax
app.TupleFile = f"{output_path}/UT_{layer}_{JOB_ID}.root"

# -------------------------------------------------------------------------------
# DD4hep -------------------------------------------------------------

if UseDD4Hep:
    # Prepare detector description
    from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc
    dd4hepsvc = DD4hepSvc()
    dd4hepsvc.VerboseLevel = 1
    dd4hepsvc.GeometryLocation = "${DETECTOR_PROJECT_ROOT}/compact"
    dd4hepsvc.GeometryVersion = "run3/trunk"
    dd4hepsvc.GeometryMain = "LHCb.xml"
    dd4hepsvc.DetectorList = ["/world", "UT"]
    iovProd = IOVProducer("ReserveIOVDD4hep", ODIN='DAQ/ODIN')

else:
    # DetDesc case
    LHCbApp().DataType = "Upgrade"
    LHCbApp().DDDBtag = 'upgrade/UTv4r2-newUTID'
    LHCbApp().CondDBtag = "master"
    LHCbApp().Simulation = False
    iovProd = IOVProducer()

LoKiSvc().Welcome = False
MessageSvc().OutputLevel = INFO
EventSelector().PrintFreq = 100

# -------------------------------------------------------------------------------
# Setup input -------------------------------------------------------------

data = []
if os.path.exists(input_path):
    data = glob.glob('/' + input_path + '*.mdf')
    midpoint = len(data) // 2
    if (half == 1):
        data = data[:midpoint]
    elif (half == 2):
        part2 = data[midpoint:]
else:
    print("Input directory doesn't exist!")
    sys.exit()
if data == []:
    print("Input data doesn't exist!")
    sys.exit()

parts = [data[i::num_parts] for i in range(num_parts)]
selected_part = parts[JOB_ID]

IOHelper("MDF").inputFiles(selected_part)

# Multithreading -------------------------------------------------------------
# -------------------------------------------------------------------------------
evtslots = 1
threads = 1

# Event Loop Manager -----------------------------------------------------------
# It's called slim since it has less functionalities overall than the good-old
# event loop manager. Here we just set its outputlevel to DEBUG.

whiteboard = HiveWhiteBoard("EventDataSvc", EventSlots=evtslots)
slimeventloopmgr = HiveSlimEventLoopMgr(
    SchedulerName="AvalancheSchedulerSvc", OutputLevel=INFO)

# ForwardScheduler -------------------------------------------------------------
# We just decide how many algorithms in flight we want to have and how many
# threads in the pool. The default value is -1, which is for TBB equivalent
# to take over the whole machine.

scheduler = AvalancheSchedulerSvc(ThreadPoolSize=threads, OutputLevel=INFO)

# Algo Resource Pool -----------------------------------------------------------
# Nothing special here, we just set the debug level.
AlgResourcePool(OutputLevel=INFO)

CPUCrunchSvc(shortCalib=True)

# Algorithms ----------------------------------------------------------
# Declare the algorithms we want to run. We set the output level to INFO

monSeq = GaudiSequencer("UTSequence")
monSeq.IgnoreFilterPassed = True

unpacker = LHCb__UnpackRawEvent(
    'UnpackRawEvent',
    BankTypes=['ODIN'],
    RawBankLocations=['/Event/DAQ/RawBanks/ODIN'])

odin = createODIN(RawBanks="DAQ/RawBanks/ODIN")

decoder = UTRawBankToUTNZSDigitsAlg(
    "UTRawToDigits",
    OutputLevel=INFO,
    Type="",
    OutputDigitData='/Event/UT/Digits',
    OutputBankData='/Event/UT/Banks')

trim = UTEmu__TrimDACAlgorithm(
    "UTDigitTrimDAC",
    OutputLevel=INFO,
    DigitLocation='/Event/UT/Digits',
    Layer=layer)

monSeq.Members = [unpacker, odin, iovProd, decoder, trim]

# Application Manager ----------------------------------------------------------
# We put everything together and change the type of message service

appMgr = ApplicationMgr(
    EvtMax=evtMax,
    TopAlg=[monSeq],
    HistogramPersistency="ROOT",
    EventLoop=slimeventloopmgr,
    ExtSvc=[
        MessageSvcSink(),
        whiteboard,
        RootHistoSink(FileName=f"{output_path}/calibrationStep.root"),
    ],
)

# Some extra stuff for timing table
ApplicationMgr().ExtSvc += ['ToolSvc', 'AuditorSvc']
ApplicationMgr().AuditAlgorithms = True
AuditorSvc().Auditors += ['TimingAuditor']
SequencerTimerTool().OutputLevel = 4

# No error messages when reading MDF
#IODataManager().DisablePFNWarning = True
