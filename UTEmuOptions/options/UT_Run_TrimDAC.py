import multiprocessing
import subprocess
import sys, os

##I/O

#run parameters
run_number = '0000284469'
side = "UTA"
half = 1  #1 or 2 or 0 (all)
#choose layer
layer = 'UTbV'

#other I/O
num_jobs = 5  # replace with the number of jobs you want to run
options_path = "./Vetra/UTEmuOptions/options"

output_path = f"./Vetra/{run_number}"
if not os.path.exists(output_path):
    os.makedirs(output_path)

if (half > 0):
    output_path += f"/{half}/"

input_data = f"/hlt2/objects/{side}/{run_number}"


def modify_script(original_script_path, modified_script_path, output_path,
                  input_data, job_id, run_number, layer, num_jobs, half):

    variable_mapping = {
        'JOB_ID': job_id,
        'run_number': f'"{run_number}"',
        'layer': f'"{layer}"',
        'output_path': f'"{output_path}"',
        'input_path': f'"{input_data}"',
        'num_parts': num_jobs,
        'half': half
    }

    with open(original_script_path, 'r') as file:
        lines = file.readlines()

    for i, line in enumerate(lines):
        for var_name, var_value in variable_mapping.items():
            if line.startswith(var_name):
                lines[i] = f'{var_name} = {var_value}\n'

    with open(modified_script_path, 'w') as file:
        file.writelines(lines)


def worker(job_id, run_number, layer, output_path, input_data, num_jobs, half):
    original_script_path = f"{options_path}/UT_trimDAC.py"
    modified_script_path = f"{options_path}/UT_trimDAC_{job_id}.py"
    output_file_path = f"{output_path}/output_{layer}_{job_id}.txt"
    modify_script(original_script_path, modified_script_path, output_path,
                  input_data, job_id, run_number, layer, num_jobs, half)
    with open(output_file_path, 'w') as output_file:
        subprocess.call(
            [
                "../../../utils/run-env", "Vetra", "gaudirun.py",
                modified_script_path
            ],
            stdout=output_file)  ##ensure path to utils/run-nv is valid


def remove_all_modified_scripts(num_jobs):
    for job_id in range(num_jobs):
        modified_script_path = f"{options_path}/UT_trimDAC_{job_id}.py"
        if os.path.exists(modified_script_path):
            os.remove(modified_script_path)
            print(f"Removed modified script for job {job_id}")


if __name__ == '__main__':
    processes = []
    for job_id in range(num_jobs):
        p = multiprocessing.Process(
            target=worker,
            args=(job_id, run_number, layer, output_path, input_data, num_jobs,
                  half))
        p.start()
        processes.append(p)

    # Wait for all processes to finish
    for p in processes:
        p.join()

    # Call this function to remove all modified scripts at the end
    remove_all_modified_scripts(num_jobs)
