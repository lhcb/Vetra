###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organizatiodn #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import sys, os, glob
from Gaudi.Configuration import MessageSvc, VERBOSE, DEBUG, INFO, ERROR, WARNING
from Gaudi.Configuration import ApplicationMgr
from Configurables import LHCbApp, GaudiSequencer
from Configurables import LoKiSvc
from Configurables import AlgResourcePool, CPUCrunchSvc, HiveSlimEventLoopMgr, HiveWhiteBoard, AvalancheSchedulerSvc
from Configurables import UTRawBankToUTNZSDigitsAlg, UTEmu__PedestalCalculatorStep, UTEmu__PedestalCalculatorDataMonitorAlgorithm
from Configurables import Gaudi__Histograming__Sink__Root as RootHistoSink
from Configurables import Gaudi__Monitoring__MessageSvcSink as MessageSvcSink
from Configurables import LHCb__Det__LbDD4hep__IOVProducer as IOVProducer
from Configurables import createODIN, LHCb__UnpackRawEvent, EventSelector
from Configurables import TimingAuditor, AuditorSvc, SequencerTimerTool
from GaudiConf import IOHelper
from DDDB.CheckDD4Hep import UseDD4Hep

# -------------------------------------------------------------------------------
app = LHCbApp()
app.DataType = "Upgrade"
app.Simulation = False
app.EvtMax = 1

# -------------------------------------------------------------------------------
# DD4hep -------------------------------------------------------------

if UseDD4Hep:
    # Prepare detector description
    from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc
    dd4hepsvc = DD4hepSvc()
    dd4hepsvc.VerboseLevel = 1
    dd4hepsvc.GeometryLocation = "${DETECTOR_PROJECT_ROOT}/compact"
    dd4hepsvc.GeometryVersion = "run3/trunk"
    dd4hepsvc.GeometryMain = "LHCb.xml"
    dd4hepsvc.DetectorList = ["/world", "UT"]
    iovProd = IOVProducer("ReserveIOVDD4hep", ODIN='DAQ/ODIN')

else:
    # DetDesc case
    LHCbApp().DataType = "Upgrade"
    LHCbApp().DDDBtag = 'upgrade/UTv4r2-newUTID'
    LHCbApp().CondDBtag = "master"
    LHCbApp().Simulation = False
    iovProd = IOVProducer()

LoKiSvc().Welcome = False
MessageSvc().OutputLevel = INFO
EventSelector().PrintFreq = 1

# -------------------------------------------------------------------------------
# Setup input -------------------------------------------------------------

run_number = "0000313122"
input_path = '/hlt2/objects/UT/' + run_number + '/'
#input_path = '/eos/lhcb/hlt2/UT/' + run_number + '/'

output_path = f"./Vetra/{run_number}"
if not os.path.exists(output_path):
    os.makedirs(output_path)

# Algorithms ----------------------------------------------------------
# Declare the algorithms we want to run. We set the output level to INFO

monSeq = GaudiSequencer("UTSequence")
monSeq.IgnoreFilterPassed = True

unpacker = LHCb__UnpackRawEvent(
    'UnpackRawEvent',
    BankTypes=['ODIN'],
    RawBankLocations=['/Event/DAQ/RawBanks/ODIN'])

odin = createODIN(RawBanks="DAQ/RawBanks/ODIN")

pedestals = UTEmu__PedestalCalculatorStep(
    'UTDigitsToPedestals', OutputLevel=INFO, RunNumber=run_number)

monSeq.Members = [unpacker, odin, iovProd, pedestals]

# Application Manager ----------------------------------------------------------
# We put everything together and change the type of message service

appMgr = ApplicationMgr(
    EvtMax=-1,
    TopAlg=[monSeq],
    ExtSvc=[MessageSvcSink()],
)

# Some extra stuff for timing table
ApplicationMgr().ExtSvc += ['ToolSvc', 'AuditorSvc']
ApplicationMgr().AuditAlgorithms = True
AuditorSvc().Auditors += ['TimingAuditor']
SequencerTimerTool().OutputLevel = 4

# No error messages when reading MDF
#IODataManager().DisablePFNWarning = True
