import os
import sys
import importlib
import ROOT  # Assuming you have ROOT installed


class UTSEUCheck:
    def __init__(self, hist_path):
        self.hist_name = [
            "UTOnlineBSMonitor/hitRateChannels_UA",
            "UTOnlineBSMonitor/hitRateChannels_UC"
        ]
        self.hist_path = hist_path
        self.root_file = ROOT.TFile.Open(hist_path)
        self.hists = {}
        noisy = {}

        if os.path.exists('/hist/Reference/UTZSMon'):
            sys.path.append('/hist/Reference/UTZSMon')
            try:
                importlib.import_module('ut_dict')
                print(f"UT Module 'ut_dict' imported successfully!")
                from ut_dict import UT_dict
            except ImportError:
                print(f"UT Error: Module 'ut_dict' not found.")
        else:
            print(f"UT Error: Directory '/hist/Reference/UTZSMon' not found.")

        for hist_name in self.hist_name:
            self.hists[hist_name] = self.root_file.Get(hist_name)

        for hist_name, hist in self.hists.items():
            print(f"UT - found {hist_name}")
            try:
                bins = hist.GetXaxis().GetNbins()
            except AttributeError:
                continue

            for checkedBin in range(1, bins + 1):
                binContent = hist.GetBinContent(checkedBin)
                if binContent > 0.05:
                    if hist_name == "UTOnlineBSMonitor/hitRateChannels_UC":
                        ch = checkedBin + 268288 - 1
                        print(UT_dict[int(ch / 128)] + ".Ch" + str(ch % 128),
                              "Hitrate: ", binContent)
                    else:
                        ch = checkedBin - 1
                        print(UT_dict[int(ch / 128)] + ".Ch" + str(ch % 128),
                              "Hitrate: ", binContent)


if __name__ == "__main__":
    hist_path = "/hist/Savesets/2024/UT/UTBSMon/11/07/UTBSMon-310205-20241107T173911-EOR.root"
    UTSEUCheck(hist_path)
