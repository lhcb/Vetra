import os
import sys
import importlib
import ROOT  # Assuming you have ROOT installed


class UTSEUCheck:
    def __init__(self, hist_path):
        self.hist_name = [
            "UTOnlineBSMonitor/Average_hitRate_UCaU",
            "UTOnlineBSMonitor/Average_hitRate_UCaX",
            "UTOnlineBSMonitor/Average_hitRate_UCbX",
            "UTOnlineBSMonitor/Average_hitRate_UCbV",
            "UTOnlineBSMonitor/Average_hitRate_UAaU",
            "UTOnlineBSMonitor/Average_hitRate_UAaX",
            "UTOnlineBSMonitor/Average_hitRate_UAbX",
            "UTOnlineBSMonitor/Average_hitRate_UAbV"
        ]
        self.hist_path = hist_path
        self.root_file = ROOT.TFile.Open(hist_path)
        self.hists = {}
        noisy = {}

        if os.path.exists('/hist/Reference/UTZSMon'):
            sys.path.append('/hist/Reference/UTZSMon')
            try:
                importlib.import_module('ut_dict')
                print(f"UT Module 'ut_dict' imported successfully!")
                from ut_dict import UT_dict
            except ImportError:
                print(f"UT Error: Module 'ut_dict' not found.")
        else:
            print(f"UT Error: Directory '/hist/Reference/UTZSMon' not found.")

        # Open the output text file
        with open("noisy_chips.txt", "w") as output_file:
            for hist_name in self.hist_name:
                self.hists[hist_name] = self.root_file.Get(hist_name)

            for hist_name, hist in self.hists.items():
                print(f"UT - found {hist_name}")
                try:
                    bins = hist.GetXaxis().GetNbins()
                except AttributeError:
                    continue

                for checkedBin in range(1, bins + 1):
                    binContent = hist.GetBinContent(checkedBin)
                    if binContent > 0.1:
                        ch = checkedBin - 1 + hist.GetBinLowEdge(1) + 0.5
                        output_line = f"{UT_dict[ch]} Hitrate: {binContent}\n"

                        # Save to the text file
                        output_file.write(f"{UT_dict[ch]}\n")

                        # Also print to console
                        print(output_line.strip())


if __name__ == "__main__":
    hist_path = "/hist/Savesets/2024/UT/UTBSMon/11/07/UTBSMon-310209-20241107T174311.root"
    UTSEUCheck(hist_path)
