import ROOT
import pandas as pd

# Open the ROOT file
filename = 'ut_data_nzs_0000295472.root'
dirname = '/swdev/wokrupa/stack2/Vetra/ut_data_nzs_0000295472.root'
myfile = ROOT.TFile.Open(filename)
dir_obj = myfile.Get(dirname)


def analyze_histogram_ranges(hist):
    """
    Function to analyze a 2D histogram by dividing it into four different ranges along the X-axis,
    calculating the RMS and mean for each range, and identifying bad channels based on noise thresholds.
    """

    def analyze_range(hist, x_start, x_end, bad_channels):
        projY = hist.ProjectionY("", x_start, x_end)
        bin_f = projY.FindFirstBinAbove(0)
        bin_l = projY.FindLastBinAbove(0)
        max_adc = projY.GetBinLowEdge(bin_l)
        min_adc = projY.GetBinLowEdge(bin_f)
        ADC_avg_RMS = projY.GetRMS()
        ADC_avg_MEAN = projY.GetMean()
        High_noise = ADC_avg_MEAN + 5 * ADC_avg_RMS
        Low_noise = ADC_avg_MEAN - 5 * ADC_avg_RMS

        n_bins_y = projY.GetNbinsX()
        for bin_y in range(1, n_bins_y + 1):
            #y_center = projY.GetXaxis().GetBinCenter(bin_y)
            y_center = projY.GetXaxis().GetBinLowEdge(bin_y)
            if y_center > High_noise or y_center < Low_noise:
                for bin_x in range(x_start, x_end + 1):
                    bin_content = hist.GetBinContent(bin_x, bin_y)
                    if bin_content > 0:
                        x_center = hist.GetXaxis().GetBinCenter(bin_x)
                        bad_channels.append({
                            "ASIC Name":
                            hist.GetName().replace('ADC_', ''),
                            "Channel":
                            bin_x,
                            "ADC Value":
                            y_center,
                            "X Center":
                            x_center,  # X center is the bin number
                            "Y Center":
                            y_center,  # Y center is the ADC value
                            "High Noise":
                            High_noise,
                            "Low Noise":
                            Low_noise,
                            "X Start":
                            x_start,
                            "X End":
                            x_end
                        })

    bad_channels = []
    # Analyze each of the four ranges
    ranges = [(1, 128), (129, 256), (257, 384), (385, 513)]
    for x_start, x_end in ranges:
        analyze_range(hist, x_start, x_end, bad_channels)

    df = pd.DataFrame(bad_channels)
    return df


# List all keys in the directory
keys = dir_obj.GetListOfKeys()
adc_keys = [key for key in keys if key.GetName().startswith("ADC_")]

# Collect all results
list_of_bad_channels_dfs = []
for key in adc_keys:
    histogram = key.ReadObj()  # Get the histogram object directly from the key
    list_of_bad_channels_dfs.append(analyze_histogram_ranges(histogram))

result = pd.concat(list_of_bad_channels_dfs, ignore_index=True)

# Remove duplicate entries for the same channel in the same ASIC
result.drop_duplicates(subset=["ASIC Name", "Channel"], inplace=True)

#print(result.to_string())
print(result[["ASIC Name", "Channel", "ADC Value", "High Noise",
              "Low Noise"]].to_string())

print('total number of channels = ', len(adc_keys) * 512)
print('Number of bad channels = ', len(result.index))
print('Fraction of bad channels = ', len(result.index) / (len(adc_keys) * 512))

# Save results to files
result.to_csv('bad_channels.txt', sep='\t', index=False)
result.to_csv('bad_channels.csv', index=False)

# Plotting the histograms with circles and noise lines using ROOT
canvas = ROOT.TCanvas("canvas", "Histogram Plots", 1600,
                      1200)  # Make the canvas larger
pdf_output = ROOT.TPDF("all_histograms.pdf", 111)

#canvas.Divide(2, 2)  # Divide canvas into 2x2 grid for 4 plots per page

pad_number = 1

for key in adc_keys:
    hist = key.ReadObj()
    asic_name = hist.GetName().replace('ADC_', '')
    df_asic = result[result['ASIC Name'] == asic_name]
    #print(df_asic)
    if df_asic.empty:
        continue

    canvas.cd(pad_number)
    #ROOT.gPad.SetLogz()  # Set logarithmic scale for better visualization
    hist.Draw("COLZ")
    ROOT.gStyle.SetOptStat(0)  # Remove the statistics box

    ellipses = []
    texts = []
    lines_high = []
    lines_low = []

    for _, row in df_asic.iterrows():
        bin_x = row['Channel']
        bin_y = hist.GetYaxis().FindBin(row['ADC Value'] + 0.5)
        x_start = row['X Start']
        x_end = row['X End']
        high_noise = row['High Noise']
        low_noise = row['Low Noise']

        # Create a larger ellipse around the bin
        ellipse = ROOT.TEllipse(hist.GetXaxis().GetBinCenter(bin_x),
                                row['ADC Value'], 5.0, 5.0)
        ellipse.SetLineColor(ROOT.kRed)
        ellipse.SetLineWidth(2)
        ellipse.SetFillStyle(0)
        ellipses.append(ellipse)

        # Add text for the channel name
        text = ROOT.TText(hist.GetXaxis().GetBinCenter(bin_x),
                          row['ADC Value'] + 5, str(row['Channel']))
        text.SetTextColor(ROOT.kBlack)
        text.SetTextSize(0.05)
        texts.append(text)

        # Create high noise line
        line_high = ROOT.TLine(hist.GetXaxis().GetBinCenter(x_start),
                               high_noise,
                               hist.GetXaxis().GetBinCenter(x_end), high_noise)
        line_high.SetLineColor(ROOT.kRed)
        line_high.SetLineStyle(ROOT.kDashed)
        lines_high.append(line_high)

        # Create low noise line
        line_low = ROOT.TLine(hist.GetXaxis().GetBinCenter(x_start), low_noise,
                              hist.GetXaxis().GetBinCenter(x_end), low_noise)
        line_low.SetLineColor(ROOT.kRed)
        line_low.SetLineStyle(ROOT.kDashed)
        lines_low.append(line_low)

    # Draw all shapes
    for ellipse in ellipses:
        ellipse.Draw("same")
    for text in texts:
        text.Draw("same")
    for line_high in lines_high:
        line_high.Draw("same")
    for line_low in lines_low:
        line_low.Draw("same")

    canvas.Update()
    '''
    pad_number += 1
    if pad_number > 4:
        pdf_output.NewPage()
        #canvas.Clear()
        canvas.Divide(2, 2)
        pad_number = 1
    '''
pdf_output.Close()

# Close the ROOT file
myfile.Close()
