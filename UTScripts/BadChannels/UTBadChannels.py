import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import re
import yaml
from collections import OrderedDict

run = "0000307299"

# Code to convert channelID to online name + channel
strip_mask = 0x1ff
sector_mask = 0x200
module_mask = 0x1c00
face_mask = 0x2000
stave_mask = 0x3c000
layer_mask = 0xc0000
side_mask = 0x100000
det_type_mask = 0x600000
det_type_ut = 2
max_strip_number = 511
max_sector_number = 1
max_module_number = 7
max_face_number = 1
max_stave_number = 8
max_layer_number = 3
max_side_number = 1
side_names = ("C", "A")
layer_names = ("aX", "aU", "bV", "bX")


def remove_duplicates_from_list(lst):
    return list(set(lst))


def get_online_name(aChan):
    det_type, side, layer, stave, face, module, sector, _ = extract_bits(aChan)
    assert det_type == det_type_ut
    side_str = side_names[side]
    layer_str = layer_names[layer]
    stave_str = str(stave + 1)  # Convert stave to string

    # Determine module_str
    if stave == 0:
        if face == 0:
            module_str = f"B_S{4 - module}" if module < 4 else f"T_M{module - 3}"
        elif face == 1:
            module_str = f"B_M{4 - module}" if module < 4 else f"T_S{module - 3}"
    else:
        if face == 0:
            module_str = f"B_S{3 - module}" if module < 4 else f"T_M{module - 3}"
        elif face == 1:
            module_str = f"B_M{4 - module}" if module < 4 else f"T_S{module - 4}"

    # Determine sector_str
    if stave < 2 and face == 0 and 2 <= module < 5:
        sector_str = "W" if sector == 0 else "E"
    elif stave < 2 and face == 1 and 2 < module <= 5:
        sector_str = "W" if sector == 0 else "E"
    else:
        assert sector == 0
        sector_str = ""

    name = f"UT{layer_str}_{stave_str}{side_str}{module_str}{sector_str}"
    return name


def extract_bits(aChan):
    binary_aChan = bin(aChan)[2:]  # Convert to binary string
    strip = int(binary_aChan, 2) & strip_mask
    sector = (int(binary_aChan, 2) & sector_mask) >> 9
    module = (int(binary_aChan, 2) & module_mask) >> 10
    face = (int(binary_aChan, 2) & face_mask) >> 13
    stave = (int(binary_aChan, 2) & stave_mask) >> 14
    layer = (int(binary_aChan, 2) & layer_mask) >> 18
    side = (int(binary_aChan, 2) & side_mask) >> 20
    det_type = (int(binary_aChan, 2) & det_type_mask) >> 21
    return det_type, side, layer, stave, face, module, sector, strip


def compute_ci(x, confidence=0.68):
    intervallo = np.percentile(
        x, [100 * (1 - confidence) / 2, 100 * (1 - (1 - confidence) / 2)])
    return (intervallo[1] - intervallo[0]) / 2


def compute_min(x, confidence):
    intervallo = np.percentile(
        x, [100 * (1 - confidence) / 2, 100 * (1 - (1 - confidence) / 2)])
    return intervallo[0]


def compute_max(x, confidence):
    intervallo = np.percentile(
        x, [100 * (1 - confidence) / 2, 100 * (1 - (1 - confidence) / 2)])
    return intervallo[1]


def mean_wo_outliers(x):
    minimo = compute_min(x, 0.98)
    maximo = compute_max(x, 0.98)
    a_sel = np.array(x)[(x > minimo) & (x < maximo)]
    return np.mean(a_sel)


def get_last(t):
    return t[-1]


# import data
data = pd.read_csv("/swdev/wokrupa/stack3/Vetra/CMS_noise_" + run + ".csv")
columns_name = ['channel_ID', 'mean', 'sigma', 'N']
data.columns = columns_name

# Extract online name and channel
data['name'] = data['channel_ID'].apply(get_online_name)
data['channel'] = data['channel_ID'].apply(extract_bits).apply(get_last)
data['chip'] = (data['channel'] // 128).astype(int)

# Compute the mean and std of noise (sigma) for each chip without outliers
mean_noise = data.groupby(
    ['name', 'chip'])['sigma'].apply(mean_wo_outliers).reset_index()
std_noise = data.groupby(['name',
                          'chip'])['sigma'].apply(compute_ci).reset_index()

# Merge dataframes - calculate mean and std of sigma per chip
merged_df = pd.merge(
    data, mean_noise, on=['name', 'chip'], suffixes=('', '_mean'))
merged_df = pd.merge(
    merged_df, std_noise, on=['name', 'chip'], suffixes=('', '_std'))

#Save all
merged_df.to_csv('all_' + run + '.csv', index=False)

# Select noisy channels
condition = (merged_df['sigma'] > merged_df['sigma_mean'] + 10 * merged_df['sigma_std']) | \
            (merged_df['sigma'] < merged_df['sigma_mean'] - 10 * merged_df['sigma_std']) | \
            (merged_df['mean'] > 2.5 * merged_df['sigma']) | \
            (merged_df['mean'] < -2.5 * merged_df['sigma'])

result_df = merged_df.loc[condition, ['channel', 'name']]

# Select bad channels based on sigma thresholds
condition_high = merged_df['sigma'] > 2 * merged_df['sigma_mean']
condition_low = merged_df['sigma'] < 0.5 * merged_df['sigma_mean']

result_df2 = merged_df.loc[condition_high | condition_low, ['channel', 'name']]

# Convert the list in the correct format
result_df['chip'] = result_df['channel'] // 128
result_df['chip'] = result_df['chip'].astype(int)

# Convert the list in the correct format
result_df2['chip'] = result_df2['channel'] // 128
result_df2['chip'] = result_df2['chip'].astype(int)

# Group by name and chip, aggregate channels as lists
combined_df = pd.concat([result_df, result_df2], ignore_index=True)

tot_n = combined_df.groupby(['name',
                             'chip'])['channel'].agg(list).reset_index()

tot_n['nBadC'] = tot_n['channel'].apply(lambda x: [(i % 128) for i in x])

# Calculate positions
tot_n['pos'] = tot_n['nBadC'].apply(lambda x: [num // 8 for num in x])
# Verify the lengths are the same
assert all(tot_n['nBadC'].apply(len) == tot_n['pos'].apply(
    len)), "Mismatch in lengths of 'nBadC' and 'pos'"
# Print noisy channels

# Apply the function to the relevant columns
tot_n['nBadC'] = tot_n['nBadC'].apply(remove_duplicates_from_list)
tot_n['pos'] = tot_n['pos'].apply(remove_duplicates_from_list)
tot_n['channel'] = tot_n['channel'].apply(remove_duplicates_from_list)

print("CMS_based")
print(tot_n)

# Tag saturation with pedestal value
# Import pedestals
ped = pd.read_csv("/swdev/wokrupa/stack3/Vetra/pedestals_" + run + ".csv")
ped.columns = ['channel_ID', 'pedestal']
ped['name'] = ped['channel_ID'].apply(get_online_name)
m_ped = ped['channel_ID'].apply(extract_bits)

df_ped = pd.DataFrame(m_ped)
df_ped['last_value'] = df_ped['channel_ID'].apply(lambda x: get_last(x))
ped['channel'] = df_ped['last_value']
ped['chip'] = (ped['channel'] // 128).astype(int)

# Select bad channels based on saturation condition
condition_p = np.abs(ped['pedestal']) > 25
result_df_p = ped.loc[condition_p, ['channel', 'name']]

# Calculate chip numbers
result_df_p['chip'] = (result_df_p['channel'] // 128).astype(int)

# Group channels by name and chip
tot_p = result_df_p.groupby(['name',
                             'chip'])['channel'].agg(list).reset_index()

# Calculate positions
tot_p['nBadC'] = tot_p['channel'].apply(lambda x: [(i % 128) for i in x])
tot_p['pos'] = tot_p['nBadC'].apply(lambda x: [num // 8 for num in x])

# Verify equal lengths
assert all(tot_p['nBadC'].apply(len) == tot_p['pos'].apply(
    len)), "Mismatch in lengths of 'nBadC' and 'pos'"

# Apply the function to the relevant columns
tot_p['nBadC'] = tot_p['nBadC'].apply(remove_duplicates_from_list)
tot_p['pos'] = tot_p['pos'].apply(remove_duplicates_from_list)
tot_p['channel'] = tot_p['channel'].apply(remove_duplicates_from_list)

print("Pedestal_based")
print(tot_p)

# Tag disabled channels
# Import total noise not biased by CMS

# import data
data = pd.read_csv("/swdev/wokrupa/stack3/Vetra/sigmaNoise_" + run + ".csv")
columns_name = ['channel_ID', 'mean', 'sigma']
data.columns = columns_name

# Extract online name and channel
data['name'] = data['channel_ID'].apply(get_online_name)
data['channel'] = data['channel_ID'].apply(extract_bits).apply(get_last)
data['chip'] = (data['channel'] // 128).astype(int)

# Compute the mean and std of noise (sigma) for each chip without outliers
mean_noise = data.groupby(
    ['name', 'chip'])['sigma'].apply(mean_wo_outliers).reset_index()
std_noise = data.groupby(['name',
                          'chip'])['sigma'].apply(compute_ci).reset_index()

# Merge dataframes - calculate mean and std of sigma per chip
merged_df = pd.merge(
    data, mean_noise, on=['name', 'chip'], suffixes=('', '_mean'))
merged_df = pd.merge(
    merged_df, std_noise, on=['name', 'chip'], suffixes=('', '_std'))

#Save all
merged_df.to_csv('all2_' + run + '.csv', index=False)

# Select noisy channels
condition = (merged_df['sigma_mean'] > 0)
condition2 = (merged_df['sigma'] == 0)

result_df = merged_df.loc[condition & condition2, ['channel', 'name']]

# Convert the list in the correct format
result_df['chip'] = result_df['channel'] // 128
result_df['chip'] = result_df['chip'].astype(int)

tot_s = result_df.groupby(['name', 'chip'])['channel'].agg(list).reset_index()
tot_s['nBadC'] = tot_s['channel'].apply(lambda x: [(i % 128) for i in x])

# Calculate positions
tot_s['pos'] = tot_s['nBadC'].apply(lambda x: [num // 8 for num in x])

# Verify the lengths are the same
assert all(tot_s['nBadC'].apply(len) == tot_s['pos'].apply(
    len)), "Mismatch in lengths of 'nBadC' and 'pos'"

# Apply the function to the relevant columns
tot_s['nBadC'] = tot_s['nBadC'].apply(remove_duplicates_from_list)
tot_s['pos'] = tot_s['pos'].apply(remove_duplicates_from_list)
tot_s['channel'] = tot_s['channel'].apply(remove_duplicates_from_list)

print("TotSig_based")
print(tot_s)

# merging bad channels from noise and from pedestal distributions
all_bad_ch = pd.concat([tot_n, tot_p, tot_s])


# Define a function to merge lists
def merge_lists(series):
    return [x for sublist in series for x in sublist]


# Group by 'chip' and 'name' and apply the merge_lists function
result1 = all_bad_ch.groupby(['chip',
                              'name'])['nBadC'].agg(merge_lists).reset_index()
result2 = all_bad_ch.groupby(['chip',
                              'name'])['pos'].agg(merge_lists).reset_index()
result3 = all_bad_ch.groupby(
    ['chip', 'name'])['channel'].agg(merge_lists).reset_index()

# Merge the results
final_list = pd.merge(result1, result2, on=['chip', 'name'], how='inner')
final_list = pd.merge(final_list, result3, on=['chip', 'name'], how='inner')

# Apply the function to the relevant columns
final_list['nBadC'] = final_list['nBadC'].apply(remove_duplicates_from_list)
final_list['channel'] = final_list['channel'].apply(
    remove_duplicates_from_list)

#trick to have repetition but only when needed!
final_list['pos'] = final_list['nBadC'].apply(
    lambda x: [num // 8 for num in x])

tot = final_list
print("Total")
print(tot)
tot.to_csv('total_list_1_' + run + '.txt', index=False)

# Open a file for writing
file = open('bad_ch_list_' + run + '.txt', 'w')

exclusions = [
    "UTbV_1AT_S1E.Chip3.Ch104",
    "UTaU_2AT_M2.Chip3.Ch18",
    "UTbV_9CT_S3.Chip0.Ch90",
]

# write the bad channels in the correct format
for i in range(len(tot)):
    exc = 1
    mask_list = [
        '0x00', '0x00', '0x00', '0x00', '0x00', '0x00', '0x00', '0x00', '0x00',
        '0x00', '0x00', '0x00', '0x00', '0x00', '0x00', '0x00'
    ]
    sum_dict = {}
    prova = []
    mask_dict = {}
    for val1, val2 in zip(tot.iloc[i]['nBadC'], tot.iloc[i]['pos']):
        name = tot.iloc[i]['name'] + ".Chip" + str(
            tot.iloc[i]['chip']) + ".Ch" + str(val1)
        print(name)
        if name in exclusions:
            exc = 0
            print("Exclusion! ", name)
            continue
        if val2 not in mask_dict:
            mask_dict[val2] = 0
        mask_dict[val2] |= (1 << val1 % 8)
    mask_list2 = list(mask_dict.values())
    mask = [hex(0x00 | element) for element in mask_list2]
    no_rep_pos = list(set(tot.iloc[i]['pos']))
    no_rep_pos.sort()
    if (exc):
        if tot.iloc[i]['nBadC']:
            for jj in range(len(no_rep_pos)):
                mask_list[no_rep_pos[jj]] = mask[jj]
        line = '--- {}.{} {}\n'.format(tot.iloc[i]['name'],
                                       tot.iloc[i]['chip'], mask_list)
        file.write(line)

# extra stuff for extra studies

tot_p.to_csv('bad_ch_list_1_' + run + '.txt', index=False)
tot_n.to_csv('bad_ch_list_2_' + run + '.txt', index=False)
tot_s.to_csv('bad_ch_list_3_' + run + '.txt', index=False)
