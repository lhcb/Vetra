/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/*
 *  Vetra_Plotter_MeanADCs.cpp
 *
 *  Created on: September, 2023
 *      Author: Wojciech Krupa (wokrupa@cern.ch)
 */

#include "TCanvas.h"
#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TKey.h"
#include <filesystem>
#include <iostream>
#include <stdio.h>
#include <sys/stat.h>

using namespace std;

// Constants
const unsigned int NUM_LAYERS = 4;
const unsigned int NUM_SIDES  = 2;
const unsigned int NUM_STAVES = 9;

std::array<std::string, NUM_LAYERS> layers_names = {"UTaX", "UTaU", "UTbV", "UTbX"};

/// Define color pallete

// Drawing lines
void drawLine( Double_t x1, Double_t y1, Double_t x2, Double_t y2, Color_t color, Width_t width, Style_t style = 1 ) {
  TLine* line = new TLine( x1, y1, x2, y2 );
  line->SetLineColor( color );
  line->SetLineWidth( width );
  line->SetLineStyle( style );
  line->Draw( "SAME" );
}

// Function for plotting 2D histograms
void plot2D( TDirectory* f2, std::string parameter, bool invertPalette, float max, std::string number,
             std::string parentDir ) {

  std::cout << parameter << std::endl;
  if ( !invertPalette ) TColor::InvertPalette();

  unsigned int counter = 1;
  TCanvas*     canva   = new TCanvas( parameter.c_str(), parameter.c_str(), 2000, 1600 );

  canva->Divide( 2, 2 );

  for ( unsigned int layer = 0; layer < 4; layer++ ) {
    std::string title_occ = parameter + "_" + layers_names[layer];
    auto        hist      = (TH2D*)f2->Get( title_occ.c_str() );
    if ( hist ) {
      for ( unsigned int i = 0; i < 4224; i++ ) {
        max = std::max( max, static_cast<float>( hist->GetBinContent( i ) ) );
        hist->SetBinContent( i, hist->GetBinContent( i ) + 0.001 );
      }
    }
  }

  for ( unsigned int layer = 0; layer < 4; layer++ ) {
    std::string title_occ = parameter + "_" + layers_names[layer];
    canva->cd( counter );

    auto hist = (TH2D*)f2->Get( title_occ.c_str() );
    if ( hist ) {

      TAxis* axisX = hist->GetXaxis();

      // Setting stave labeles
      if ( axisX ) {
        for ( signed bin = -9; bin < 9; bin++ ) {
          if ( bin < 0 ) {
            auto bin_title = std::to_string( bin * -1 ) + "A";
            axisX->SetBinLabel( ( bin + 10 ) * 8 - 4, bin_title.c_str() );
          } else {
            auto bin_title = std::to_string( bin + 1 ) + "C";
            axisX->SetBinLabel( ( bin + 10 ) * 8 - 4, bin_title.c_str() );
          }
        }
      }
      // Setting module labels
      std::vector<std::string> y_bins = {"M4B",        "S3(S4)B", "M3B", "S2(S3)B", "M2B", "S1(S2)B", "M1(M1/S1)B",
                                         "M1(S1/M1)T", "S1(S2)T", "M2T", "S2(S3)T", "M3T", "S3(S4)T", "M4T"};
      TAxis*                   axisY  = hist->GetYaxis();
      if ( axisY ) {
        for ( signed bin = -7; bin < 7; bin++ ) {
          if ( bin < 0 ) {
            axisY->SetBinLabel( ( bin + 8 ) * 2 - 1, ( y_bins[bin + 7] ).c_str() );
          } else {
            axisY->SetBinLabel( ( bin + 8 ) * 2 - 1, ( y_bins[bin + 7] ).c_str() );
          }
        }
      }

      counter++;
      gStyle->SetPalette( 57 );
      hist->SetFillStyle( 3001 );
      hist->SetStats( 0 );
      hist->GetZaxis()->SetRangeUser( 0, 6 );

      std::string title = hist->GetTitle();
      title             = title.substr( 0, title.size() );
      hist->SetTitle( title.c_str() );
      hist->Draw( "COLZ" );
    }

    // Per module split
    double stepSize = 1;  // Replace with the actual step size
    double xMin     = -9; // Replace with the actual minimum x value
    double xMax     = 9;  // Replace with the actual maximum x value
    double yMin     = -7; // Replace with the actual minimum y value
    double yMax     = 7;  // Replace with the actual maximum y value

    // Commonly desired beam cutout
    // Assuming xMin, xMax, yMin, yMax are defined as the bounds of your histogram
    double xCenter = ( xMax + xMin ) / 2.0;
    double yCenter = ( yMax + yMin ) / 2.0;
    double radius  = 0.15; // Adjust this value to change the size of the circle

    TEllipse* circle = new TEllipse( xCenter, yCenter, radius );
    circle->SetFillColor( kWhite );
    circle->Draw();

    // Drawing reference
    std::string file_name =
        parentDir + "/UTScripts/References/overlay_center_natural_asic_" + layers_names[layer] + ".root";
    TFile file( file_name.c_str() );
    TIter nextKey( file.GetListOfKeys() );
    TKey* key;
    while ( ( key = dynamic_cast<TKey*>( nextKey() ) ) ) {
      TObject* obj = key->ReadObj();
      if ( obj ) {
        TBox* myBox = static_cast<TBox*>( obj );
        if ( myBox && myBox->GetFillColor() != kWhite ) obj->Draw( "SAME" );
        TLine* myLine = static_cast<TLine*>( obj );
        if ( myLine ) obj->Draw( "SAME" );
      }
    }

    gPad->RedrawAxis();
    gPad->RedrawAxis( "g" );
  }

  std::string filename = parentDir + number + "/Plots/" + parameter + ".png";
  canva->SaveAs( filename.c_str() );
}

// Function for plotting histograms per stave
void plot_histos_stave( TDirectory* f2, std::string parameter, unsigned int layer, unsigned int side,
                        unsigned int stave, std::string number, std::string parentDir ) {

  gStyle->SetStatX( 0.9 );
  gStyle->SetStatY( 0.9 );

  std::array<std::string, NUM_LAYERS> layers_names = {"UTaX", "UTaU", "UTbV", "UTbX"};
  std::string                         side_name    = ( side == 0 ) ? "C" : "A";
  std::string title_canva = parameter + layers_names[layer] + "_" + std::to_string( stave ) + side_name;

  TCanvas* c_pedsub = new TCanvas( title_canva.c_str(), title_canva.c_str(), 2500, 2500 );

  unsigned int   counter_raw = 1;
  std::string    title;
  static TString classname_th1( "TH1D" );
  static TString classname_th2( "TH2D" );

  std::vector<TH1D*> hists;

  for ( auto it : *f2->GetListOfKeys() ) {
    TKey*   key = static_cast<TKey*>( it );
    TClass* cl  = gROOT->GetClass( key->GetClassName() );

    if ( key->GetClassName() == classname_th1 ) {
      TH1D* hist = (TH1D*)key->ReadObj();
      title      = hist->GetName();
      if ( hist && title.find( parameter + layers_names[layer] + "_" + std::to_string( stave ) + side_name ) !=
                       std::string::npos ) {
        hists.push_back( hist );
      }
    }
  }

  if ( hists.size() > 18 )
    c_pedsub->Divide( 4, 6 );
  else if ( hists.size() > 14 )
    c_pedsub->Divide( 4, 5 );
  else
    c_pedsub->Divide( 4, 4 );

  // Sorting histograms per stave
  if ( stave == 1 ) {
    std::vector<std::string> sorting_order = {"T_M4",  "T_S4",  "T_M3",  "T_S3",  "T_M2",  "T_S2W", "T_S2E", "T_M1W",
                                              "T_M1E", "T_S1W", "T_S1E", "B_S1W", "B_S1E", "B_M1W", "B_M1E", "B_S2W",
                                              "B_S2E", "B_M2",  "B_S3",  "B_M3",  "B_S4",  "B_M4"};
    // Function for sorting histograms
    std::sort( hists.begin(), hists.end(), [&sorting_order]( TH1D* h1, TH1D* h2 ) {
      for ( const auto& substring : sorting_order ) {
        bool contains1 = std::string( h1->GetName() ).find( substring ) != std::string::npos;
        bool contains2 = std::string( h2->GetName() ).find( substring ) != std::string::npos;
        if ( contains1 != contains2 ) { return contains1; }
      }
      return std::string( h1->GetName() ) < std::string( h2->GetName() );
    } );

  } else if ( stave == 2 ) {
    std::vector<std::string> sorting_order = {"T_M4",  "T_S3",  "T_M3",  "T_S2",  "T_M2",  "T_S1W",
                                              "T_S1E", "T_M1W", "T_M1E", "B_M1W", "B_M1E", "B_S1W",
                                              "B_S1E", "B_M2",  "B_S2",  "B_M3",  "B_S3",  "B_M4"};
    // Function for sorting histograms
    std::sort( hists.begin(), hists.end(), [&sorting_order]( TH1D* h1, TH1D* h2 ) {
      for ( const auto& substring : sorting_order ) {
        bool contains1 = std::string( h1->GetName() ).find( substring ) != std::string::npos;
        bool contains2 = std::string( h2->GetName() ).find( substring ) != std::string::npos;
        if ( contains1 != contains2 ) { return contains1; }
      }
      return std::string( h1->GetName() ) < std::string( h2->GetName() );
    } );

  }

  else {
    std::vector<std::string> sorting_order = {"T_M4", "T_S3", "T_M3", "T_S2", "T_M2", "T_S1", "T_M1",
                                              "B_M1", "B_S1", "B_M2", "B_S2", "B_M3", "B_S3", "B_M4"};
    // Function for sorting histograms
    std::sort( hists.begin(), hists.end(), [&sorting_order]( TH1D* h1, TH1D* h2 ) {
      for ( const auto& substring : sorting_order ) {
        bool contains1 = std::string( h1->GetName() ).find( substring ) != std::string::npos;
        bool contains2 = std::string( h2->GetName() ).find( substring ) != std::string::npos;
        if ( contains1 != contains2 ) { return contains1; }
      }
      return std::string( h1->GetName() ) < std::string( h2->GetName() );
    } );
  }

  for ( TH1D* hist : hists ) {
    c_pedsub->GetPad( counter_raw )->SetRightMargin( 0.105 );
    c_pedsub->cd( counter_raw++ );
    hist->SetFillStyle( 3001 );
    hist->GetXaxis()->SetTitle( "Strip" );
    hist->GetYaxis()->SetTitle( "ADC" );
    if ( parameter == "MeanADC_" )
      hist->GetYaxis()->SetRangeUser( -31, 31 );
    else
      hist->GetYaxis()->SetRangeUser( 0, 3 );

    hist->Draw( "COLZ" );
  }

  auto path = parentDir + '/' + number + "/Plot/" + parameter + layers_names[layer] + "_" + std::to_string( stave ) +
              side_name + ".png";
  c_pedsub->SaveAs( path.c_str() );
}

// To run: root -l -b Vetra/Ut/UTScripts/Vetra_Plotter_MeanADC.cpp

void Vetra_Plotter_MeanADC() {

  /////////////////////////////////
  const char* number = "0000313122";
  /////////////////////////////////
  std::string number_str( number );

  // Get the current directory (where the script is located)
  std::filesystem::path currentDir = std::filesystem::current_path();

  std::string parentDir = currentDir.string() + "/Vetra/";

  // Setting up the directory
  std::string dir_name = parentDir + std::string( number );

  struct stat st = {0};

  if ( stat( ( dir_name + "/Plots" ).c_str(), &st ) == -1 ) { mkdir( ( dir_name + "/Plots" ).c_str(), 0755 ); }

  TFile* f1 = TFile::Open(
      ( parentDir + '/' + std::string( number ) + "/ut_data_pedestals_" + std::string( number ) + ".root" ).c_str() );

  if ( !f1 ) exit( -1 );

  TDirectory* f3 = (TDirectory*)f1->Get( "UTMeanADCMonitor" );
  for ( unsigned int layer = 0; layer < 4; layer++ )
    for ( unsigned int side = 0; side < 2; side++ )
      for ( unsigned int stave = 1; stave <= 9; stave++ ) {
        // plot_histos_stave( f3, "MeanADC_", layer, side, stave, number_str, parentDir );
        // plot_histos_stave( f3, "SigmaNoise_", layer, side, stave, number_str, parentDir );
      }

  plot2D( f3, "MeanADCAverage", 1, 4, number_str, parentDir );
  plot2D( f3, "SigmaNoiseAverage", 1, 2, number_str, parentDir );
  plot2D( f3, "ZS_th", 1, 4, number_str, parentDir );

  TCanvas* c_proj = new TCanvas( "Projections_SigmaNoise", "Projections_SigmaNoise", 1400, 1200 );

  // Plotting projections
  c_proj->Divide( 2, 2 );

  TH1F* h1 = (TH1F*)f3->Get( "Projection_SigmaNoise" );
  if ( !h1 ) {
    std::cerr << "Failed to get histogram Projection_SigmaNoise\n";
    return;
  }

  TH1F* h2 = (TH1F*)f3->Get( "Projection_SigmaNoise_A" );
  if ( !h2 ) {
    std::cerr << "Failed to get histogram Projection_SigmaNoise_A\n";
    return;
  }

  TH1F* h3 = (TH1F*)f3->Get( "Projection_SigmaNoise_B" );
  if ( !h3 ) {
    std::cerr << "Failed to get histogram Projection_SigmaNoise_B\n";
    return;
  }

  TH1F* h4 = (TH1F*)f3->Get( "Projection_SigmaNoise_C" );
  if ( !h4 ) {
    std::cerr << "Failed to get histogram Projection_SigmaNoise_C\n";
    return;
  }

  c_proj->cd( 1 );
  h1->Draw();

  c_proj->cd( 2 );
  h2->Draw();

  c_proj->cd( 3 );
  h3->Draw();

  c_proj->cd( 4 );
  h4->Draw();

  c_proj->SaveAs( ( parentDir + '/' + std::string( number_str ) + "/Plots/Projections_SigmaNoise.png" ).c_str() );

  TCanvas* c_proj2 = new TCanvas( "Projections_MeanADC", "Projections_MeanADC", 1400, 1200 );

  // Plotting projections
  c_proj2->Divide( 2, 2 );

  TH1F* h5 = (TH1F*)f3->Get( "Projection_MeanADC" );
  if ( !h5 ) {
    std::cerr << "Failed to get histogram Projection_MeanADC\n";
    return;
  }

  TH1F* h6 = (TH1F*)f3->Get( "Projection_MeanADC_A" );
  if ( !h6 ) {
    std::cerr << "Failed to get histogram Projection_MeanADC_A\n";
    return;
  }

  TH1F* h7 = (TH1F*)f3->Get( "Projection_MeanADC_B" );
  if ( !h7 ) {
    std::cerr << "Failed to get histogram Projection_MeanADC_B\n";
    return;
  }

  TH1F* h8 = (TH1F*)f3->Get( "Projection_MeanADC_C" );
  if ( !h8 ) {
    std::cerr << "Failed to get histogram Projection_MeanADC_C\n";
    return;
  }

  c_proj2->cd( 1 );
  h5->Draw();

  c_proj2->cd( 2 );
  h6->Draw();

  c_proj2->cd( 3 );
  h7->Draw();

  c_proj2->cd( 4 );
  h8->Draw();

  c_proj2->SaveAs( ( parentDir + '/' + std::string( number_str ) + "/Plots/Projections_MeanADC.png" ).c_str() );

  gPad->SetLogy();
  c_proj2->cd( 1 );
  h5->Draw();

  gPad->SetLogy();
  c_proj2->cd( 2 );
  h6->Draw();

  gPad->SetLogy();
  c_proj2->cd( 3 );
  h7->Draw();

  gPad->SetLogy();
  c_proj2->cd( 4 );
  h8->Draw();

  c_proj2->SaveAs( ( parentDir + '/' + std::string( number_str ) + "/Plots/Projections_MeanADCLog.png" ).c_str() );

  gSystem->Exit( 0 );
}
