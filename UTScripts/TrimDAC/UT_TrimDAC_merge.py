import ROOT
import glob
import os

layer = "UTbV"
input_path = "/group/ut/ONLINE/tmp/"
run_number = "0000284469"


def merge_branches(layer_name, output_file):
    chain = ROOT.TChain(f"UTDigitTrimDAC/{layer}")

    # Use glob to find all files containing the specified layer name
    matching_files = glob.glob(f"{input_path}/{run_number}/*{layer}*.root")

    # Exclude the output file if it already exists
    if os.path.exists(output_file):
        matching_files = [
            file for file in matching_files if file != output_file
        ]

    print(matching_files)

    for file in matching_files:
        chain.Add(file)

    output = ROOT.TFile(output_file, "RECREATE")

    output.cd()
    tree = chain.CloneTree(-1, "fast")

    output.Write()
    output.Close()
    print("Done")


layer_name = "UTDigitTrimDAC"  # Change this to the desired layer name
output_file = f"{input_path}/{run_number}/{layer}_merged.root"
merge_branches(layer_name, output_file)
