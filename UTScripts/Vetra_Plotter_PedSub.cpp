/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/*
 *  Vetra_Plotter_PedSub.cpp
 *
 *  Created on: September, 2023
 *      Author: Wojciech Krupa (wokrupa@cern.ch)
 */

#include "TCanvas.h"
#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TKey.h"
#include <filesystem>
#include <iostream>
#include <stdio.h>
#include <sys/stat.h>

using namespace std;

// Constants
const unsigned int NUM_LAYERS = 4;
const unsigned int NUM_SIDES  = 2;
const unsigned int NUM_STAVES = 9;

// Function for plotting histograms per stave
void plot_histos_stave( TDirectory* f2, unsigned int layer, unsigned int side, unsigned int stave, std::string number,
                        std::string parentDir ) {

  gStyle->SetStatX( 0.9 );
  gStyle->SetStatY( 0.9 );

  std::array<std::string, NUM_LAYERS> layers_names = {"UTaX", "UTaU", "UTbV", "UTbX"};
  std::string                         side_name    = ( side == 0 ) ? "C" : "A";
  std::string title_canva = "PedSubDigits_" + layers_names[layer] + "_" + std::to_string( stave ) + side_name;

  TCanvas* c_pedsub = new TCanvas( title_canva.c_str(), title_canva.c_str(), 2500, 2500 );

  unsigned int   counter_raw = 1;
  std::string    title;
  static TString classname_th1( "TH1D" );
  static TString classname_th2( "TH2D" );

  std::vector<TH2D*> hists;

  for ( auto it : *f2->GetListOfKeys() ) {
    TKey*   key = static_cast<TKey*>( it );
    TClass* cl  = gROOT->GetClass( key->GetClassName() );

    if ( key->GetClassName() == classname_th2 ) {
      TH2D* hist = (TH2D*)key->ReadObj();
      title      = hist->GetName();
      if ( hist &&
           title.find( layers_names[layer] + "_" + std::to_string( stave ) + side_name ) != std::string::npos ) {
        hists.push_back( hist );
      }
    }
  }

  if ( hists.size() > 18 )
    c_pedsub->Divide( 4, 6 );
  else if ( hists.size() > 14 )
    c_pedsub->Divide( 4, 5 );
  else
    c_pedsub->Divide( 4, 4 );

  // Sorting histograms per stave
  if ( stave == 1 ) {
    std::vector<std::string> sorting_order = {"T_M4",  "T_S4",  "T_M3",  "T_S3",  "T_M2",  "T_S2W", "T_S2E", "T_M1W",
                                              "T_M1E", "T_S1W", "T_S1E", "B_S1W", "B_S1E", "B_M1W", "B_M1E", "B_S2W",
                                              "B_S2E", "B_M2",  "B_S3",  "B_M3",  "B_S4",  "B_M4"};
    // Function for sorting histograms
    std::sort( hists.begin(), hists.end(), [&sorting_order]( TH2D* h1, TH2D* h2 ) {
      for ( const auto& substring : sorting_order ) {
        bool contains1 = std::string( h1->GetName() ).find( substring ) != std::string::npos;
        bool contains2 = std::string( h2->GetName() ).find( substring ) != std::string::npos;
        if ( contains1 != contains2 ) { return contains1; }
      }
      return std::string( h1->GetName() ) < std::string( h2->GetName() );
    } );

  } else if ( stave == 2 ) {
    std::vector<std::string> sorting_order = {"T_M4",  "T_S3",  "T_M3",  "T_S2",  "T_M2",  "T_S1W",
                                              "T_S1E", "T_M1W", "T_M1E", "B_M1W", "B_M1E", "B_S1W",
                                              "B_S1E", "B_M2",  "B_S2",  "B_M3",  "B_S3",  "B_M4"};
    // Function for sorting histograms
    std::sort( hists.begin(), hists.end(), [&sorting_order]( TH2D* h1, TH2D* h2 ) {
      for ( const auto& substring : sorting_order ) {
        bool contains1 = std::string( h1->GetName() ).find( substring ) != std::string::npos;
        bool contains2 = std::string( h2->GetName() ).find( substring ) != std::string::npos;
        if ( contains1 != contains2 ) { return contains1; }
      }
      return std::string( h1->GetName() ) < std::string( h2->GetName() );
    } );

  }

  else {
    std::vector<std::string> sorting_order = {"T_M4", "T_S3", "T_M3", "T_S2", "T_M2", "T_S1", "T_M1",
                                              "B_M1", "B_S1", "B_M2", "B_S2", "B_M3", "B_S3", "B_M4"};
    // Function for sorting histograms
    std::sort( hists.begin(), hists.end(), [&sorting_order]( TH2D* h1, TH2D* h2 ) {
      for ( const auto& substring : sorting_order ) {
        bool contains1 = std::string( h1->GetName() ).find( substring ) != std::string::npos;
        bool contains2 = std::string( h2->GetName() ).find( substring ) != std::string::npos;
        if ( contains1 != contains2 ) { return contains1; }
      }
      return std::string( h1->GetName() ) < std::string( h2->GetName() );
    } );
  }

  for ( TH2D* hist : hists ) {
    c_pedsub->GetPad( counter_raw )->SetRightMargin( 0.105 );
    c_pedsub->cd( counter_raw++ );
    hist->SetFillStyle( 3001 );
    hist->GetXaxis()->SetTitle( "Strip" );
    hist->GetYaxis()->SetTitle( "ADC" );
    hist->Draw( "COLZ" );
  }

  auto path = parentDir + number + "/Plots/" + "PedSubDigits_" + layers_names[layer] + "_" + std::to_string( stave ) +
              side_name + ".png";

  if ( c_pedsub->GetListOfPrimitives()->GetSize() > 0 ) { c_pedsub->SaveAs( path.c_str() ); }
}

// To run: root -l -b Vetra/Ut/UTScripts/Vetra_Plotter_PedSubDigits.cpp

void Vetra_Plotter_PedSub() {

  /////////////////////////////////
  const char* number = "0000313122";
  /////////////////////////////////

  std::string number_str( number );

  // Get the current directory (where the script is located)
  std::filesystem::path currentDir = std::filesystem::current_path();

  std::string parentDir = currentDir.string() + "/Vetra/";

  // Setting up the directory
  std::string dir_name = parentDir + std::string( number );

  struct stat st = {0};

  if ( stat( ( dir_name + "/Plots" ).c_str(), &st ) == -1 ) { mkdir( ( dir_name + "/Plots" ).c_str(), 0755 ); }

  gStyle->SetPalette( 87 );

  TFile* f1 = TFile::Open(
      ( parentDir + std::string( number ) + "/ut_data_pedsub_" + std::string( number ) + ".root" ).c_str() );
  if ( !f1 ) exit( -1 );

  TDirectory* f2 = (TDirectory*)f1->Get( "UTPedestalDigitMonitor" );

  if ( f2 == nullptr ) {
    std::cerr << "Error: UT/UTPedestalDigitMonitor directory not found in the file." << std::endl;
    exit( -1 );
  }

  gStyle->SetPalette( 87 );

  for ( unsigned int layer = 0; layer < 4; layer++ )
    for ( unsigned int side = 0; side < 2; side++ )
      for ( unsigned int stave = 1; stave <= 9; stave++ ) {
        plot_histos_stave( f2, layer, side, stave, number_str, parentDir );
      }

  gSystem->Exit( 0 );
}
