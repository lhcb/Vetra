/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the teSigma of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/*
 *  Vetra_Plotter_EmuNoiseComponents.cpp
 *
 *  Created on: September, 2023
 *      Author: Wojciech Krupa (wokrupa@cern.ch)
 */

#include "TCanvas.h"
#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TKey.h"
#include <filesystem>
#include <iostream>
#include <stdio.h>
#include <sys/stat.h>

using namespace std;

// Constants
const unsigned int NUM_LAYERS = 4;
const unsigned int NUM_SIDES  = 2;
const unsigned int NUM_STAVES = 9;

std::array<std::string, NUM_LAYERS> layers_names = {"UTaX", "UTaU", "UTbV", "UTbX"};

// Drawing lines
void drawLine( Double_t x1, Double_t y1, Double_t x2, Double_t y2, Color_t color, Width_t width, Style_t style = 1 ) {
  TLine* line = new TLine( x1, y1, x2, y2 );
  line->SetLineColor( color );
  line->SetLineWidth( width );
  line->SetLineStyle( style );
  line->Draw( "SAME" );
}

// Function for plotting 2D histograms
void plot2D( TDirectory* f2, std::string parameter, bool invertPalette, float max, std::string number,
             std::string parentDir ) {

  std::cout << parameter << std::endl;
  if ( !invertPalette ) TColor::InvertPalette();

  unsigned int counter = 1;
  TCanvas*     canva   = new TCanvas( parameter.c_str(), parameter.c_str(), 2000, 1600 );

  canva->Divide( 2, 2 );

  for ( unsigned int layer = 0; layer < 4; layer++ ) {
    std::string title_occ = parameter + "_" + layers_names[layer];
    auto        hist      = (TH2D*)f2->Get( title_occ.c_str() );
    if ( hist ) {
      for ( unsigned int i = 0; i < 4224; i++ ) {
        max = std::max( max, static_cast<float>( hist->GetBinContent( i ) ) );
        hist->SetBinContent( i, hist->GetBinContent( i ) + 0.001 );
      }
    }
  }

  for ( unsigned int layer = 0; layer < 4; layer++ ) {
    std::string title_occ = parameter + "_" + layers_names[layer];
    canva->cd( counter );

    auto hist = (TH2D*)f2->Get( title_occ.c_str() );
    if ( hist ) {

      TAxis* axisX = hist->GetXaxis();

      // Setting stave labeles
      if ( axisX ) {
        for ( signed bin = -9; bin < 9; bin++ ) {
          if ( bin < 0 ) {
            auto bin_title = std::to_string( bin * -1 ) + "A";
            axisX->SetBinLabel( ( bin + 10 ) * 8 - 4, bin_title.c_str() );
          } else {
            auto bin_title = std::to_string( bin + 1 ) + "C";
            axisX->SetBinLabel( ( bin + 10 ) * 8 - 4, bin_title.c_str() );
          }
        }
      }
      // Setting module labels
      std::vector<std::string> y_bins = {"M4B",        "S3(S4)B", "M3B", "S2(S3)B", "M2B", "S1(S2)B", "M1(M1/S1)B",
                                         "M1(S1/M1)T", "S1(S2)T", "M2T", "S2(S3)T", "M3T", "S3(S4)T", "M4T"};
      TAxis*                   axisY  = hist->GetYaxis();
      if ( axisY ) {
        for ( signed bin = -7; bin < 7; bin++ ) {
          if ( bin < 0 ) {
            axisY->SetBinLabel( ( bin + 8 ) * 2 - 1, ( y_bins[bin + 7] ).c_str() );
          } else {
            axisY->SetBinLabel( ( bin + 8 ) * 2 - 1, ( y_bins[bin + 7] ).c_str() );
          }
        }
      }

      counter++;
      gStyle->SetPalette( 57 );
      hist->SetFillStyle( 3001 );
      hist->SetStats( 0 );
      hist->GetZaxis()->SetRangeUser( 0, max );

      std::string title = hist->GetTitle();
      title             = title.substr( 0, title.size() );
      hist->SetTitle( title.c_str() );
      hist->Draw( "COLZ" );
    }

    // Per module split
    double stepSize = 1;  // Replace with the actual step size
    double xMin     = -9; // Replace with the actual minimum x value
    double xMax     = 9;  // Replace with the actual maximum x value
    double yMin     = -7; // Replace with the actual minimum y value
    double yMax     = 7;  // Replace with the actual maximum y value

    // Commonly desired beam cutout
    // Assuming xMin, xMax, yMin, yMax are defined as the bounds of your histogram
    double xCenter = ( xMax + xMin ) / 2.0;
    double yCenter = ( yMax + yMin ) / 2.0;
    double radius  = 0.15; // Adjust this value to change the size of the circle

    TEllipse* circle = new TEllipse( xCenter, yCenter, radius );
    circle->SetFillColor( kWhite );
    circle->Draw();

    // Drawing reference
    std::string file_name =
        parentDir + "/UTScripts/References/overlay_center_natural_asic_" + layers_names[layer] + ".root";
    TFile file( file_name.c_str() );
    TIter nextKey( file.GetListOfKeys() );
    TKey* key;
    while ( ( key = dynamic_cast<TKey*>( nextKey() ) ) ) {
      TObject* obj = key->ReadObj();
      if ( obj ) {
        TBox* myBox = static_cast<TBox*>( obj );
        if ( myBox && myBox->GetFillColor() != kWhite ) obj->Draw( "SAME" );
        TLine* myLine = static_cast<TLine*>( obj );
        if ( myLine ) obj->Draw( "SAME" );
      }
    }

    gPad->RedrawAxis();
    gPad->RedrawAxis( "g" );
  }

  std::string filename = parentDir + number + "/Plots/" + parameter + ".png";
  canva->SaveAs( filename.c_str() );
}

// Function for plotting histograms per stave
void plot_histos_stave( TDirectory* f2, unsigned int layer, unsigned int side, unsigned int stave, std::string number,
                        std::string parentDir ) {

  gStyle->SetStatX( 0.9 );
  gStyle->SetStatY( 0.9 );

  std::string side_name   = ( side == 0 ) ? "C" : "A";
  std::string title_canva = "CMS_" + layers_names[layer] + "_" + std::to_string( stave ) + side_name;

  TCanvas* c_cms = new TCanvas( title_canva.c_str(), title_canva.c_str(), 2500, 2500 );

  std::string    title;
  static TString classname_th1( "TH1D" );
  static TString classname_th2( "TH2D" );

  std::vector<TH2D*> hists;

  for ( auto it : *f2->GetListOfKeys() ) {
    TKey*   key = static_cast<TKey*>( it );
    TClass* cl  = gROOT->GetClass( key->GetClassName() );

    if ( key->GetClassName() == classname_th2 ) {
      TH2D* hist = (TH2D*)key->ReadObj();
      title      = hist->GetName();
      if ( hist &&
           title.find( layers_names[layer] + "_" + std::to_string( stave ) + side_name ) != std::string::npos ) {
        hists.push_back( hist );
      }
    }
  }

  if ( hists.size() > 18 )
    c_cms->Divide( 4, 6 );
  else if ( hists.size() > 14 )
    c_cms->Divide( 4, 5 );
  else
    c_cms->Divide( 4, 4 );

  // Sorting histograms per stave
  if ( stave == 1 ) {
    std::vector<std::string> sorting_order = {"T_M4",  "T_S4",  "T_M3",  "T_S3",  "T_M2",  "T_S2W", "T_S2E", "T_M1W",
                                              "T_M1E", "T_S1W", "T_S1E", "B_S1W", "B_S1E", "B_M1W", "B_M1E", "B_S2W",
                                              "B_S2E", "B_M2",  "B_S3",  "B_M3",  "B_S4",  "B_M4"};
    // Function for sorting histograms
    std::sort( hists.begin(), hists.end(), [&sorting_order]( TH2D* h1, TH2D* h2 ) {
      for ( const auto& substring : sorting_order ) {
        bool contains1 = std::string( h1->GetName() ).find( substring ) != std::string::npos;
        bool contains2 = std::string( h2->GetName() ).find( substring ) != std::string::npos;
        if ( contains1 != contains2 ) { return contains1; }
      }
      return std::string( h1->GetName() ) < std::string( h2->GetName() );
    } );

  } else if ( stave == 2 ) {
    std::vector<std::string> sorting_order = {"T_M4",  "T_S3",  "T_M3",  "T_S2",  "T_M2",  "T_S1W",
                                              "T_S1E", "T_M1W", "T_M1E", "B_M1W", "B_M1E", "B_S1W",
                                              "B_S1E", "B_M2",  "B_S2",  "B_M3",  "B_S3",  "B_M4"};
    // Function for sorting histograms
    std::sort( hists.begin(), hists.end(), [&sorting_order]( TH2D* h1, TH2D* h2 ) {
      for ( const auto& substring : sorting_order ) {
        bool contains1 = std::string( h1->GetName() ).find( substring ) != std::string::npos;
        bool contains2 = std::string( h2->GetName() ).find( substring ) != std::string::npos;
        if ( contains1 != contains2 ) { return contains1; }
      }
      return std::string( h1->GetName() ) < std::string( h2->GetName() );
    } );

  }

  else {
    std::vector<std::string> sorting_order = {"T_M4", "T_S3", "T_M3", "T_S2", "T_M2", "T_S1", "T_M1",
                                              "B_M1", "B_S1", "B_M2", "B_S2", "B_M3", "B_S3", "B_M4"};
    // Function for sorting histograms
    std::sort( hists.begin(), hists.end(), [&sorting_order]( TH2D* h1, TH2D* h2 ) {
      for ( const auto& substring : sorting_order ) {
        bool contains1 = std::string( h1->GetName() ).find( substring ) != std::string::npos;
        bool contains2 = std::string( h2->GetName() ).find( substring ) != std::string::npos;
        if ( contains1 != contains2 ) { return contains1; }
      }
      return std::string( h1->GetName() ) < std::string( h2->GetName() );
    } );
  }
  unsigned int counter_raw = 1;

  for ( TH2D* hist : hists ) {
    if ( hist ) {
      //  c_cms->GetPad( counter_raw )->SetRightMargin( 0.105 );
      c_cms->cd( counter_raw++ );
      hist->SetFillStyle( 3001 );
      hist->GetXaxis()->SetTitle( "Strip" );
      hist->GetYaxis()->SetTitle( "ADC" );
      hist->Draw( "COLZ" );
    }
  }

  auto path = parentDir + number + "/Plots/" + "CMS_" + layers_names[layer] + "_" + std::to_string( stave ) +
              side_name + ".png";

  c_cms->SaveAs( path.c_str() );
}

void plot_histos_incnoise_stave_mean( TDirectory* f2, unsigned int layer, unsigned side, unsigned int stave,
                                      std::string number, std::string parentDir ) {

  std::array<std::string, 4> layers_names = {"UTaX", "UTaU", "UTbV", "UTbX"};
  std::string                side_name    = ( side == 0 ) ? "C" : "A";

  bool        not_empty    = 0;
  std::string title_canva  = "CMSNoiseMean_" + layers_names[layer] + "_" + std::to_string( stave ) + side_name;
  TCanvas*    c_noise_mean = new TCanvas( title_canva.c_str(), title_canva.c_str(), 2500, 2500 );
  c_noise_mean->Divide( 5, 5 );

  unsigned int   counter_noise_mean = 1;
  std::string    title;
  static TString classname_th1( "TH1D" );
  static TString classname_th2( "TH2D" );

  for ( auto it : *f2->GetListOfKeys() ) {
    TKey*   key = static_cast<TKey*>( it );
    TClass* cl  = gROOT->GetClass( key->GetClassName() );

    if ( key->GetClassName() == classname_th1 ) {
      TH1D* hist = (TH1D*)key->ReadObj();
      title      = hist->GetName();

      if ( hist && title.find( "CMSNoiseMean_" + layers_names[layer] + "_" + std::to_string( stave ) + side_name ) !=
                       std::string::npos ) {

        // Finding max value for setting range of occupancy plots
        auto max = 0.5;
        auto min = -0.5;

        for ( unsigned int i = 0; i < 512; i++ ) {
          max = ( hist->GetBinContent( i ) > max ) ? hist->GetBinContent( i ) : max;
          min = ( hist->GetBinContent( i ) < min ) ? hist->GetBinContent( i ) : min;
        }
        not_empty = 1;

        c_noise_mean->GetPad( counter_noise_mean )->SetRightMargin( 0.105 );
        c_noise_mean->cd( counter_noise_mean++ );

        hist->SetMaximum( max );
        hist->SetMinimum( min );
        hist->SetFillStyle( 3001 );
        hist->SetStats( 0 );
        hist->GetYaxis()->SetRangeUser( -31, 31 );
        hist->SetLineColor( kBlack );

        hist->Draw();
      }
    }
  }
  if ( not_empty ) {
    auto path = parentDir + number + "/Plots/" + "CMSNoiseMean_" + layers_names[layer] + "_" + std::to_string( stave ) +
                side_name + ".png";
    c_noise_mean->SaveAs( path.c_str() );
  }
}

void plot_histos_noise_stave( TDirectory* f3, TDirectory* f4, unsigned int layer, unsigned side, unsigned int stave,
                              std::map<std::string, float> moduleData, std::string number, std::string parentDir ) {

  std::array<std::string, 4> layers_names = {"UTaX", "UTaU", "UTbV", "UTbX"};
  std::string                side_name    = ( side == 0 ) ? "C" : "A";

  // Plotting noise histograms
  bool        not_empty   = 0;
  std::string title_canva = "CMSNoiseSigma_" + layers_names[layer] + "_" + std::to_string( stave ) + side_name;
  TCanvas*    c_noise     = new TCanvas( title_canva.c_str(), title_canva.c_str(), 2000, 3000 );

  c_noise->Divide( 6, 8 );

  unsigned int   counter_noise = 1;
  std::string    title;
  static TString classname_th1( "TH1D" );
  static TString classname_th2( "TH2D" );

  for ( auto it : *f4->GetListOfKeys() ) {
    TKey*   key = static_cast<TKey*>( it );
    TClass* cl  = gROOT->GetClass( key->GetClassName() );

    // Total Noise Sigma
    if ( key->GetClassName() == classname_th1 ) {
      TH1D* hist = (TH1D*)key->ReadObj();
      title      = hist->GetName();
      if ( hist && title.find( "SigmaNoise_" + layers_names[layer] + "_" + std::to_string( stave ) + side_name ) !=
                       std::string::npos ) {

        std::cout << title << std::endl;
        float max = 3;
        float min = 0;

        not_empty = 1;

        c_noise->GetPad( counter_noise )->SetRightMargin( 0.105 );
        c_noise->cd( counter_noise++ );

        std::string new_title = title;
        size_t      pos       = title.find( "SigmaNoise_" );

        hist->SetTitle( new_title.c_str() );
        hist->SetFillStyle( 3001 );
        hist->SetStats( 0 );
        hist->SetLineColor( kBlue );
        hist->SetAxisRange( 0, 5, "Y" );
        hist->Draw();

        std::string title_occ =
            title.replace( title.find( "SigmaNoise_" ), sizeof( "SigmaNoise_" ) - 1, "CMSNoiseSigma_" );
        auto hist_incoh = (TH1D*)f3->Get( title_occ.c_str() );

        // Plotting CMS noise Sigma
        if ( hist_incoh ) {
          hist_incoh->SetLineColor( kRed );
          hist_incoh->Draw( "SAME" );

          // Create the legend
          auto legend = new TLegend( 0.6, 0.7, 0.88, 0.88 );
          legend->AddEntry( hist, "CMSNoiseSigma", "l" );         // Black line
          legend->AddEntry( hist_incoh, "TotalNoiseSigma", "l" ); // Red line with error bars
          legend->Draw();

          // Creating histograms for projections
          title_occ =
              title.replace( title.find( "CMSNoiseSigma_" ), sizeof( "CMSNoiseSigma_" ) - 1, "Noise_Projections_" );
          auto hist_proj       = new TH1F( title.c_str(), title.c_str(), 100, 0, 3 );
          title_occ            = title.replace( title.find( "Noise_Projections_" ), sizeof( "Noise_Projections_" ) - 1,
                                     "Noise_Projections_Comp_" );
          auto hist_incoh_proj = new TH1F( title.c_str(), title.c_str(), 100, 0, 3 );

          std::string prefix         = "Noise_Projections_Comp_";
          std::string baseModuleName = title_occ.substr( prefix.length() );

          float sum   = 0;
          int   count = 0;
          for ( int i = 0; i < 4; i++ ) {
            std::string moduleName = baseModuleName + "_" + std::to_string( i );
            if ( moduleData.find( moduleName ) != moduleData.end() ) {
              sum += moduleData[moduleName];
              count++;
            }
          }

          for ( unsigned int i = 0; i < 512; i++ ) {
            hist_proj->Fill( hist->GetBinContent( i ) );
            hist_incoh_proj->Fill( hist_incoh->GetBinContent( i ) );
          }

          int max_bin = hist_proj->GetMaximum();

          c_noise->cd( counter_noise++ );
          hist_proj->SetLineColor( kBlue );
          hist_proj->SetLineWidth( 2 );
          hist_proj->SetFillStyle( 3001 );
          hist_proj->SetStats( 0 );
          hist_proj->SetTitle( title_occ.c_str() );
          hist_proj->SetAxisRange( 0, 1.3 * max_bin, "Y" );

          hist_incoh_proj->SetFillStyle( 3001 );
          hist_incoh_proj->SetLineColor( kRed );
          hist_incoh_proj->SetLineWidth( 2 );
          hist_incoh_proj->SetTitle( title_occ.c_str() );
          hist_incoh_proj->SetAxisRange( 0, 1.3 * max_bin, "Y" );

          hist_proj->SetStats( 0 );
          hist_incoh_proj->SetStats( 0 );
          hist_proj->Draw( "HISTOSAME" );
          hist_incoh_proj->SetStats( 0 );
          hist_incoh_proj->Draw( "HISTOSAME" );

          // Create the legend
          auto legend_proj = new TLegend( 0.6, 0.7, 0.88, 0.88 );
          legend_proj->AddEntry( hist_incoh_proj, "CMSNoiseSigma", "l" ); // Black line
          legend_proj->AddEntry( hist_proj, "TotalNoiseSigma", "l" );     // Red line with error bars
          legend_proj->Draw();
        }
      }
    }
  }
  if ( not_empty ) {
    auto path = parentDir + number + "/Plots/" + "Noise_" + layers_names[layer] + "_" + std::to_string( stave ) +
                side_name + ".png";
    c_noise->SaveAs( path.c_str() );
  }
}

// To run: root -l -b Vetra/Ut/UTScripts/Vetra_Plotter_EmuNoiseComponents.cpp

void Vetra_Plotter_EmuNoiseComponents() {

  /////////////////////////////////
  const char* number = "0000313122";
  /////////////////////////////////

  std::string number_str( number );

  // Get the current directory (where the script is located)
  std::filesystem::path currentDir = std::filesystem::current_path();

  std::string parentDir = currentDir.string() + "/Vetra/";

  // Setting up the directory
  std::string dir_name = parentDir + std::string( number );

  struct stat st = {0};

  if ( stat( ( dir_name + "/Plots" ).c_str(), &st ) == -1 ) { mkdir( ( dir_name + "/Plots" ).c_str(), 0755 ); }

  gStyle->SetPalette( 87 );

  TFile* f1 = TFile::Open( ( parentDir + number + "/ut_data_emuNoise_" + std::string( number ) + ".root" ).c_str() );
  if ( !f1 ) {
    std::cerr << "Error: Failed to open file 'ut_data_emuNoise_" << number << ".root'\n";
    exit( -1 );
  }
  TDirectory* f3 = (TDirectory*)f1->Get( "UTPedDigitsToCMDigits" );

  TFile* f0 = TFile::Open( ( parentDir + number + "/ut_data_pedestals_" + std::string( number ) + ".root" ).c_str() );
  if ( !f0 ) {
    std::cerr << "Error: Failed to open file 'ut_data_pedestals_" << number << ".root'\n";
    exit( -1 );
  }
  TDirectory* f4 = (TDirectory*)f0->Get( "UTMeanADCMonitor" );

  if ( f3 == nullptr ) {
    std::cerr << "Error: UTIncNoiseDigitMonitor directory not found in the file." << std::endl;
    exit( -1 );
  }

  if ( f4 == nullptr ) {
    std::cerr << "Error: UTMeanADCMonitor directory not found in the file." << std::endl;
    exit( -1 );
  }

  // Reading CM csv file to plot also CM mean value
  std::map<std::string, float> moduleData;
  std::ifstream                file( ( parentDir + number + "/commonMode_" + std::string( number ) + ".csv" ).c_str() );
  if ( file.is_open() ) {
    std::string line;
    while ( std::getline( file, line ) ) {
      std::stringstream ss( line );
      std::string       module, column2;
      float             value;
      std::getline( ss, module, ',' );
      std::getline( ss, column2, ',' );
      ss >> value;
      moduleData[module] = value;
    }
    file.close();
  } else {
    std::cout << "Unable to open commonMode csv file" << std::endl;
  }

  // Plotting histograms 1D: CMS noise mean, Sigma and ADC
  for ( unsigned int layer = 0; layer < 4; layer++ )
    for ( unsigned int side = 0; side < 2; side++ )
      for ( unsigned int stave = 1; stave <= 9; stave++ ) {
        plot_histos_noise_stave( f3, f4, layer, side, stave, moduleData, number_str, parentDir );
        // plot_histos_incnoise_stave_mean( f3, layer, side, stave, number_str, parentDir );
        // plot_histos_stave( f3, layer, side, stave, number_str, parentDir );
      }

  // Plotting histograms 2D: CommonModeMean, CommonModeSigma, CMSNoiseMean, CMSNoiseSigma
  plot2D( f3, "CommonModeSigmaAverage", 0, 2, number_str, parentDir );
  plot2D( f3, "CommonModeMeanAverage", 0, 2, number_str, parentDir );
  plot2D( f3, "CMSNoiseSigmaAverage", 0, 0, number_str, parentDir );
  plot2D( f3, "CMSNoiseMeanAverage", 0, 0, number_str, parentDir );

  TCanvas* c_proj = new TCanvas( "Projections_NoiseSigma", "Projections_NoiseSigma", 1400, 1200 );

  // Plotting projections
  c_proj->Divide( 2, 2 );

  TH1F* h1 = (TH1F*)f3->Get( "Projection_CMSNoiseSigma" );
  if ( !h1 ) {
    std::cerr << "Failed to get histogram 'Projection_CMSNoiseSigma'\n";
  } else {
    c_proj->cd( 1 );
    h1->Draw();
  }
  TH1F* h2 = (TH1F*)f3->Get( "Projection_CommonModeMean" );
  if ( !h2 ) {
    std::cerr << "Failed to get histogram 'Projection_CommonModeMean'\n";
  } else {
    c_proj->cd( 2 );
    h2->Draw();
  }
  TH1F* h3 = (TH1F*)f3->Get( "Projection_CommonModeSigma" );
  if ( !h3 ) {
    std::cerr << "Failed to get histogram 'Projection_CommonModeSigma'\n";
  } else {
    c_proj->cd( 3 );
    h3->Draw();
  }
  c_proj->SaveAs( ( parentDir + std::string( number_str ) + "/Plots/ProjectionsNoiseSigma.png" ).c_str() );

  TCanvas* c_proj2 = new TCanvas( "Projections_CMSNoiseSigma", "Projections_CMSNoiseSigma", 1400, 1200 );

  // Plotting projections
  c_proj2->Divide( 2, 2 );

  TH1F* h5 = (TH1F*)f3->Get( "Projection_CMSNoiseSigma" );
  if ( !h5 ) {
    std::cerr << "Failed to get histogram 'Projection_CMSNoiseSigma'\n";
  } else {
    c_proj2->cd( 1 );
    h5->Draw();
  }
  TH1F* h6 = (TH1F*)f3->Get( "Projection_CMSNoiseSigma_A" );
  if ( !h6 ) {
    std::cerr << "Failed to get histogram 'Projection_CMSNoiseSigma_A'\n";
  } else {
    c_proj2->cd( 2 );
    h6->Draw();
  }
  TH1F* h7 = (TH1F*)f3->Get( "Projection_CMSNoiseSigma_B" );
  if ( !h7 ) {
    std::cerr << "Failed to get histogram 'Projection_CMSNoiseSigma_B'\n";
  } else {
    c_proj2->cd( 3 );
    h7->Draw();
  }
  TH1F* h8 = (TH1F*)f3->Get( "Projection_CMSNoiseSigma_C" );
  if ( !h8 ) {
    std::cerr << "Failed to get histogram 'Projection_CMSNoiseSigma_C'\n";
  } else {
    c_proj2->cd( 4 );
    h8->Draw();
  }

  c_proj2->SaveAs( ( parentDir + std::string( number_str ) + "/ProjectionsCMSNoiseSigma.png" ).c_str() );

  gSystem->Exit( 0 );
}
