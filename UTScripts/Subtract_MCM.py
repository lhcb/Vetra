import pandas as pd

#This simple script allows to subtract MCM from pedestal file.

# Read the input CSV file (adjust the filename as needed)
input_filename = '/swdev/wokrupa/stack3/Vetra/pedestals_0000309677_test_3.csv'
df = pd.read_csv(input_filename, header=None, names=['channel', 'value'])

# Calculate the average for each group of channels (128 channels in your case)
group_size = 128
num_groups = len(df) // group_size

for group_num in range(num_groups):
    start_idx = group_num * group_size
    end_idx = (group_num + 1) * group_size
    group_values = df.loc[start_idx:end_idx - 1, 'value']

    # Initial average calculation
    initial_avg = group_values.mean()

    # Identify non-outliers (values within the ±5 range of initial average)
    non_outliers = group_values[abs(group_values - initial_avg) <= 5]

    # Recalculate average excluding outliers
    adjusted_avg = int(non_outliers.mean())

    # Adjust only the non-outliers
    df.loc[non_outliers.index, 'value'] -= adjusted_avg

# Save the modified data to a new CSV file
output_filename = '/swdev/wokrupa/stack3/Vetra/pedestals_0000309677_test_4.csv'
df.to_csv(output_filename, index=False)

print(f"Modified data saved to {output_filename}")
