/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/*
 *  Vetra_Plotter_Decoding.cpp
 *
 *  Created on: September, 2023
 *      Author: Wojciech Krupa (wokrupa@cern.ch)
 */

#include "TCanvas.h"
#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TKey.h"
#include <filesystem>
#include <iostream>
#include <stdio.h>
#include <sys/stat.h>

using namespace std;

// Constants
const unsigned int NUM_LAYERS = 4;
const unsigned int NUM_SIDES  = 2;
const unsigned int NUM_STAVES = 9;

std::array<std::string, NUM_LAYERS> layers_names = {"UTaX", "UTaU", "UTbV", "UTbX"};

// Function for plotting 2D histograms
// Function for calculation of the average value of the histogram
void plot2D( TDirectory* f2, std::string parameter, bool invertPalette, float max, std::string number,
             std::string parentDir ) {

  std::cout << parameter << std::endl;
  if ( !invertPalette ) TColor::InvertPalette();

  unsigned int counter = 1;
  TCanvas*     canva   = new TCanvas( parameter.c_str(), parameter.c_str(), 2000, 1600 );

  canva->Divide( 2, 2 );

  for ( unsigned int layer = 0; layer < 4; layer++ ) {
    std::string title_occ = parameter + "_" + layers_names[layer];
    auto        hist      = (TH2D*)f2->Get( title_occ.c_str() );
    if ( hist ) {
      for ( unsigned int i = 0; i < 4224; i++ ) {
        max = std::max( max, static_cast<float>( hist->GetBinContent( i ) ) );
        hist->SetBinContent( i, hist->GetBinContent( i ) + 0.001 );
      }
    }
  }

  for ( unsigned int layer = 0; layer < 4; layer++ ) {
    std::string title_occ = parameter + "_" + layers_names[layer];
    canva->cd( counter );

    auto hist = (TH2D*)f2->Get( title_occ.c_str() );
    if ( hist ) {

      TAxis* axisX = hist->GetXaxis();
      std::cout << hist->GetName() << std::endl;

      // Setting stave labeles
      if ( axisX ) {
        for ( signed bin = -9; bin < 9; bin++ ) {
          if ( bin < 0 ) {
            auto bin_title = std::to_string( bin * -1 ) + "A";
            axisX->SetBinLabel( ( bin + 10 ) * 8 - 4, bin_title.c_str() );
          } else {
            auto bin_title = std::to_string( bin + 1 ) + "C";
            axisX->SetBinLabel( ( bin + 10 ) * 8 - 4, bin_title.c_str() );
          }
        }
      }
      // Setting module labels
      std::vector<std::string> y_bins = {"M4B",        "S3(S4)B", "M3B", "S2(S3)B", "M2B", "S1(S2)B", "M1(M1/S1)B",
                                         "M1(S1/M1)T", "S1(S2)T", "M2T", "S2(S3)T", "M3T", "S3(S4)T", "M4T"};
      TAxis*                   axisY  = hist->GetYaxis();
      if ( axisY ) {
        for ( signed bin = -7; bin < 7; bin++ ) {
          if ( bin < 0 ) {
            axisY->SetBinLabel( ( bin + 8 ) * 2 - 1, ( y_bins[bin + 7] ).c_str() );
          } else {
            axisY->SetBinLabel( ( bin + 8 ) * 2 - 1, ( y_bins[bin + 7] ).c_str() );
          }
        }
      }

      counter++;
      gStyle->SetPalette( 112 );
      hist->SetFillStyle( 3001 );
      hist->SetStats( 0 );
      // hist->GetZaxis()->SetRangeUser( 0, max );

      std::string title = hist->GetTitle();
      title             = title.substr( 0, title.size() );
      hist->SetTitle( title.c_str() );
      hist->Draw( "COLZ" );
    }

    // Per module split
    double stepSize = 1;  // Replace with the actual step size
    double xMin     = -9; // Replace with the actual minimum x value
    double xMax     = 9;  // Replace with the actual maximum x value
    double yMin     = -7; // Replace with the actual minimum y value
    double yMax     = 7;  // Replace with the actual maximum y value

    // Commonly desired beam cutout
    // Assuming xMin, xMax, yMin, yMax are defined as the bounds of your histogram
    double xCenter = ( xMax + xMin ) / 2.0;
    double yCenter = ( yMax + yMin ) / 2.0;
    double radius  = 0.15; // Adjust this value to change the size of the circle

    TEllipse* circle = new TEllipse( xCenter, yCenter, radius );
    circle->SetFillColor( kWhite );
    circle->Draw();

    // Drawing reference
    std::string file_name =
        parentDir + "/UTScripts/References/overlay_center_natural_asic_" + layers_names[layer] + ".root";
    TFile file( file_name.c_str() );
    TIter nextKey( file.GetListOfKeys() );
    TKey* key;
    while ( ( key = dynamic_cast<TKey*>( nextKey() ) ) ) {
      TObject* obj = key->ReadObj();
      if ( obj ) {
        TBox* myBox = static_cast<TBox*>( obj );
        if ( myBox && myBox->GetFillColor() != kWhite ) obj->Draw( "SAME" );
        TLine* myLine = static_cast<TLine*>( obj );
        if ( myLine ) obj->Draw( "SAME" );
      }
    }

    gPad->RedrawAxis();
    gPad->RedrawAxis( "g" );
  }

  std::string filename = parentDir + number + "/Plots/" + parameter + ".png";
  canva->SaveAs( filename.c_str() );
}

std::vector<std::string> types = {"UTZS",     "UTError",   "FragThr.",    "BDCorpt.",    "SyncBXCorr.",
                                  "FragMis.", "FragTrun.", "IdleBXCorr.", "DEFragMalf.", "EVIDJump",
                                  "UTNZS",    "UTSpecial", "FIFOFull",    "ImpSize",     "Uknown"};

// Function for reading CSV ref file
std::map<std::string, std::vector<std::string>> readCSVToMap( const std::string& filename ) {
  std::map<std::string, std::vector<std::string>> csvMap;
  std::ifstream                                   inputFile( filename );

  if ( !inputFile ) {
    std::cerr << "Failed to open the CSV file." << std::endl;
    return csvMap;
  }

  std::string line;
  while ( std::getline( inputFile, line ) ) {
    std::vector<std::string> tokens;
    std::string              token;
    std::istringstream       lineStream( line );

    // Split the line into tokens using commas as the delimiter
    while ( std::getline( lineStream, token, ',' ) ) { tokens.push_back( token ); }

    if ( tokens.size() >= 25 ) {
      std::string key = tokens[0];
      tokens.erase( tokens.begin() ); // Remove the first element (the key)
      csvMap[key] = tokens;
    }
  }

  inputFile.close();
  return csvMap;
}

// Function for finding the most frequent non-zero element
template <typename T>
T mostFrequentNonZeroElement( const std::vector<T>& vec ) {
  std::map<T, int> frequencyMap;

  // Count the occurrences of each non-zero element in the vector
  for ( const T& element : vec ) {
    if ( element != 0 ) { frequencyMap[element]++; }
  }

  if ( frequencyMap.empty() ) {
    // Handle the case where there are no non-zero elements
    throw std::runtime_error( "No non-zero elements in the vector" );
  }

  T   mostFrequent;
  int maxFrequency = 0;

  // Find the most frequent non-zero element
  for ( const auto& pair : frequencyMap ) {
    if ( pair.second > maxFrequency ) {
      mostFrequent = pair.first;
      maxFrequency = pair.second;
    }
  }

  return mostFrequent;
}
// Function for plotting histograms per stave
void plot_histos_stave( TDirectory* f2, unsigned int layer, unsigned int side, unsigned int stave, std::string number,
                        std::string parentDir ) {

  gStyle->SetStatX( 0.9 );
  gStyle->SetStatY( 0.9 );

  std::array<std::string, NUM_LAYERS> layers_names = {"UTaX", "UTaU", "UTbV", "UTbX"};
  std::string                         side_name    = ( side == 0 ) ? "C" : "A";
  std::string                         title_canva  = layers_names[layer] + "_" + std::to_string( stave ) + side_name;
  TCanvas*                            c_raw = new TCanvas( title_canva.c_str(), title_canva.c_str(), 2500, 2500 );

  unsigned int   counter_raw = 1;
  std::string    title;
  static TString classname_th1( "TH1D" );
  static TString classname_th2( "TH2D" );

  std::vector<TH2D*> hists;

  for ( auto it : *f2->GetListOfKeys() ) {
    TKey*   key = static_cast<TKey*>( it );
    TClass* cl  = gROOT->GetClass( key->GetClassName() );

    if ( key->GetClassName() == classname_th2 ) {
      TH2D* hist = (TH2D*)key->ReadObj();
      title      = hist->GetName();
      if ( hist &&
           title.find( layers_names[layer] + "_" + std::to_string( stave ) + side_name ) != std::string::npos ) {
        hists.push_back( hist );
      }
    }
  }

  if ( hists.size() > 18 )
    c_raw->Divide( 4, 6 );
  else if ( hists.size() > 14 )
    c_raw->Divide( 4, 5 );
  else
    c_raw->Divide( 4, 4 );

  // Sorting histograms per stave
  if ( stave == 1 ) {
    std::vector<std::string> sorting_order = {"T_M4",  "T_S4",  "T_M3",  "T_S3",  "T_M2",  "T_S2W", "T_S2E", "T_M1W",
                                              "T_M1E", "T_S1W", "T_S1E", "B_S1W", "B_S1E", "B_M1W", "B_M1E", "B_S2W",
                                              "B_S2E", "B_M2",  "B_S3",  "B_M3",  "B_S4",  "B_M4"};
    // Function for sorting histograms
    std::sort( hists.begin(), hists.end(), [&sorting_order]( TH2D* h1, TH2D* h2 ) {
      for ( const auto& substring : sorting_order ) {
        bool contains1 = std::string( h1->GetName() ).find( substring ) != std::string::npos;
        bool contains2 = std::string( h2->GetName() ).find( substring ) != std::string::npos;
        if ( contains1 != contains2 ) { return contains1; }
      }
      return std::string( h1->GetName() ) < std::string( h2->GetName() );
    } );

  } else if ( stave == 2 ) {
    std::vector<std::string> sorting_order = {"T_M4",  "T_S3",  "T_M3",  "T_S2",  "T_M2",  "T_S1W",
                                              "T_S1E", "T_M1W", "T_M1E", "B_M1W", "B_M1E", "B_S1W",
                                              "B_S1E", "B_M2",  "B_S2",  "B_M3",  "B_S3",  "B_M4"};
    // Function for sorting histograms
    std::sort( hists.begin(), hists.end(), [&sorting_order]( TH2D* h1, TH2D* h2 ) {
      for ( const auto& substring : sorting_order ) {
        bool contains1 = std::string( h1->GetName() ).find( substring ) != std::string::npos;
        bool contains2 = std::string( h2->GetName() ).find( substring ) != std::string::npos;
        if ( contains1 != contains2 ) { return contains1; }
      }
      return std::string( h1->GetName() ) < std::string( h2->GetName() );
    } );

  }

  else {
    std::vector<std::string> sorting_order = {"T_M4", "T_S3", "T_M3", "T_S2", "T_M2", "T_S1", "T_M1",
                                              "B_M1", "B_S1", "B_M2", "B_S2", "B_M3", "B_S3", "B_M4"};
    // Function for sorting histograms
    std::sort( hists.begin(), hists.end(), [&sorting_order]( TH2D* h1, TH2D* h2 ) {
      for ( const auto& substring : sorting_order ) {
        bool contains1 = std::string( h1->GetName() ).find( substring ) != std::string::npos;
        bool contains2 = std::string( h2->GetName() ).find( substring ) != std::string::npos;
        if ( contains1 != contains2 ) { return contains1; }
      }
      return std::string( h1->GetName() ) < std::string( h2->GetName() );
    } );
  }

  for ( TH2D* hist : hists ) {
    c_raw->GetPad( counter_raw )->SetRightMargin( 0.105 );
    c_raw->cd( counter_raw++ );
    hist->SetFillStyle( 3001 );
    hist->GetXaxis()->SetTitle( "Strip" );
    hist->GetYaxis()->SetTitle( "ADC" );
    hist->Draw( "COLZ" );
  }

  auto path = parentDir + number + "/Plots/" + layers_names[layer] + "_" + std::to_string( stave ) + side_name + ".png";

  c_raw->SaveAs( path.c_str() );
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// To run: root -l -b Vetra/Ut/UTScripts/Vetra_Plotter_RawADC.cpp

void Vetra_Plotter_RawADC() {

  /////////////////////////////////
  const char* number = "0000313122";
  /////////////////////////////////

  std::string number_str( number );

  // Get the current directory (where the script is located)
  std::filesystem::path currentDir = std::filesystem::current_path();

  std::string parentDir = currentDir.string() + "/Vetra/";

  // Setting up the directory
  std::string dir_name = parentDir + std::string( number );

  struct stat st = {0};

  if ( stat( ( dir_name + "/Plots" ).c_str(), &st ) == -1 ) { mkdir( ( dir_name + "/Plots" ).c_str(), 0755 ); }

  gStyle->SetPalette( 87 );

  TFile* f1 =
      TFile::Open( ( parentDir + std::string( number ) + "/ut_data_nzs_" + std::string( number ) + ".root" ).c_str() );
  if ( !f1 ) exit( -1 );

  TDirectory* f2 = (TDirectory*)f1->Get( "UTDigitMonitor" );

  if ( f2 == nullptr ) {
    std::cerr << "Error: UTDigitMonitor directory not found in the file." << std::endl;
    exit( -1 );
  }

  // Plotting raw ADC
  for ( unsigned int layer = 0; layer < NUM_LAYERS; layer++ )
    for ( unsigned int side = 0; side < NUM_SIDES; side++ )
      for ( unsigned int stave = 1; stave <= NUM_STAVES; stave++ ) {
        plot_histos_stave( f2, layer, side, stave, number_str, parentDir );
      }

  // MCMS

  plot2D( f2, "MCM_strip", 0, 128, number_str, parentDir );
  plot2D( f2, "MCM_val", 0, 3, number_str, parentDir );

  // BankType vs OdinEvent
  std::string    title;
  static TString classname_th1( "TH1D" );
  static TString classname_th2( "TH2D" );

  std::vector<TH1D*> hists;

  for ( auto it : *f2->GetListOfKeys() ) {
    TKey*   key = static_cast<TKey*>( it );
    TClass* cl  = gROOT->GetClass( key->GetClassName() );

    if ( key->GetClassName() == classname_th2 ) {
      TH1D* hist = (TH1D*)key->ReadObj();
      title      = hist->GetName();
      if ( hist && ( title.find( "UATEL" ) != std::string::npos || title.find( "UCTEL" ) != std::string::npos ) ) {
        hists.push_back( hist );
        std::cout << title << std::endl;
      }
    }
  }

  unsigned int counter_raw    = 1;
  unsigned int counter_canvas = 0;

  TCanvas* c_tell40Odin = nullptr;

  for ( TH1D* hist : hists ) {
    auto xx = counter_raw % 16;
    if ( counter_raw % 16 == 1 ) { // Create a new canvas every 16 plots
      if ( c_tell40Odin ) {
        auto path = parentDir + std::string( number ) + "/Plots/" + "TELL40_" +
                    std::to_string( ( counter_raw - 1 ) / 16 ) + ".png";
        c_tell40Odin->SaveAs( path.c_str() );
      }
      std::string title_canva = "TELL40_" + std::to_string( counter_raw / 16 - 1 );
      c_tell40Odin            = new TCanvas( title_canva.c_str(), title_canva.c_str(), 4200, 3000 );
      c_tell40Odin->Divide( 4, 4 );
    }
    TAxis* axisY = hist->GetYaxis();
    if ( axisY ) {
      for ( signed bin = 1; bin < 16; bin++ ) {
        auto bin_title = types[bin - 1];
        axisY->SetBinLabel( bin, bin_title.c_str() );
      }
    }
    c_tell40Odin->GetPad( counter_raw % 16 == 0 ? 16 : counter_raw % 16 )->SetRightMargin( 0.105 );
    c_tell40Odin->cd( counter_raw % 16 == 0 ? 16 : counter_raw % 16 );
    hist->SetFillStyle( 3001 );
    hist->GetXaxis()->SetTitle( "OdinEvent" );
    hist->GetYaxis()->SetTitle( "Type" );
    hist->SetStats( 0 );
    hist->Draw( "COLZ" );
    counter_raw++;
  }

  if ( c_tell40Odin ) {
    auto path = parentDir + std::string( number ) + "/Plots/" + "TELL40_" + std::to_string( counter_canvas ) + ".png";
    counter_canvas++;
    c_tell40Odin->SaveAs( path.c_str() );
  }

  TColor::InvertPalette();
  // Draw summary plots
  unsigned int counter_occupancy = 1;
  TCanvas*     c_occupancy       = new TCanvas( "Summary_Digits", "Summary_Digits", 2500, 1800 );

  c_occupancy->SetLeftMargin( 0.2 );
  c_occupancy->SetRightMargin( 6.2 );
  c_occupancy->Divide( 2, 2 );

  TGaxis::SetMaxDigits( 3 );

  // List of TELL40s per board
  std::vector<std::string> UT_A_0 = {
      "UA011_0", "UA012_0", "UA013_0", "UA021_0", "UA022_0", "UA023_0", "UA031_0", "UA032_0", "UA033_0",
      "UA041_0", "UA042_0", "UA043_0", "UA051_0", "UA052_0", "UA053_0", "UA061_0", "UA062_0", "UA063_0",
      "UA071_0", "UA072_0", "UA073_0", "UA081_0", "UA082_0", "UA083_0", "UA091_0", "UA092_0", "UA093_0",
      "UA101_0", "UA102_0", "UA103_0", "UA111_0", "UA112_0", "UA113_0", "UA121_0", "UA122_0", "UA123_0",
      "UA131_0", "UA132_0", "UA133_0", "UA141_0", "UA142_0", "UA143_0", "UA151_0", "UA152_0", "UA153_0",
      "UA161_0", "UA162_0", "UA163_0", "UA171_0", "UA172_0", "UA173_0", "UA181_0", "UA182_0", "UA183_0"};

  std::vector<std::string> UT_A_1 = {
      "UA011_1", "UA012_1", "UA013_1", "UA021_1", "UA022_1", "UA023_1", "UA031_1", "UA032_1", "UA033_1",
      "UA041_1", "UA042_1", "UA043_1", "UA051_1", "UA052_1", "UA053_1", "UA061_1", "UA062_1", "UA063_1",
      "UA071_1", "UA072_1", "UA073_1", "UA081_1", "UA082_1", "UA083_1", "UA091_1", "UA092_1", "UA093_1",
      "UA101_1", "UA102_1", "UA103_1", "UA111_1", "UA112_1", "UA113_1", "UA121_1", "UA122_1", "UA123_1",
      "UA131_1", "UA132_1", "UA133_1", "UA141_1", "UA142_1", "UA143_1", "UA151_1", "UA152_1", "UA153_1",
      "UA161_1", "UA162_1", "UA163_1", "UA171_1", "UA172_1", "UA173_1", "UA181_1", "UA182_1", "UA183_1",

  };

  std::vector<std::string> UT_C_0 = {
      "UC011_0", "UC012_0", "UC013_0", "UC021_0", "UC022_0", "UC023_0", "UC031_0", "UC032_0", "UC033_0",
      "UC041_0", "UC042_0", "UC043_0", "UC051_0", "UC052_0", "UC053_0", "UC061_0", "UC062_0", "UC063_0",
      "UC071_0", "UC072_0", "UC073_0", "UC081_0", "UC082_0", "UC083_0", "UC091_0", "UC092_0", "UC093_0",
      "UC101_0", "UC102_0", "UC103_0", "UC111_0", "UC112_0", "UC113_0", "UC121_0", "UC122_0", "UC123_0",
      "UC131_0", "UC132_0", "UC133_0", "UC141_0", "UC142_0", "UC143_0", "UC151_0", "UC152_0", "UC153_0",
      "UC161_0", "UC162_0", "UC163_0", "UC171_0", "UC172_0", "UC173_0", "UC181_0", "UC182_0", "UC183_0"};

  std::vector<std::string> UT_C_1 = {
      "UC011_1", "UC012_1", "UC013_1", "UC021_1", "UC022_1", "UC023_1", "UC031_1", "UC032_1", "UC033_1",
      "UC041_1", "UC042_1", "UC043_1", "UC051_1", "UC052_1", "UC053_1", "UC061_1", "UC062_1", "UC063_1",
      "UC071_1", "UC072_1", "UC073_1", "UC081_1", "UC082_1", "UC083_1", "UC091_1", "UC092_1", "UC093_1",
      "UC101_1", "UC102_1", "UC103_1", "UC111_1", "UC112_1", "UC113_1", "UC121_1", "UC122_1", "UC123_1",
      "UC131_1", "UC132_1", "UC133_1", "UC141_1", "UC142_1", "UC143_1", "UC151_1", "UC152_1", "UC153_1",
      "UC161_1", "UC162_1", "UC163_1", "UC171_1", "UC172_1", "UC173_1", "UC181_1", "UC182_1", "UC183_1",
  };

  // List of TELL40s per board
  const Int_t NRGBs = 3;
  const Int_t NCont = 25;

  Double_t stops[NRGBs] = {0.00, 0.2, 1.00};
  Double_t red[NRGBs]   = {1.00, 1.00, 0.2}; // Lighter red
  Double_t green[NRGBs] = {0.2, 1.00, 0.2};  // Lighter green
  Double_t blue[NRGBs]  = {0.2, 0.2, 1.00};  // Lighter blue

  TColor::CreateGradientColorTable( NRGBs, stops, red, green, blue, NCont );
  gStyle->SetNumberContours( NCont );

  std::array<std::string, NUM_LAYERS> layers_names = {"UTaX", "UTaU", "UTbV", "UTbX"};

  c_occupancy->cd( counter_occupancy );

  std::vector<unsigned int> digicts_asics;

  // Find maximum value at occupancy plots
  for ( unsigned int layer = 0; layer < NUM_LAYERS; layer++ ) {
    std::string title_occ = "Occupancy_" + layers_names[layer];
    auto        hist      = (TH2D*)f2->Get( title_occ.c_str() );
    if ( hist ) {
      for ( unsigned int i = 0; i < 4224; i++ ) { digicts_asics.push_back( hist->GetBinContent( i ) ); }
    }
  }

  // Finding max value for setting common range for all occupancy plots
  auto max = 0;
  max      = mostFrequentNonZeroElement( digicts_asics );
  std::cout << "Max: " << max << std::endl;

  // Create occupancy plots
  for ( unsigned int layer = 0; layer < NUM_LAYERS; layer++ ) {

    std::string title_occ = "Occupancy_" + layers_names[layer];
    c_occupancy->cd( counter_occupancy );

    auto hist = (TH2D*)f2->Get( title_occ.c_str() );
    if ( hist ) {

      TAxis* axisX = hist->GetXaxis();
      std::cout << hist->GetName() << std::endl;

      // Setting stave labeles
      if ( axisX ) {
        for ( signed bin = -9; bin < 9; bin++ ) {
          if ( bin < 0 ) {
            auto bin_title = std::to_string( bin * -1 ) + "A";
            axisX->SetBinLabel( ( bin + 10 ) * 8 - 4, bin_title.c_str() );
          } else {
            auto bin_title = std::to_string( bin + 1 ) + "C";
            axisX->SetBinLabel( ( bin + 10 ) * 8 - 4, bin_title.c_str() );
          }
        }
      }
      // Setting module labels
      std::vector<std::string> y_bins = {"M4B",        "S3(S4)B", "M3B", "S2(S3)B", "M2B", "S1(S2)B", "M1(M1/S1)B",
                                         "M1(S1/M1)T", "S1(S2)T", "M2T", "S2(S3)T", "M3T", "S3(S4)T", "M4T"};
      TAxis*                   axisY  = hist->GetYaxis();
      if ( axisY ) {
        for ( signed bin = -7; bin < 7; bin++ ) {
          if ( bin < 0 ) {
            axisY->SetBinLabel( ( bin + 8 ) * 2 - 1, ( y_bins[bin + 7] ).c_str() );
          } else {
            axisY->SetBinLabel( ( bin + 8 ) * 2 - 1, ( y_bins[bin + 7] ).c_str() );
          }
        }
      }

      counter_occupancy++;
      hist->GetZaxis()->SetRangeUser( 0, max );
      hist->GetZaxis()->SetTitle( "# Digits" );
      hist->SetFillStyle( 3001 );
      hist->SetStats( 0 );
      hist->SetTitleOffset( 0.7, "Z" );
      hist->Draw( "COLZ" );
      auto title_new = "DigitsCounter_" + layers_names[layer];
      hist->SetTitle( title_new.c_str() );
    }

    // Drawing reference
    std::string file_name =
        parentDir + "/UTScripts/References/overlay_center_natural_asic_" + layers_names[layer] + ".root";
    TFile file( file_name.c_str() );
    TIter nextKey( file.GetListOfKeys() );
    TKey* key;
    while ( ( key = dynamic_cast<TKey*>( nextKey() ) ) ) {
      TObject* obj = key->ReadObj();
      if ( obj ) {
        TBox* myBox = static_cast<TBox*>( obj );
        if ( myBox && myBox->GetFillColor() != kWhite ) obj->Draw( "SAME" );
        TLine* myLine = static_cast<TLine*>( obj );
        if ( myLine ) obj->Draw( "SAME" );
      }
    }

    gPad->RedrawAxis();
    gPad->RedrawAxis( "g" );
  }

  c_occupancy->SaveAs( ( parentDir + number_str + "/Plots/Summary_Occupancy.png" ).c_str() );

  TCanvas* c_tell40 = new TCanvas( "Summary_TELL40", "Summary_TELL40", 3000, 1600 );

  c_tell40->SetLeftMargin( 0.01 );
  c_tell40->Divide( 2, 2 );

  const char* strings[] = {"TELL40_vs_ASIC_ASide_Flow0", "TELL40_vs_ASIC_ASide_Flow1", "TELL40_vs_ASIC_CSide_Flow0",
                           "TELL40_vs_ASIC_CSide_Flow1"};

  for ( int i = 0; i < 4; i++ ) {

    c_tell40->cd( i + 1 );

    const char* title = strings[i];

    auto hist = (TH2D*)f2->Get( title );
    if ( hist ) {
      TAxis* axisY = hist->GetYaxis();
      if ( axisY ) {
        for ( signed bin = 1; bin < 25; bin++ ) {
          auto bin_title = std::to_string( ( bin - 1 ) % 24 );
          axisY->SetBinLabel( bin, bin_title.c_str() );
        }
      }
      TAxis* axisX = hist->GetXaxis();
      if ( axisX ) {
        for ( signed bin = 1; bin < 55; bin++ ) {
          std::string s( title );
          std::string bin_title = "";
          if ( i % 2 == 1 )
            bin_title = ( s.find( "ASide" ) != std::string::npos ) ? UT_A_1[bin - 1] : UT_C_1[bin - 1];
          else
            bin_title = ( s.find( "ASide" ) != std::string::npos ) ? UT_A_0[bin - 1] : UT_C_0[bin - 1];
          axisX->SetBinLabel( bin, bin_title.c_str() );
          // axisX->LabelsOption( "a" );
        }
      }

      TColor::InvertPalette();

      hist->SetStats( 0 );
      hist->GetZaxis()->SetTitle( "# Banks" );
      hist->SetTitleOffset( 0.7, "Z" );
      hist->Draw( "SAMECOLZ" );

      double xmin        = hist->GetXaxis()->GetXmin();
      double xmax        = hist->GetXaxis()->GetXmax();
      double ymin        = hist->GetYaxis()->GetXmin();
      double ymax        = hist->GetYaxis()->GetXmax();
      int    ndivisionsX = 54;
      int    ndivisionsY = 24;
      double binwidth    = ( xmax - xmin ) / ndivisionsX;
      double binheight   = ( ymax - ymin ) / ndivisionsY;
      for ( int i = 0; i <= ndivisionsX; i++ ) {
        double x     = xmin + i * binwidth;
        TLine* lineX = new TLine( x, ymin, x, ymax );
        lineX->SetLineStyle( 2 ); // dashed line
        lineX->Draw( "same" );
      }
      for ( int i = 0; i <= ndivisionsY; i++ ) {
        double y     = ymin + i * binheight;
        TLine* lineY = new TLine( xmin, y, xmax, y );
        lineY->SetLineStyle( 2 ); // dashed line
        lineY->Draw( "same" );
      }

      std::string file_name = parentDir + "/UTScripts/References/overlay_tell40" + std::string( title, 15, 1 ) +
                              std::string( title, 25, 1 ) + ".root";

      TFile file( file_name.c_str() );
      TIter nextKey( file.GetListOfKeys() );
      TKey* key;
      while ( ( key = dynamic_cast<TKey*>( nextKey() ) ) ) {
        TObject* obj = key->ReadObj();
        if ( obj ) {
          // Plot the object (you can customize this part)
          obj->Draw( "SAME" );
        }
      }
    }
  }

  c_tell40->SaveAs( ( parentDir + number_str + "/Plots/Summary_TELL40.png" ).c_str() );
  gSystem->Exit( 0 );
}
