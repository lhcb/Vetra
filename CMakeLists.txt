###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

cmake_minimum_required(VERSION 3.15)

project(Vetra VERSION 4.6
        LANGUAGES CXX)

# Definition of path for ut
add_definitions(-DUTEMU_PATH=\"${PROJECT_SOURCE_DIR}/\")

# Enable testing with CTest/CDash
include(CTest)

list(PREPEND CMAKE_MODULE_PATH
    ${PROJECT_SOURCE_DIR}/cmake
)

# Dependencies
set(WITH_Vetra_PRIVATE_DEPENDENCIES TRUE)

include(VetraDependencies)

# -- Subdirectories
lhcb_add_subdirectories(
    UTEmuAlgorithms
    UTEmuOptions   
)

lhcb_finalize_configuration()